const headerMainAnimation = {
  initial: { y: -30, opacity: 0 },
  animate: {
    y: 0,
    opacity: 1,
    transition: {
      delayChildren: 0.7,
    },
  },
};

const headerAnimation = {
  initial: { y: -30, opacity: 0 },
  animate: {
    y: 0,
    opacity: 1,
    transition: {
      staggerChildren: 0.1,
    },
  },
};

const headerChildAnimation = {
  initial: { y: -5, opacity: 0, scale: 1.1 },
  animate: {
    y: 0,
    opacity: 1,
    scale: 1,
    transition: {
      duration: 0.7,
      ease: [0.6, 0.05, -0.01, 0.9],
    },
  },
};

export default {
  headerMainAnimation,
  headerAnimation,
  headerChildAnimation,
};
