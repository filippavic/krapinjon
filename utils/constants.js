const krapinjonTitle = "Krapinjon - udruga mladih iz Krapine";

const krapinjonDescriptionText =
  "Krapinjon je udruga mladih koja djeluje na području Krapine i Krapinsko - zagorske županije. Cilj Udruge Krapinjon je poticanje kreativnog razvoja i stvaralaštva, radoznalosti i inovativnosti među djecom i mladima.";
const newsDescriptionText =
  "Najnovije vijesti i obavijesti o događanjima Udruge Krapinjon.";
const projectsDescriptionText = "Projekti u organizaciji Udruge Krapinjon.";

const webPreviewImageUrl =
  "https://res.cloudinary.com/krapinjon/image/upload/v1667845360/Logo/web_preview.jpg";
const newsPreviewImageUrl =
  "https://res.cloudinary.com/krapinjon/image/upload/v1667845235/Logo/news_preview.jpg";
const projectsPreviewImageUrl =
  "https://res.cloudinary.com/krapinjon/image/upload/v1667845250/Logo/projects_preview.jpg";

const krapinjonVariants = {
  Krapinjon: "krapinjon",
  Krakon: "krakon",
  Huskaj: "huskaj",
};

const elementVariants = {
  Text: "text",
  Bg: "bg",
};

const videoVariant = {
  YouTube: "YouTube",
  Vimeo: "Vimeo",
};

const krakonTitle = "KraKon - prva zagorska geek konvencija";
const krakonDescriptionText =
  "KraKon je prva zagorska geek konvencija. Održava se u Krapini, u organizaciji Udruge Krapinjon, a okuplja zaljubljenike u stripove, znanstvenu fantastiku, fantasy, igre, filmove, serije i cosplay - jednom riječju, geekove.";
const krakonNewsDescriptionText = "Najnovije vijesti i obavijesti o KraKonu.";
const krakonPreviewImageUrl =
  "https://res.cloudinary.com/krapinjon/image/upload/v1678394432/Logo/krakon_preview.jpg";

const huskajTitle = "hušKAJ! - krapinski alternativni glazbeni festival";
const huskajDescriptionText =
  "hušKAJ! je krapinski glazbeni festival usmjeren na poticanje neafirmiranih i lokalnih glazbenika te širenje svijesti o alternativnim glazbenim žanrovima.";
const huskajNewsDescriptionText =
  "Najnovije vijesti i obavijesti o festivalu hušKAJ!";
const huskajPreviewImageUrl =
  "https://res.cloudinary.com/krapinjon/image/upload/v1708541211/Logo/huskaj_web_preview.jpg";

export default {
  krapinjonTitle,
  krapinjonDescriptionText,
  newsDescriptionText,
  projectsDescriptionText,
  webPreviewImageUrl,
  newsPreviewImageUrl,
  projectsPreviewImageUrl,
  krapinjonVariants,
  elementVariants,
  videoVariant,
  krakonTitle,
  krakonDescriptionText,
  krakonNewsDescriptionText,
  krakonPreviewImageUrl,
  huskajTitle,
  huskajDescriptionText,
  huskajNewsDescriptionText,
  huskajPreviewImageUrl,
};
