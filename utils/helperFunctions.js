import { DateTime } from "luxon";
import { currentTimeZone } from "./constants/datetime_constants";

/**
 * Converts dates and times into an appropriate display format
 * @param {string} startDateTime Date and time when the event starts
 * @param {string|undefined} endDateTime Date and time when the event ends (can be undefined)
 * @param {boolean} allDay Indicates whether the event lasts all day
 * @returns Event date and time in a string format
 */
export const dateTimeToString = function (startDateTime, endDateTime, allDay) {
  let eventDateTime;

  let isoStartDate = DateTime.fromISO(startDateTime, {
    zone: currentTimeZone,
  });

  if (allDay) {
    eventDateTime = isoStartDate.toFormat("d. L. yyyy.");

    if (endDateTime) {
      let isoEndDate = DateTime.fromISO(endDateTime, { zone: currentTimeZone });

      if (isoStartDate.day != isoEndDate.day) {
        eventDateTime =
          eventDateTime +
          " - " +
          DateTime.fromISO(endDateTime, { zone: currentTimeZone }).toFormat(
            "d. L. yyyy."
          );
      }
    }
  } else {
    eventDateTime = isoStartDate.toFormat("d. L. yyyy. | HH:mm");

    if (endDateTime) {
      let isoEndDate = DateTime.fromISO(endDateTime, { zone: currentTimeZone });
      if (eventDateTime.startsWith(isoEndDate.toFormat("d. L. yyyy."))) {
        eventDateTime = eventDateTime + " - " + isoEndDate.toFormat("HH:mm");
      } else {
        eventDateTime =
          eventDateTime + " - " + isoEndDate.toFormat("d. L. yyyy. | HH:mm");
      }
    }
  }

  return eventDateTime;
};

/**
 * Finds days between a given date and current date
 * @param {string} startDateTime Date and time when the event starts
 * @returns Remaining days or 0 if event has passed
 */
export const daysRemaining = function (startDateTime) {
  const date1 = DateTime.fromISO(startDateTime, {
    zone: currentTimeZone,
  });
  const date2 = DateTime.now();

  let diff = date1.diff(date2, "days").days;

  return diff > 0 ? Math.round(diff) : 0;
};

/**
 * Simple method to determine if an event has ended.
 * @param {string} endDateTime Date and time when the event ends
 * @returns True if event has ended, false otherwise
 */
export const hasEventEnded = function (endDateTime) {
  const date1 = DateTime.fromISO(endDateTime, {
    zone: currentTimeZone,
  });
  const date2 = DateTime.now();

  return date2 > date1;
};

/**
 * Converts the current DateTime into a year string.
 * @returns Current year in a string format (yyyy)
 */
export const getCurrentYear = function () {
  return DateTime.now().toFormat("yyyy");
};

/**
 * Extracts the day and month from a DateTime object.
 * @param {string} dateTime Date and time to convert
 * @returns Day and month in a string format (DD.MM.)
 */
export const getFormattedDate = function (dateTime) {
  return DateTime.fromISO(dateTime).toFormat("d. L.");
};

/**
 * Extracts the year from a DateTime object.
 * @param {string} dateTime Date and time to convert
 * @returns Year in a string format (yyyy.)
 */
export const getFormattedYear = function (dateTime) {
  return DateTime.fromISO(dateTime).toFormat("yyyy.");
};

/**
 * Converts date and time into a full date format.
 * @param {string} dateTime Date and time to convert
 * @returns Full date in a string format (DD.MM.yyyy.)
 */
export const getFormattedDateYear = function (dateTime) {
  return DateTime.fromISO(dateTime).toFormat("d. L. yyyy.");
};

/**
 * Converts date and time into a full, human-readable date and time format.
 * @param {string} dateTime Date and time to convert
 * @returns Full date and time in a string format (DD.MM.yyyy. HH:mm)
 */
export const getFormattedDateYearTime = function (dateTime) {
  return DateTime.fromISO(dateTime).toFormat("d. L. yyyy. HH:mm");
};

/**
 * Converts a given link to a thumbnail link
 * @param {string} url Standard Cloudinary image URL
 * @returns Thumbnail URL
 */
export const getCloudinaryThumbLink = function (url) {
  let linkArr = url.split("/");
  linkArr.splice(6, 0, "t_thumb");
  let thumbLink = linkArr.join("/");

  return thumbLink;
};

/**
 * Get the event info URL
 * @param {string} link External info URL (if exists)
 * @param {string} slug Unique event ID
 * @returns An object containing the event info URL and a flag marking whether the page is external
 */
export const getEventLink = function (link, slug) {
  if (link) {
    if (link.startsWith("/")) {
      return { url: link, external: false };
    } else {
      return { url: link, external: true };
    }
  }

  return { url: "/dogadaji/" + slug, external: false };
};

/**
 * Get the project page URL
 * @param {string} link External project URL (if exists)
 * @param {string} slug Unique project ID
 * @returns An object containing the project page URL and a flag marking whether the page is external
 */
export const getProjectLink = function (link, slug) {
  if (!link && !slug) return;

  if (link) return { url: link, external: true };

  return { url: "/" + slug, external: false };
};

/**
 * Groups a given list by a certain parameter
 * @param {*} list List of elements to group
 * @param {*} keyGetter Grouping function
 * @returns Array of grouped elements
 */
export const groupBy = function (list, keyGetter) {
  const map = new Map();
  list.forEach((item) => {
    const key = keyGetter(item);
    const collection = map.get(key);
    if (!collection) {
      map.set(key, [item]);
    } else {
      collection.push(item);
    }
  });

  let mapSorted = new Map(
    [...map].sort((a, b) => String(a[0]).localeCompare(b[0]))
  );

  let mapArr = Array.from(mapSorted);

  return mapArr;
};

/**
 * Saves a given key-value pair to browser's local storage
 * @param key Pair key
 * @param value Value to save
 */
export const saveToLocalStorage = function (key, value) {
  localStorage.setItem(key, value);
};

/**
 * Retrieves an item from browser's local storage
 * @param key Items's key
 * @returns Value from storage
 */
export const readFromLocalStorage = function (key) {
  return localStorage.getItem(key);
};
