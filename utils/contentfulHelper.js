import { createClient } from "contentful";
import { Config } from "./config";

export const client = createClient({
  space: process.env.NEXT_PUBLIC_CONTENTFUL_SPACE_ID,
  accessToken: process.env.NEXT_PUBLIC_CONTENTFUL_ACCESS_TOKEN,
  environment: process.env.NEXT_PUBLIC_ENV,
});

export default class ContentfulHelper {
  /**
   * Get the total number of articles
   * @param {string} tag - Specific tag for filtering articles
   * @returns Total article number
   */
  static async getTotalArticlesNumber(tag) {
    const res = await client.getEntries({
      content_type: "news",
      limit: 0,
      ...(tag != null && { "fields.tags[in]": tag }),
    });

    const totalArticles = res.total ? res.total : 0;

    return totalArticles;
  }

  /**
   * Fetches the specific article page
   * @param {number} page - Page of articles to fetch
   * @param {string} tag - Specific tag for filtering articles
   * @returns Object containing articles and the their total count
   */
  static async getArticlePage(page, tag) {
    const skipMultiplier = page === 1 ? 0 : page - 1;
    const skip =
      skipMultiplier > 0 ? Config.pagination.pageSize * skipMultiplier : 0;

    const res = await client.getEntries({
      content_type: "news",
      select:
        "fields.slug,fields.title,fields.tags,fields.thumbnail,sys.contentType,sys.createdAt",
      limit: Config.pagination.pageSize,
      skip: skip,
      order: "-sys.createdAt",
      ...(tag != null && { "fields.tags[in]": tag }),
    });

    const articlePage = res.items
      ? { totalCount: res.total, articles: res.items }
      : { totalCount: 0, articles: [] };

    return articlePage;
  }

  /**
   * Fetches the latest highlighted news article
   * @returns Object containing the latest article
   */
  static async getHighlightedNews(tag) {
    const res = await client.getEntries({
      content_type: "news",
      select: "fields.slug,fields.title,fields.tags,sys.createdAt",
      limit: 1,
      order: "-sys.createdAt",
      ...(tag != null && { "fields.tags[all]": `${tag}` }),
      ...((tag == null || tag == undefined) && { "fields.tags[in]": "Važno" }),
    });

    return res.items.length != 0 ? res.items[0] : null;
  }

  /**
   * Fetches the event page
   * @param {string} eventId - Unique event identificator (slug)
   * @returns Object containing the event page
   */
  static async getEventPage(eventId) {
    const res = await client.getEntries({
      content_type: "eventAbout",
      "fields.slug": eventId,
      select:
        "fields.eventTitle,fields.startDateTime,fields.allDayEvent,fields.endDateTime,fields.locationName,fields.locationForMaps,fields.prices,fields.discounts,fields.information,fields.frequentlyAsked,fields.hospitality,fields.transit,fields.parking,fields.accessibility,fields.guidelines,fields.documents,fields.sponsors,fields.thumbnail,sys.contentType",
      limit: 1,
    });

    return res.items.length != 0 ? res.items[0] : null;
  }
}
