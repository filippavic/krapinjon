/**
 * Returns a Tailwind color used for main elements.
 * @param variant Style variant
 * @returns Tailwind color variant
 */
export const getTailwindMainColor = function (variant) {
  switch (variant) {
    case "krakon":
      return "white";
    default:
      return "beige";
  }
};

/**
 * Returns a Tailwind color used for accent elements.
 * @param variant Style variant
 * @returns Tailwind color variant
 */
export const getTailwindAccentColor = function (variant) {
  switch (variant) {
    case "krakon":
      return "krakon-purple";
    default:
      return "beige";
  }
};
