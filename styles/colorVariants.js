const variants = {
  krakon: "krakon",
  huskaj: "huskaj",
  krapinjon: "default",
};

const mainColorVariants = {
  krakon: "text-beige",
  huskaj: "text-beige",
  default: "text-beige",
};

const accentColorVariants = {
  krakon: "text-krakon-purple",
  huskaj: "text-huskaj-red",
  default: "text-krapinjon-orange",
};

const accentColorHoverVariants = {
  krakon: "text-krakon-purple hover:text-krakon-purple-dark",
  huskaj: "text-huskaj-red hover:text-huskaj-red-dark",
  default: "text-krapinjon-orange hover:text-krapinjon-orange-dark",
};

const hoverVariants = {
  krakon: "hover:text-krakon-purple",
  huskaj: "hover:text-huskaj-red",
  default: "hover:text-krapinjon-orange",
};

const bgColorVariants = {
  krakon: "bg-krakon-purple",
  huskaj: "bg-huskaj-red",
  default: "bg-krapinjon-orange",
};

const borderVariants = {
  krakon: "border-krakon-purple",
  huskaj: "border-huskaj-red",
  default: "border-beige",
};

const buttonBgVariants = {
  krakon: "bg-medium-gray group-hover:bg-krakon-purple",
  huskaj: "bg-medium-gray group-hover:bg-huskaj-red",
  default: "bg-medium-gray group-hover:bg-beige",
};

const buttonIconVariants = {
  krakon: "text-beige group-hover:text-beige",
  huskaj: "text-beige group-hover:text-beige",
  default: "text-beige group-hover:text-medium-gray",
};

export {
  variants,
  mainColorVariants,
  accentColorVariants,
  accentColorHoverVariants,
  hoverVariants,
  bgColorVariants,
  borderVariants,
  buttonBgVariants,
  buttonIconVariants,
};
