import { BLOCKS, MARKS, INLINES } from "@contentful/rich-text-types";
import { motion } from "framer-motion";
import { variants, accentColorVariants } from "./colorVariants";
import IframeElement from "../components/shared/elements/IframeElement";
import constants from "../utils/constants";

// Rich text component customization
const Heading1 = ({ children, variant }) => (
  <motion.h1
    className={`font-bold ${accentColorVariants[variant || "default"]} text-xl`}
    initial={{ opacity: 0, x: 20 }}
    whileInView={{
      opacity: 1,
      x: 0,
      transition: {
        duration: 1,
        ease: [0.6, 0.01, -0.05, 0.9],
      },
    }}
    viewport={{ margin: "-70px 0px -70px 0px", once: true }}
  >
    {children}
  </motion.h1>
);

const Heading2 = ({ children, variant }) => (
  <motion.h2
    className={`font-bold ${
      accentColorVariants[variant || "default"]
    } text-base`}
    initial={{ opacity: 0, x: 15 }}
    whileInView={{
      opacity: 1,
      x: 0,
      transition: {
        duration: 1,
        ease: [0.6, 0.01, -0.05, 0.9],
      },
    }}
    viewport={{ margin: "-70px 0px -70px 0px", once: true }}
  >
    {children}
  </motion.h2>
);

const Heading3 = ({ children, variant }) => (
  <motion.h3
    className={`font-semibold ${
      accentColorVariants[variant || "default"]
    } text-sm`}
    initial={{ opacity: 0, x: 10 }}
    whileInView={{
      opacity: 1,
      x: 0,
      transition: {
        duration: 1,
        ease: [0.6, 0.01, -0.05, 0.9],
      },
    }}
    viewport={{ margin: "-70px 0px -70px 0px", once: true }}
  >
    {children}
  </motion.h3>
);

const Bold = ({ children, variant }) => (
  <span
    className={`font-semibold ${accentColorVariants[variant || "default"]}`}
  >
    {children}
  </span>
);

const Text = ({ children }) => (
  <motion.p
    className="align-center text-beige py-3 font-medium tracking-krapinjon-wide"
    initial={{ opacity: 0, x: 10 }}
    whileInView={{
      opacity: 1,
      x: 0,
      transition: {
        duration: 1,
        ease: [0.6, 0.01, -0.05, 0.9],
      },
    }}
    viewport={{ margin: "-70px 0px -70px 0px", once: true }}
  >
    {children}
  </motion.p>
);

const Quote = ({ children }) => (
  <motion.blockquote
    className="py-5 px-5 align-center"
    initial={{ opacity: 0, x: 10 }}
    whileInView={{
      opacity: 1,
      x: 0,
      transition: {
        duration: 1,
        ease: [0.6, 0.01, -0.05, 0.9],
      },
    }}
    viewport={{ margin: "-70px 0px -70px 0px", once: true }}
  >
    {children}
  </motion.blockquote>
);

const krakonTextRenderOptions = {
  renderMark: {
    [MARKS.BOLD]: function BoldText(text) {
      return <Bold variant={variants.krakon}>{text}</Bold>;
    },
  },
  renderNode: {
    [BLOCKS.PARAGRAPH]: function Paragraph(node, children) {
      return <Text>{children}</Text>;
    },
    [BLOCKS.HEADING_1]: function H1(node, children) {
      return <Heading1 variant={variants.krakon}>{children}</Heading1>;
    },
    [BLOCKS.HEADING_2]: function H2(node, children) {
      return <Heading2 variant={variants.krakon}>{children}</Heading2>;
    },
    [BLOCKS.HEADING_3]: function H3(node, children) {
      return <Heading3 variant={variants.krakon}>{children}</Heading3>;
    },
    [BLOCKS.QUOTE]: function Q(node, children) {
      return <Quote>{children}</Quote>;
    },
    [INLINES.HYPERLINK]: function Hyperlink({ data }, children) {
      if (data.uri.includes("player.vimeo.com/video")) {
        return (
          <IframeElement
            url={data.uri}
            variant={constants.videoVariant.Vimeo}
          />
        );
      } else if (data.uri.includes("youtube.com/embed")) {
        return (
          <IframeElement
            url={data.uri}
            variant={constants.videoVariant.YouTube}
          />
        );
      }

      return (
        <a
          className="text-krakon-purple underline decoration-wavy hover:text-krakon-purple-dark transition duration-300 ease-in-out"
          href={data.uri}
          target="_blank"
          rel="noopener noreferrer"
        >
          {children}
        </a>
      );
    },
    [BLOCKS.UL_LIST]: function UlList(node, children) {
      return (
        <motion.ul
          className="list-disc list-outside pl-12"
          initial={{ opacity: 0, x: 10 }}
          whileInView={{
            opacity: 1,
            x: 0,
            transition: {
              duration: 1,
              ease: [0.6, 0.01, -0.05, 0.9],
            },
          }}
          viewport={{ margin: "-70px 0px -70px 0px", once: true }}
        >
          {children}
        </motion.ul>
      );
    },
    [BLOCKS.OL_LIST]: function OlList(node, children) {
      return (
        <motion.ul
          className="list-decimal list-outside pl-12"
          initial={{ opacity: 0, x: 10 }}
          whileInView={{
            opacity: 1,
            x: 0,
            transition: {
              duration: 1,
              ease: [0.6, 0.01, -0.05, 0.9],
            },
          }}
          viewport={{ margin: "-70px 0px -70px 0px", once: true }}
        >
          {children}
        </motion.ul>
      );
    },
    [BLOCKS.LIST_ITEM]: function ListItem(node, children) {
      return (
        <li className="list-item text-krakon-purple leading-none">
          {children}
        </li>
      );
    },
    [BLOCKS.HR]: function Hr(node, children) {
      return (
        <motion.div
          className="border-t border-krakon-purple my-9"
          initial={{ opacity: 0, scaleX: 0.7 }}
          whileInView={{
            opacity: 0.4,
            scaleX: 1,
            transition: {
              duration: 1,
              ease: [0.6, 0.01, -0.05, 0.9],
            },
          }}
          viewport={{ margin: "-70px 0px -70px 0px" }}
        ></motion.div>
      );
    },
  },
};

const krapinjonTextRenderOptions = {
  renderMark: {
    [MARKS.BOLD]: function BoldText(text) {
      return <Bold>{text}</Bold>;
    },
  },
  renderNode: {
    [BLOCKS.PARAGRAPH]: function Paragraph(node, children) {
      return <Text>{children}</Text>;
    },
    [BLOCKS.HEADING_1]: function H1(node, children) {
      return <Heading1>{children}</Heading1>;
    },
    [BLOCKS.HEADING_2]: function H2(node, children) {
      return <Heading2>{children}</Heading2>;
    },
    [BLOCKS.HEADING_3]: function H3(node, children) {
      return <Heading3>{children}</Heading3>;
    },
    [BLOCKS.QUOTE]: function Q(node, children) {
      return <Quote>{children}</Quote>;
    },
    [INLINES.HYPERLINK]: function Hyperlink({ data }, children) {
      if (data.uri.includes("player.vimeo.com/video")) {
        return (
          <IframeElement
            url={data.uri}
            variant={constants.videoVariant.Vimeo}
          />
        );
      } else if (data.uri.includes("youtube.com/embed")) {
        return (
          <IframeElement
            url={data.uri}
            variant={constants.videoVariant.YouTube}
          />
        );
      }

      return (
        <a
          className="text-krapinjon-orange underline decoration-wavy hover:text-krapinjon-orange-dark transition duration-300 ease-in-out"
          href={data.uri}
          target="_blank"
          rel="noopener noreferrer"
        >
          {children}
        </a>
      );
    },
    [BLOCKS.UL_LIST]: function UlList(node, children) {
      return (
        <motion.ul
          className="list-disc list-outside pl-12"
          initial={{ opacity: 0, x: 10 }}
          whileInView={{
            opacity: 1,
            x: 0,
            transition: {
              duration: 1,
              ease: [0.6, 0.01, -0.05, 0.9],
            },
          }}
          viewport={{ margin: "-70px 0px -70px 0px", once: true }}
        >
          {children}
        </motion.ul>
      );
    },
    [BLOCKS.OL_LIST]: function OlList(node, children) {
      return (
        <motion.ul
          className="list-decimal list-outside pl-12"
          initial={{ opacity: 0, x: 10 }}
          whileInView={{
            opacity: 1,
            x: 0,
            transition: {
              duration: 1,
              ease: [0.6, 0.01, -0.05, 0.9],
            },
          }}
          viewport={{ margin: "-70px 0px -70px 0px", once: true }}
        >
          {children}
        </motion.ul>
      );
    },
    [BLOCKS.LIST_ITEM]: function ListItem(node, children) {
      return (
        <li className="list-item text-krapinjon-orange leading-none">
          {children}
        </li>
      );
    },
    [BLOCKS.HR]: function Hr(node, children) {
      return (
        <motion.div
          className="border-t border-krapinjon-orange my-9"
          initial={{ opacity: 0, scaleX: 0.7 }}
          whileInView={{
            opacity: 0.4,
            scaleX: 1,
            transition: {
              duration: 1,
              ease: [0.6, 0.01, -0.05, 0.9],
            },
          }}
          viewport={{ margin: "-70px 0px -70px 0px" }}
        ></motion.div>
      );
    },
  },
};

const huskajTextRenderOptions = {
  renderMark: {
    [MARKS.BOLD]: function BoldText(text) {
      return <Bold variant={variants.huskaj}>{text}</Bold>;
    },
  },
  renderNode: {
    [BLOCKS.PARAGRAPH]: function Paragraph(node, children) {
      return <Text>{children}</Text>;
    },
    [BLOCKS.HEADING_1]: function H1(node, children) {
      return <Heading1 variant={variants.huskaj}>{children}</Heading1>;
    },
    [BLOCKS.HEADING_2]: function H2(node, children) {
      return <Heading2 variant={variants.huskaj}>{children}</Heading2>;
    },
    [BLOCKS.HEADING_3]: function H3(node, children) {
      return <Heading3 variant={variants.huskaj}>{children}</Heading3>;
    },
    [BLOCKS.QUOTE]: function Q(node, children) {
      return <Quote>{children}</Quote>;
    },
    [INLINES.HYPERLINK]: function Hyperlink({ data }, children) {
      if (data.uri.includes("player.vimeo.com/video")) {
        return (
          <IframeElement
            url={data.uri}
            variant={constants.videoVariant.Vimeo}
          />
        );
      } else if (data.uri.includes("youtube.com/embed")) {
        return (
          <IframeElement
            url={data.uri}
            variant={constants.videoVariant.YouTube}
          />
        );
      }

      return (
        <a
          className="text-huskaj-red underline decoration-wavy hover:text-huskaj-red-dark transition duration-300 ease-in-out"
          href={data.uri}
          target="_blank"
          rel="noopener noreferrer"
        >
          {children}
        </a>
      );
    },
    [BLOCKS.UL_LIST]: function UlList(node, children) {
      return (
        <motion.ul
          className="list-disc list-outside pl-12"
          initial={{ opacity: 0, x: 10 }}
          whileInView={{
            opacity: 1,
            x: 0,
            transition: {
              duration: 1,
              ease: [0.6, 0.01, -0.05, 0.9],
            },
          }}
          viewport={{ margin: "-70px 0px -70px 0px", once: true }}
        >
          {children}
        </motion.ul>
      );
    },
    [BLOCKS.OL_LIST]: function OlList(node, children) {
      return (
        <motion.ul
          className="list-decimal list-outside pl-12"
          initial={{ opacity: 0, x: 10 }}
          whileInView={{
            opacity: 1,
            x: 0,
            transition: {
              duration: 1,
              ease: [0.6, 0.01, -0.05, 0.9],
            },
          }}
          viewport={{ margin: "-70px 0px -70px 0px", once: true }}
        >
          {children}
        </motion.ul>
      );
    },
    [BLOCKS.LIST_ITEM]: function ListItem(node, children) {
      return (
        <li className="list-item text-huskaj-red leading-none">{children}</li>
      );
    },
    [BLOCKS.HR]: function Hr(node, children) {
      return (
        <motion.div
          className="border-t border-huskaj-red my-9"
          initial={{ opacity: 0, scaleX: 0.7 }}
          whileInView={{
            opacity: 0.4,
            scaleX: 1,
            transition: {
              duration: 1,
              ease: [0.6, 0.01, -0.05, 0.9],
            },
          }}
          viewport={{ margin: "-70px 0px -70px 0px" }}
        ></motion.div>
      );
    },
  },
};

const getVariantTextRenderOptions = function (variant) {
  switch (variant) {
    case variants.krakon:
      return krakonTextRenderOptions;
    case variants.huskaj:
      return huskajTextRenderOptions;
    default:
      return krapinjonTextRenderOptions;
  }
};

export {
  krakonTextRenderOptions,
  krapinjonTextRenderOptions,
  huskajTextRenderOptions,
  getVariantTextRenderOptions,
};
