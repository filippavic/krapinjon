import React from "react";
import Head from "next/head";
import { motion } from "framer-motion";
import { createClient } from "contentful";

import KrakonLayout from "../../components/layouts/KrakonLayout";
import KrakonCountdown from "../../components/krakon/KrakonCountdown";
import KrakonContentCard from "../../components/krakon/KrakonContentCard";
import NewsCard from "../../components/NewsCard";
import ScrollDisplayTitleH1 from "../../components/shared/typography/scroll/ScrollDisplayTitleH1";
import ScrollParagraph from "../../components/shared/typography/scroll/ScrollParagraph";
import ArrowButton from "../../components/shared/buttons/ArrowButton";

import { variants } from "../../styles/colorVariants";
import constants from "../../utils/constants";

import workshopThumbnail from "../../public/images/krakon/krakon_workshop.jpg";
import cosplayThumbnail from "../../public/images/krakon/krakon_cosplay.jpg";
import gamesThumbnail from "../../public/images/krakon/krakon_games.jpg";
import talksThumbnail from "../../public/images/krakon/krakon_talks.jpg";
import expoThumbnail from "../../public/images/krakon/krakon_expo.jpg";
import esportsThumbnail from "../../public/images/krakon/krakon_esports.jpg";

const krakonContent = [
  {
    title: "Radionice",
    thumbnail: workshopThumbnail,
  },
  {
    title: "Cosplay",
    thumbnail: cosplayThumbnail,
  },
  {
    title: "Igre",
    thumbnail: gamesThumbnail,
  },
  {
    title: "Predavanja",
    thumbnail: talksThumbnail,
  },
  {
    title: "Štandovi",
    thumbnail: expoThumbnail,
  },
  {
    title: "Esports",
    thumbnail: esportsThumbnail,
  },
];

export async function getStaticProps() {
  const client = createClient({
    space: process.env.CONTENTFUL_SPACE_ID,
    accessToken: process.env.CONTENTFUL_ACCESS_TOKEN,
    environment: process.env.NEXT_PUBLIC_ENV,
  });

  const latestNews = await client.getEntries({
    content_type: "news",
    select:
      "fields.slug,fields.title,fields.tags,fields.thumbnail,sys.contentType,sys.createdAt",
    limit: 4,
    order: "-sys.createdAt",
    "fields.tags[in]": "KraKon",
  });

  const eventData = await client.getEntries({
    content_type: "eventAbout",
    "fields.slug": "krakon",
    select:
      "fields.eventTitle,fields.startDateTime,fields.allDayEvent,fields.endDateTime,fields.locationName,sys.contentType",
    limit: 1,
  });

  return {
    props: {
      latestNews: latestNews.items,
      eventData: eventData.items.length != 0 ? eventData.items[0] : null,
    },
  };
}

export default function Krakon({ latestNews, eventData }) {
  return (
    <motion.div
      className="min-h-screen flex flex-col justify-center items-center"
      exit={{
        opacity: 0,
        transition: { duration: 0.35, ease: [0.6, 0.01, -0.05, 0.9] },
      }}
    >
      <Head>
        <title>{constants.krakonTitle}</title>

        <meta name="description" content={constants.krakonDescriptionText} />
        <meta name="copyright" content="Filip Pavić/Udruga Krapinjon" />

        {/* Theme color */}
        <meta name="theme-color" content="#151515" />
        <meta name="msapplication-navbutton-color" content="#151515" />
        <meta name="apple-mobile-web-app-status-bar-style" content="#151515" />

        {/* Open Graph / Facebook */}
        <meta property="og:type" content="website" />
        <meta property="og:url" content={"https://www.krapinjon.hr/krakon"} />
        <meta property="og:title" content={constants.krakonTitle} />
        <meta
          property="og:description"
          content={constants.krakonDescriptionText}
        />
        <meta property="og:image" content={constants.krakonPreviewImageUrl} />

        {/* Twitter */}
        <meta name="twitter:card" content="summary_large_image" />
        <meta name="twitter:url" content={"https://www.krapinjon.hr/krakon"} />
        <meta name="twitter:title" content={constants.krakonTitle} />
        <meta
          name="twitter:description"
          content={constants.krakonDescriptionText}
        />
        <meta name="twitter:image" content={constants.krakonPreviewImageUrl} />

        <link rel="icon" href="/favicon.ico" />
      </Head>

      <div className="relative w-full h-screen flex justify-center items-center">
        <KrakonCountdown eventData={eventData.fields} />
      </div>

      <div className="self-center w-10/12 max-w-7xl">
        <ScrollDisplayTitleH1 title={"Novosti"} variant={variants.krakon} />

        <div className="justify-items-center mt-10">
          {/* Highlighted news card */}
          {latestNews && latestNews.length > 0 && (
            <NewsCard
              key={0}
              article={latestNews[0]}
              index={0}
              highlightImage={true}
              isGrayscale={true}
              subpage={"krakon"}
              variant="krakon"
            />
          )}

          {/* Other news cards */}
          <div className="w-full grid grid-cols-1 md:grid-cols-3 gap-10 mt-10">
            {latestNews &&
              latestNews.length > 1 &&
              latestNews.slice(1).map((article, index) => {
                return (
                  <NewsCard
                    key={index + 1}
                    article={article}
                    index={index + 1}
                    highlightImage={false}
                    isGrayscale={true}
                    subpage={"krakon"}
                    variant="krakon"
                  />
                );
              })}
          </div>

          {latestNews.length == 0 && (
            <ScrollParagraph text={"Trenutno nema novosti."} />
          )}
        </div>

        {latestNews.length != 0 && (
          <ArrowButton
            id="all-news-button"
            text={"Više novosti"}
            link={"/krakon/novosti"}
            variant={variants.krakon}
            position={"left"}
          />
        )}
      </div>

      <div className="self-center mt-44 w-10/12 max-w-7xl">
        <ScrollDisplayTitleH1 title={"Sadržaj"} variant={variants.krakon} />

        <div className="w-full grid grid-cols-2 lg:grid-cols-3 gap-6 md:gap-10 mt-10">
          {krakonContent.map((content, index) => {
            return <KrakonContentCard key={index + 1} content={content} />;
          })}
        </div>
      </div>
    </motion.div>
  );
}

Krakon.getLayout = function (page, classNames) {
  return (
    <main className={classNames}>
      <KrakonLayout>{page}</KrakonLayout>
    </main>
  );
};
