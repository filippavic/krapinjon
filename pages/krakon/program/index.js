import React from "react";
import Head from "next/head";
import { motion } from "framer-motion";

import KrakonLayout from "../../../components/layouts/KrakonLayout";

import animations from "../../../utils/otherAnimations";
import constants from "../../../utils/constants";

export default function KrakonProgram() {
  return (
    <motion.div
      className="min-h-screen flex flex-col content-center"
      exit={{
        opacity: 0,
        transition: { duration: 0.35, ease: [0.6, 0.01, -0.05, 0.9] },
      }}
    >
      <Head>
        <title>{`Program | ${constants.krakonTitle}`}</title>

        <meta name="description" content={constants.krakonDescriptionText} />
        <meta name="copyright" content="Filip Pavić/Udruga Krapinjon" />

        {/* Theme color */}
        <meta name="theme-color" content="#151515" />
        <meta name="msapplication-navbutton-color" content="#151515" />
        <meta name="apple-mobile-web-app-status-bar-style" content="#151515" />

        {/* Open Graph / Facebook */}
        <meta property="og:type" content="website" />
        <meta
          property="og:url"
          content={"https://www.krapinjon.hr/krakon/program"}
        />
        <meta
          property="og:title"
          content={`Program | ${constants.krakonTitle}`}
        />
        <meta
          property="og:description"
          content={constants.krakonDescriptionText}
        />
        <meta property="og:image" content={constants.krakonPreviewImageUrl} />

        {/* Twitter */}
        <meta name="twitter:card" content="summary_large_image" />
        <meta
          name="twitter:url"
          content={"https://www.krapinjon.hr/krakon/program"}
        />
        <meta
          name="twitter:title"
          content={`Program | ${constants.krakonTitle}`}
        />
        <meta
          name="twitter:description"
          content={constants.krakonDescriptionText}
        />
        <meta name="twitter:image" content={constants.krakonPreviewImageUrl} />

        <link rel="icon" href="/favicon.ico" />
      </Head>

      <div className="flex flex-col w-full h-screen items-center justify-center ">
        <motion.h1
          className="flex self-center justify-center font-display text-5xl text-center text-krakon-purple"
          variants={animations.titleAnimation}
          initial="initial"
          animate="animate"
        >
          Program dolazi uskoro
        </motion.h1>
      </div>
    </motion.div>
  );
}

KrakonProgram.getLayout = function (page, classNames) {
  return (
    <main className={classNames}>
      <KrakonLayout>{page}</KrakonLayout>
    </main>
  );
};
