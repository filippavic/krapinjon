import React from "react";
import { motion } from "framer-motion";

// Components
import KrakonLayout from "../../components/layouts/KrakonLayout";
import InfoScreen from "../../components/screens/info/InfoScreen";

// Functions and helpers
import ContentfulHelper from "../../utils/contentfulHelper";
import constants from "../../utils/constants";
import { variants } from "../../styles/colorVariants";
import CustomHead from "../../components/head/CustomHead";

export async function getStaticProps() {
  const eventPage = await ContentfulHelper.getEventPage("krakon");

  return {
    props: {
      info: eventPage,
    },
  };
}

export default function KrakonInformacije({ info }) {
  // Event page data
  const infoPageData = info.fields;

  // Meta tags
  let metaTags = {
    title: `Informacije | ${constants.krakonTitle}`,
    description: constants.krakonDescriptionText,
    url: "https://www.krapinjon.hr/krakon/informacije",
    previewImageUrl: constants.krakonPreviewImageUrl,
    accentColor: "#151515",
  };

  return (
    <motion.div
      className="min-h-screen flex flex-col"
      exit={{
        opacity: 0,
        transition: { duration: 0.35, ease: [0.6, 0.01, -0.05, 0.9] },
      }}
    >
      <CustomHead metaTags={metaTags} />

      <InfoScreen eventData={infoPageData} variant={variants.krakon} />
    </motion.div>
  );
}

KrakonInformacije.getLayout = function (page, classNames) {
  return (
    <main className={classNames}>
      <KrakonLayout>{page}</KrakonLayout>
    </main>
  );
};
