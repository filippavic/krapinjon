import React from "react";
import { motion } from "framer-motion";

// Components
import KrakonLayout from "../../../../components/layouts/KrakonLayout";
import NewsScreen from "../../../../components/screens/news/NewsScreen";

// Functions and helpers
import ContentfulHelper from "../../../../utils/contentfulHelper";
import { Config } from "../../../../utils/config";
import constants from "../../../../utils/constants";
import { variants } from "../../../../styles/colorVariants";
import CustomHead from "../../../../components/head/CustomHead";

export async function getStaticPaths() {
  const totalArticles = await ContentfulHelper.getTotalArticlesNumber("KraKon");
  const totalPages = Math.ceil(totalArticles / Config.pagination.pageSize);

  const paths = [];

  for (let page = 2; page <= totalPages; page++) {
    paths.push({ params: { page: page.toString() } });
  }

  return {
    paths,
    fallback: "blocking",
  };
}

export async function getStaticProps({ params }) {
  const res = await ContentfulHelper.getArticlePage(params.page, "KraKon");

  const totalPages = Math.ceil(res.totalCount / Config.pagination.pageSize);

  if (params.page <= 0 || params.page > totalPages) {
    return {
      notFound: true,
    };
  }

  return {
    props: {
      news: res.articles,
      totalPages,
      currentPage: params.page,
    },
  };
}

export default function KrakonNovosti({ news, totalPages, currentPage }) {
  // Meta tags
  let metaTags = {
    title: `Novosti (stranica ${currentPage}) | ${constants.krakonTitle}`,
    description: constants.krakonNewsDescriptionText,
    url: `https://www.krapinjon.hr/krakon/novosti/stranica/${currentPage}`,
    previewImageUrl: constants.krakonPreviewImageUrl,
    accentColor: "#151515",
  };

  return (
    <motion.div
      className="min-h-screen flex flex-col"
      exit={{
        opacity: 0,
        transition: { duration: 0.35, ease: [0.6, 0.01, -0.05, 0.9] },
      }}
    >
      <CustomHead metaTags={metaTags} />

      <NewsScreen
        news={news}
        totalPages={totalPages}
        currentPage={currentPage}
        variant={variants.krakon}
      />
    </motion.div>
  );
}

KrakonNovosti.getLayout = function (page, classNames) {
  return (
    <main className={classNames}>
      <KrakonLayout>{page}</KrakonLayout>
    </main>
  );
};
