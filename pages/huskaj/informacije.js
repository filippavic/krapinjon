import React from "react";
import { motion } from "framer-motion";

// Components
import HuskajLayout from "../../components/layouts/HuskajLayout";
import InfoScreen from "../../components/screens/info/InfoScreen";

// Functions and helpers
import ContentfulHelper from "../../utils/contentfulHelper";
import constants from "../../utils/constants";
import { variants } from "../../styles/colorVariants";
import CustomHead from "../../components/head/CustomHead";

export async function getStaticProps() {
  const eventPage = await ContentfulHelper.getEventPage("huskaj");

  return {
    props: {
      info: eventPage,
    },
  };
}

export default function HuskajInformacije({ info }) {
  // Event page data
  const infoPageData = info.fields;

  // Meta tags
  let metaTags = {
    title: `Informacije | ${constants.huskajTitle}`,
    description: constants.huskajDescriptionText,
    url: "https://www.krapinjon.hr/huskaj/informacije",
    previewImageUrl: constants.huskajPreviewImageUrl,
    accentColor: "#151515",
  };

  return (
    <motion.div
      className="min-h-screen flex flex-col"
      exit={{
        opacity: 0,
        transition: { duration: 0.35, ease: [0.6, 0.01, -0.05, 0.9] },
      }}
    >
      <CustomHead metaTags={metaTags} />

      <InfoScreen eventData={infoPageData} variant={variants.huskaj} />
    </motion.div>
  );
}

HuskajInformacije.getLayout = function (page, classNames) {
  return (
    <main className={classNames}>
      <HuskajLayout>{page}</HuskajLayout>
    </main>
  );
};
