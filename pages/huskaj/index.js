import React from "react";
import Head from "next/head";
import { motion } from "framer-motion";
import { createClient } from "contentful";

import HuskajLayout from "../../components/layouts/HuskajLayout";
import NewsCard from "../../components/NewsCard";
import ScrollDisplayTitleH1 from "../../components/shared/typography/scroll/ScrollDisplayTitleH1";
import ScrollParagraph from "../../components/shared/typography/scroll/ScrollParagraph";
import ArrowButton from "../../components/shared/buttons/ArrowButton";

import { variants } from "../../styles/colorVariants";
import constants from "../../utils/constants";
import HuskajHero from "../../components/huskaj/HuskajHero";
import InfoElement from "../../components/shared/elements/InfoElement";
import TaglineElement from "../../components/shared/elements/TaglineElement";

export async function getStaticProps() {
  const client = createClient({
    space: process.env.CONTENTFUL_SPACE_ID,
    accessToken: process.env.CONTENTFUL_ACCESS_TOKEN,
    environment: process.env.NEXT_PUBLIC_ENV,
  });

  const latestNews = await client.getEntries({
    content_type: "news",
    select:
      "fields.slug,fields.title,fields.tags,fields.thumbnail,sys.contentType,sys.createdAt",
    limit: 4,
    order: "-sys.createdAt",
    "fields.tags[in]": "hušKAJ",
  });

  const eventData = await client.getEntries({
    content_type: "eventAbout",
    "fields.slug": "huskaj",
    select:
      "fields.eventTitle,fields.startDateTime,fields.allDayEvent,fields.endDateTime,fields.locationName,fields.infoElements,fields.actionLink,fields.tagline,sys.contentType",
    limit: 1,
  });

  return {
    props: {
      latestNews: latestNews.items,
      eventData: eventData.items.length != 0 ? eventData.items[0] : null,
    },
  };
}

export default function Huskaj({ latestNews, eventData }) {
  // Event page data
  const { infoElements, tagline } = eventData.fields;

  return (
    <motion.div
      className="min-h-screen flex flex-col justify-center items-center"
      exit={{
        opacity: 0,
        transition: { duration: 0.35, ease: [0.6, 0.01, -0.05, 0.9] },
      }}
    >
      <Head>
        <title>{constants.huskajTitle}</title>

        <meta name="description" content={constants.huskajDescriptionText} />
        <meta name="copyright" content="Filip Pavić/Udruga Krapinjon" />

        {/* Theme color */}
        <meta name="theme-color" content="#151515" />
        <meta name="msapplication-navbutton-color" content="#151515" />
        <meta name="apple-mobile-web-app-status-bar-style" content="#151515" />

        {/* Open Graph / Facebook */}
        <meta property="og:type" content="website" />
        <meta property="og:url" content={"https://www.krapinjon.hr/huskaj"} />
        <meta property="og:title" content={constants.huskajDescriptionText} />
        <meta
          property="og:description"
          content={constants.huskajDescriptionText}
        />
        <meta property="og:image" content={constants.huskajPreviewImageUrl} />

        {/* Twitter */}
        <meta name="twitter:card" content="summary_large_image" />
        <meta name="twitter:url" content={"https://www.krapinjon.hr/huskaj"} />
        <meta name="twitter:title" content={constants.huskajTitle} />
        <meta
          name="twitter:description"
          content={constants.huskajDescriptionText}
        />
        <meta name="twitter:image" content={constants.huskajPreviewImageUrl} />

        <link rel="icon" href="/favicon.ico" />
      </Head>

      <div className="relative w-full h-screen flex justify-center items-center">
        <HuskajHero eventData={eventData.fields} />
      </div>

      <div className="w-10/12 max-w-7xl md:mt-8 md:mb-32 mt-0 mb-16">
        <TaglineElement text={tagline} variant={variants.huskaj} />
      </div>

      {infoElements &&
        infoElements.length > 0 &&
        infoElements.map((element, index) => {
          return (
            <div
              key={`infoElemenent_${index}`}
              className="w-10/12 max-w-7xl my-10"
            >
              <InfoElement
                title={element.fields.title}
                text={element.fields.infoText}
                image={element.fields.image}
                variant={variants.huskaj}
                alignment={element.fields.alignment}
              />
            </div>
          );
        })}

      <div className="self-center w-10/12 max-w-7xl mt-36">
        <ScrollDisplayTitleH1 title={"Novosti"} variant={variants.huskaj} />

        <div className="justify-items-center mt-10">
          {/* Highlighted news card */}
          {latestNews && latestNews.length > 0 && (
            <NewsCard
              key={0}
              article={latestNews[0]}
              index={0}
              highlightImage={true}
              isGrayscale={true}
              subpage={"huskaj"}
              variant="huskaj"
            />
          )}

          {/* Other news cards */}
          <div className="w-full grid grid-cols-1 md:grid-cols-3 gap-10 mt-10">
            {latestNews &&
              latestNews.length > 1 &&
              latestNews.slice(1).map((article, index) => {
                return (
                  <NewsCard
                    key={index + 1}
                    article={article}
                    index={index + 1}
                    highlightImage={false}
                    isGrayscale={true}
                    subpage={"huskaj"}
                    variant="huskaj"
                  />
                );
              })}
          </div>

          {latestNews.length == 0 && (
            <ScrollParagraph text={"Trenutno nema novosti."} />
          )}
        </div>

        {latestNews.length != 0 && (
          <ArrowButton
            id="all-news-button"
            text={"Više novosti"}
            link={"/huskaj/novosti"}
            variant={variants.huskaj}
            position={"left"}
          />
        )}
      </div>
    </motion.div>
  );
}

Huskaj.getLayout = function (page, classNames) {
  return (
    <main className={classNames}>
      <HuskajLayout>{page}</HuskajLayout>
    </main>
  );
};
