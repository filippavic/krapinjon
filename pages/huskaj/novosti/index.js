import React from "react";
import { motion } from "framer-motion";

// Components
import HuskajLayout from "../../../components/layouts/HuskajLayout";
import NewsScreen from "../../../components/screens/news/NewsScreen";

// Functions and helpers
import ContentfulHelper from "../../../utils/contentfulHelper";
import { variants } from "../../../styles/colorVariants";
import { Config } from "../../../utils/config";
import constants from "../../../utils/constants";
import CustomHead from "../../../components/head/CustomHead";

export async function getStaticProps() {
  const res = await ContentfulHelper.getArticlePage(1, "hušKAJ");

  const totalPages = Math.ceil(res.totalCount / Config.pagination.pageSize);

  return {
    props: {
      news: res.articles,
      totalPages,
      currentPage: "1",
    },
  };
}

export default function HuskajNovosti({ news, totalPages, currentPage }) {
  // Meta tags
  let metaTags = {
    title: `Novosti (stranica ${currentPage}) | ${constants.huskajTitle}`,
    description: constants.huskajNewsDescriptionText,
    url: "https://www.krapinjon.hr/huskaj/novosti",
    previewImageUrl: constants.huskajPreviewImageUrl,
    accentColor: "#151515",
  };

  return (
    <motion.div
      className="min-h-screen flex flex-col content-center"
      exit={{
        opacity: 0,
        transition: { duration: 0.35, ease: [0.6, 0.01, -0.05, 0.9] },
      }}
    >
      <CustomHead metaTags={metaTags} />

      <NewsScreen
        news={news}
        totalPages={totalPages}
        currentPage={currentPage}
        variant={variants.huskaj}
      />
    </motion.div>
  );
}

HuskajNovosti.getLayout = function (page, classNames) {
  return (
    <main className={classNames}>
      <HuskajLayout>{page}</HuskajLayout>
    </main>
  );
};
