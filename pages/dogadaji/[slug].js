import React, { useEffect, useState } from "react";
import Head from "next/head";
import Image from "next/image";
import { motion, useReducedMotion } from "framer-motion";
import { createClient } from "contentful";
import { documentToReactComponents } from "@contentful/rich-text-react-renderer";
import { BLOCKS, MARKS, INLINES } from "@contentful/rich-text-types";
import ShareComp from "../../components/ShareComp";

import {
  getCloudinaryThumbLink,
  dateTimeToString,
} from "../../utils/helperFunctions";

import LocationMarkerIcon from "@heroicons/react/24/outline/MapPinIcon";
import CurrencyEuroIcon from "@heroicons/react/24/outline/CurrencyEuroIcon";

import AnimatedWords from "../../components/AnimatedWords";
import constants from "../../utils/constants";

// Contentful client
const client = createClient({
  space: process.env.CONTENTFUL_SPACE_ID,
  accessToken: process.env.CONTENTFUL_ACCESS_TOKEN,
  environment: process.env.NEXT_PUBLIC_ENV,
});

export async function getStaticPaths() {
  const res = await client.getEntries({ content_type: "event" });

  const paths = res.items.map((item) => {
    return {
      params: { slug: item.fields.slug },
    };
  });

  return {
    paths,
    fallback: "blocking",
  };
}

export async function getStaticProps({ params }) {
  const { items } = await client.getEntries({
    content_type: "event",
    "fields.slug": params.slug,
  });

  if (!items[0]) {
    return {
      notFound: true,
    };
  }

  return {
    props: { event: items[0] },
  };
}

export default function EventInfo({ event }) {
  // Event data
  const {
    name,
    slug,
    thumbnail,
    type,
    location,
    locationForMaps,
    information,
    startDateTime,
    endDateTime,
    allDay,
    price,
    documents,
    accentColor,
  } = event.fields;

  const [eventDateTime, setEventDateTime] = useState("");
  let thumbLink = getCloudinaryThumbLink(thumbnail[0].original_secure_url);

  // Resize image based on scroll postion
  // Removed for performance reasons
  // const { scrollYProgress } = useScroll();
  // const imageScroll = useTransform(scrollYProgress, [0, 1], [1, 1.2]);

  // Accessibility
  const shouldReduceMotion = useReducedMotion();

  useEffect(() => {
    // Set date and time client-side
    let eventDateTime = dateTimeToString(startDateTime, endDateTime, allDay);
    setEventDateTime(eventDateTime);

    // eslint-disable-next-line react-hooks/exhaustive-deps
  }, []);

  // Rich text component customization
  const Heading1 = ({ children }) => (
    <motion.h1
      className="font-bold text-lg"
      initial={{ opacity: 0, x: 20 }}
      whileInView={{
        opacity: 1,
        x: 0,
        transition: {
          duration: 1,
          ease: [0.6, 0.01, -0.05, 0.9],
        },
      }}
      viewport={{ margin: "-70px 0px -70px 0px", once: true }}
      style={{
        color: accentColor ? accentColor : "#FC8A17",
      }}
    >
      {children}
    </motion.h1>
  );

  const Heading2 = ({ children }) => (
    <motion.h2
      className="font-bold text-beige-dark text-base"
      initial={{ opacity: 0, x: 15 }}
      whileInView={{
        opacity: 1,
        x: 0,
        transition: {
          duration: 1,
          ease: [0.6, 0.01, -0.05, 0.9],
        },
      }}
      viewport={{ margin: "-70px 0px -70px 0px", once: true }}
    >
      {children}
    </motion.h2>
  );

  const Heading3 = ({ children }) => (
    <motion.h3
      className="font-semibold text-beige text-sm"
      initial={{ opacity: 0, x: 10 }}
      whileInView={{
        opacity: 1,
        x: 0,
        transition: {
          duration: 1,
          ease: [0.6, 0.01, -0.05, 0.9],
        },
      }}
      viewport={{ margin: "-70px 0px -70px 0px", once: true }}
    >
      {children}
    </motion.h3>
  );

  const Bold = ({ children }) => (
    <span
      className="font-semibold"
      style={{
        color: accentColor ? accentColor : "#FC8A17",
      }}
    >
      {children}
    </span>
  );

  const Text = ({ children }) => (
    <motion.p
      className="align-center text-beige py-3 font-medium tracking-krapinjon-wide"
      initial={{ opacity: 0, x: 10 }}
      whileInView={{
        opacity: 1,
        x: 0,
        transition: {
          duration: 1,
          ease: [0.6, 0.01, -0.05, 0.9],
        },
      }}
      viewport={{ margin: "-70px 0px -70px 0px", once: true }}
    >
      {children}
    </motion.p>
  );

  const Quote = ({ children }) => (
    <motion.blockquote
      className="py-5 px-5 align-center"
      initial={{ opacity: 0, x: 10 }}
      whileInView={{
        opacity: 1,
        x: 0,
        transition: {
          duration: 1,
          ease: [0.6, 0.01, -0.05, 0.9],
        },
      }}
      viewport={{ margin: "-70px 0px -70px 0px", once: true }}
    >
      {children}
    </motion.blockquote>
  );

  const textRenderOptions = {
    renderMark: {
      [MARKS.BOLD]: function BoldText(text) {
        return <Bold>{text}</Bold>;
      },
    },
    renderNode: {
      [BLOCKS.PARAGRAPH]: function Paragraph(node, children) {
        return <Text>{children}</Text>;
      },
      [BLOCKS.HEADING_1]: function H1(node, children) {
        return <Heading1>{children}</Heading1>;
      },
      [BLOCKS.HEADING_2]: function H2(node, children) {
        return <Heading2>{children}</Heading2>;
      },
      [BLOCKS.HEADING_3]: function H3(node, children) {
        return <Heading3>{children}</Heading3>;
      },
      [BLOCKS.QUOTE]: function Q(node, children) {
        return <Quote>{children}</Quote>;
      },
      [INLINES.HYPERLINK]: function Hyperlink({ data }, children) {
        return (
          <a
            className="underline decoration-wavy transition duration-300 ease-in-out"
            href={data.uri}
            target="_blank"
            rel="noopener noreferrer"
            style={{
              color: accentColor ? accentColor : "#FC8A17",
            }}
          >
            {children}
          </a>
        );
      },
      [BLOCKS.UL_LIST]: function UlList(node, children) {
        return (
          <motion.ul
            className="list-disc list-outside pl-12"
            initial={{ opacity: 0, x: 10 }}
            whileInView={{
              opacity: 1,
              x: 0,
              transition: {
                duration: 1,
                ease: [0.6, 0.01, -0.05, 0.9],
              },
            }}
            viewport={{ margin: "-70px 0px -70px 0px", once: true }}
          >
            {children}
          </motion.ul>
        );
      },
      [BLOCKS.OL_LIST]: function OlList(node, children) {
        return (
          <motion.ul
            className="list-decimal list-outside pl-12"
            initial={{ opacity: 0, x: 10 }}
            whileInView={{
              opacity: 1,
              x: 0,
              transition: {
                duration: 1,
                ease: [0.6, 0.01, -0.05, 0.9],
              },
            }}
            viewport={{ margin: "-70px 0px -70px 0px", once: true }}
          >
            {children}
          </motion.ul>
        );
      },
      [BLOCKS.LIST_ITEM]: function ListItem(node, children) {
        return (
          <li
            className="list-item leading-none"
            style={{
              color: accentColor ? accentColor : "#FC8A17",
            }}
          >
            {children}
          </li>
        );
      },
      [BLOCKS.HR]: function Hr(node, children) {
        return (
          <motion.div
            className="border-t border-krapinjon-orange my-9"
            initial={{ opacity: 0, scaleX: 0.7 }}
            whileInView={{
              opacity: 0.4,
              scaleX: 1,
              transition: {
                duration: 1,
                ease: [0.6, 0.01, -0.05, 0.9],
              },
            }}
            viewport={{ margin: "-70px 0px -70px 0px" }}
          ></motion.div>
        );
      },
    },
  };

  return (
    <motion.div
      className="min-h-screen flex flex-col"
      exit={{
        opacity: 0,
        transition: { duration: 0.35, ease: [0.6, 0.01, -0.05, 0.9] },
      }}
    >
      <Head>
        <title>{`${name} | ${constants.krapinjonTitle}`}</title>

        <meta name="description" content="" />
        <meta name="copyright" content="Filip Pavić/Udruga Krapinjon" />

        {/* Theme color */}
        <meta name="theme-color" content="#151515" />
        <meta name="msapplication-navbutton-color" content="#151515" />
        <meta name="apple-mobile-web-app-status-bar-style" content="#151515" />

        {/* Open Graph / Facebook */}
        <meta property="og:type" content="website" />
        <meta
          property="og:url"
          content={"https://www.krapinjon.hr/dogadaji/" + slug}
        />
        <meta
          property="og:title"
          content={`${name} | ${constants.krapinjonTitle}`}
        />
        <meta property="og:description" content="" />
        <meta property="og:image" content={thumbnail[0].original_secure_url} />

        {/* Twitter */}
        <meta name="twitter:card" content="summary_large_image" />
        <meta
          name="twitter:url"
          content={"https://www.krapinjon.hr/dogadaji/" + slug}
        />
        <meta
          name="twitter:title"
          content={`${name} | ${constants.krapinjonTitle}`}
        />
        <meta name="twitter:description" content="" />
        <meta name="twitter:image" content={thumbnail[0].original_secure_url} />

        <link rel="icon" href="/favicon.ico" />
      </Head>

      <motion.div className="absolute w-full h-4/5 overflow-hidden">
        <div className="absolute z-20 w-full h-full bg-gradient-to-b from-transparent to-background-black"></div>
        <div
          className="absolute z-10 w-full h-full mix-blend-multiply"
          style={{
            backgroundColor: accentColor ? accentColor : "#FC8A17",
          }}
        ></div>

        <div className="absolute z-10 w-full h-full bg-texture-image mix-blend-multiply"></div>

        <motion.div
          className="w-full h-full relative"
          initial={{
            opacity: 0,
            y: 40,
          }}
          animate={{
            opacity: 1,
            y: 0,
            transition: {
              duration: 1,
              ease: [0.6, 0.01, -0.05, 0.9],
            },
          }}
          // Removed for performance reasons
          // style={{
          //   scale: shouldReduceMotion ? 1 : imageScroll,
          // }}
        >
          <Image
            src={thumbnail[0].original_secure_url}
            blurDataURL={thumbLink}
            placeholder="blur"
            fill
            style={{
              objectFit: "cover",
            }}
            alt=""
            quality={68}
            className="grayscale"
            priority
          />
        </motion.div>
      </motion.div>

      <motion.div className="self-center mt-h-1/3 w-10/12 rounded-xl max-w-screen-2xl">
        <motion.div className="flex flex-col relative w-full h-80 justify-end overflow-hidden">
          <motion.span
            className="z-50 text-xs uppercase font-extrabold mb-1 text-beige"
            initial={{ opacity: 0, y: 15 }}
            animate={{
              opacity: 1,
              y: 0,
              transition: {
                duration: 0.7,
                ease: [0.6, 0.01, -0.05, 0.9],
              },
            }}
          >
            {type}
          </motion.span>
          <AnimatedWords title={name} accentColor={accentColor} />
          <motion.span
            className="z-50 flex text-base sm:text-xl font-bold text-beige"
            initial={{ opacity: 0, y: 15 }}
            animate={{
              opacity: 1,
              y: 0,
              transition: {
                duration: 0.7,
                ease: [0.6, 0.01, -0.05, 0.9],
                delay: 0.1,
              },
            }}
          >
            {eventDateTime}
          </motion.span>
          <motion.div
            className="z-50 inline-flex items-center mt-4"
            initial={{ opacity: 0, y: 15 }}
            animate={{
              opacity: 1,
              y: 0,
              transition: {
                duration: 0.7,
                ease: [0.6, 0.01, -0.05, 0.9],
                delay: 0.2,
              },
            }}
          >
            <LocationMarkerIcon className="h-5 w-5 text-beige mr-1" />
            {locationForMaps ? (
              <a
                className="text-beige text-base font-semibold hover:text-beige-dark transition duration-300 ease-in-out"
                href={locationForMaps}
              >
                {location}
              </a>
            ) : (
              <span className="text-beige text-base font-semibold">
                {location}
              </span>
            )}
          </motion.div>
          <motion.div
            className="z-50 inline-flex items-center mt-2"
            initial={{ opacity: 0, y: 15 }}
            animate={{
              opacity: 1,
              y: 0,
              transition: {
                duration: 0.7,
                ease: [0.6, 0.01, -0.05, 0.9],
                delay: 0.3,
              },
            }}
          >
            <CurrencyEuroIcon className="h-5 w-5 text-beige mr-1" />
            <span className="text-beige text-base font-semibold">{price}</span>
          </motion.div>
        </motion.div>
        <div className="flex flex-col-reverse md:flex-row w-full h-auto relative z-50 mt-24">
          <div className="flex flex-col w-full md:w-2/5 justify-between md:justify-start mt-16 md:mt-0">
            <motion.div className="">
              <motion.span
                className="text-sm text-beige font-semibold block"
                initial={{ opacity: 0, x: -10 }}
                animate={{
                  opacity: 1,
                  x: 0,
                  transition: {
                    duration: 1,
                    ease: [0.6, 0.01, -0.05, 0.9],
                  },
                }}
              >
                Prilozi
              </motion.span>
              {documents !== undefined ? (
                <div className="mt-1">
                  {documents.map((document, index) => {
                    return (
                      <motion.a
                        className="text-beige-dark font-semibold hover:text-beige transition duration-300 ease-in-out block"
                        href={"https:" + document.fields.file.url}
                        key={index}
                        target="_blank"
                        rel="noreferrer"
                        initial={{ opacity: 0, x: -10 }}
                        animate={{
                          opacity: 1,
                          x: 0,
                          transition: {
                            duration: 0.5,
                            ease: [0.6, 0.01, -0.05, 0.9],
                            delay: 0.05 * index,
                          },
                        }}
                      >
                        {`${
                          document.fields.title
                        } (.${document.fields.file.fileName.split(".").pop()})`}
                      </motion.a>
                    );
                  })}
                </div>
              ) : (
                <div className="mt-1">
                  <motion.span
                    className="text-beige block"
                    initial={{ opacity: 0, x: -10 }}
                    animate={{
                      opacity: 1,
                      x: 0,
                      transition: {
                        duration: 0.5,
                        ease: [0.6, 0.01, -0.05, 0.9],
                      },
                    }}
                  >
                    -
                  </motion.span>
                </div>
              )}
            </motion.div>
          </div>
          <div className="flex flex-col w-full md:w-3/5">
            <motion.div
              initial={{ opacity: 0, y: 15 }}
              animate={{
                opacity: 1,
                y: 0,
                transition: {
                  duration: 1,
                  ease: [0.6, 0.01, -0.05, 0.9],
                },
              }}
            >
              {documentToReactComponents(information, textRenderOptions)}
            </motion.div>
          </div>
        </div>

        <motion.div
          className="flex flex-col mt-16"
          initial={{ opacity: 0, y: 15 }}
          whileInView={{
            opacity: 1,
            y: 0,
            transition: {
              duration: 0.8,
              ease: [0.6, 0.01, -0.05, 0.9],
            },
          }}
          viewport={{
            margin: "-50px 0px -50px 0px",
            once: true,
          }}
        >
          <span className="text-beige font-semibold text-sm">Podijeli</span>
          <ShareComp url={"https://www.krapinjon.hr/dogadaji/" + slug} />
        </motion.div>
      </motion.div>
    </motion.div>
  );
}
