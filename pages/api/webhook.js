import ContentfulHelper from "../../utils/contentfulHelper";
import { Config } from "../../utils/config";

export default async function handleWebhook(req, res) {
  const body = await getRawBody(req);
  if (!body) {
    res.status(400).send("Bad request (no body)");
    return;
  }

  const jsonBody = JSON.parse(body);

  const secret = process.env.REVALIDATION_TOKEN;
  const token = req.headers["token"];

  if (secret === token) {
    const type = jsonBody.type;
    const changeType = jsonBody.changeType;
    const slug = jsonBody.slug != undefined ? jsonBody.slug["hr-HR"] : null;

    // Revalidate landing page
    if (type == "event" || type == "news") {
      await res.revalidate("/");

      // Revalidate KraKon and husKAJ landing pages
      await res.revalidate("/krakon");
      await res.revalidate("/huskaj");
    }

    // Revalidate information page
    if (type == "about") {
      await res.revalidate("/informacije");
    }

    // Revalidate event details page
    if (type == "event" && slug != null) {
      await res.revalidate(`/dogadaji/${slug}`);
    }

    // Revalidate event subpage
    if (type == "eventAbout" && slug != null) {
      await res.revalidate(`/${slug}/informacije`);

      // Also invalidate the landing page
      await res.revalidate(`/${slug}`);
    }

    // Revalidate projects page
    if (type == "project") {
      await res.revalidate("/projekti");
    }

    // Revalidate news
    if (type == "news") {
      if (slug != null) {
        // Revalidate article page
        await res.revalidate(`/novosti/${slug}`);
      }

      // Revalidate news index page
      await res.revalidate("/novosti");

      // Revalidate the rest
      const totalArticles = await ContentfulHelper.getTotalArticlesNumber();
      const totalPages = Math.ceil(totalArticles / Config.pagination.pageSize);

      for (let page = 2; page <= totalPages; page++) {
        await res.revalidate(`/novosti/stranica/${page}`);
      }

      try {
        const tagList = Array.from(tags);
        if (tagList.includes("KraKon")) {
          // Revalidate KraKon pages
          if (slug != null) {
            // Revalidate KraKon article page
            await res.revalidate(`/krakon/novosti/${slug}`);
          }

          // Revalidate KraKon news index page
          await res.revalidate("/krakon/novosti");

          // Revalidate the rest
          const totalKrakonArticles =
            await ContentfulHelper.getTotalArticlesNumber("KraKon");
          const totalKrakonPages = Math.ceil(
            totalKrakonArticles / Config.pagination.pageSize
          );

          for (let page = 2; page <= totalKrakonPages; page++) {
            await res.revalidate(`/krakon/novosti/stranica/${page}`);
          }
        } else if (tagList.includes("hušKAJ")) {
          // Revalidate husKAJ pages
          if (slug != null) {
            // Revalidate husKAJ article page
            await res.revalidate(`/huskaj/novosti/${slug}`);
          }

          // Revalidate husKAJ news index page
          await res.revalidate("/huskaj/novosti");

          // Revalidate the rest
          const totalHuskajArticles =
            await ContentfulHelper.getTotalArticlesNumber("hušKAJ");
          const totalHuskajPages = Math.ceil(
            totalHuskajArticles / Config.pagination.pageSize
          );

          for (let page = 2; page <= totalHuskajPages; page++) {
            await res.revalidate(`/huskaj/novosti/stranica/${page}`);
          }
        }
      } catch (_) {}
    }

    return res.status(200).send("Success!");
  } else {
    return res.status(403).send("Forbidden");
  }
}

function getRawBody(req) {
  return new Promise((resolve, reject) => {
    let bodyChunks = [];
    req.on("end", () => {
      const rawBody = Buffer.concat(bodyChunks).toString("utf8");
      resolve(rawBody);
    });
    req.on("data", (chunk) => bodyChunks.push(chunk));
  });
}

export const config = {
  api: {
    bodyParser: false,
  },
};
