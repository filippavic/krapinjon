import React from "react";
import Head from "next/head";
import { motion } from "framer-motion";
import { createClient } from "contentful";
import ProjectCard from "../../components/ProjectCard";

import animations from "../../utils/otherAnimations";
import constants from "../../utils/constants";

export async function getStaticProps() {
  const client = createClient({
    space: process.env.CONTENTFUL_SPACE_ID,
    accessToken: process.env.CONTENTFUL_ACCESS_TOKEN,
    environment: process.env.NEXT_PUBLIC_ENV,
  });

  const projects = await client.getEntries({
    content_type: "project",
    select:
      "fields.slug,fields.name,fields.description,fields.thumbnail,fields.link,fields.partners,fields.accentColor,sys.contentType",
    order: "fields.order",
  });

  return {
    props: {
      projects: projects.items,
    },
  };
}

export default function Projekti({ projects }) {
  return (
    <motion.div
      className="min-h-screen flex flex-col"
      exit={{
        opacity: 0,
        transition: { duration: 0.35, ease: [0.6, 0.01, -0.05, 0.9] },
      }}
    >
      <Head>
        <title>{`Projekti | ${constants.krapinjonTitle}`}</title>

        <meta name="description" content={constants.projectsDescriptionText} />
        <meta name="copyright" content="Filip Pavić/Udruga Krapinjon" />

        {/* Theme color */}
        <meta name="theme-color" content="#151515" />
        <meta name="msapplication-navbutton-color" content="#151515" />
        <meta name="apple-mobile-web-app-status-bar-style" content="#151515" />

        {/* Open Graph / Facebook */}
        <meta property="og:type" content="website" />
        <meta
          property="og:url"
          content={"https://www.krapinjon.hr/projekti/"}
        />
        <meta
          property="og:title"
          content={`Projekti | ${constants.krapinjonTitle}`}
        />
        <meta
          property="og:description"
          content={constants.projectsDescriptionText}
        />
        <meta property="og:image" content={constants.projectsPreviewImageUrl} />

        {/* Twitter */}
        <meta name="twitter:card" content="summary_large_image" />
        <meta
          name="twitter:url"
          content={"https://www.krapinjon.hr/projekti/"}
        />
        <meta
          name="twitter:title"
          content={`Projekti | ${constants.krapinjonTitle}`}
        />
        <meta
          name="twitter:description"
          content={constants.projectsDescriptionText}
        />
        <meta
          name="twitter:image"
          content={constants.projectsPreviewImageUrl}
        />

        <link rel="icon" href="/favicon.ico" />
      </Head>

      <div className="w-full h-72 items-center justify-center flex">
        <div className="w-10/12 max-w-7xl flex overflow-hidden">
          <motion.h1
            className="flex self-center justify-center font-display text-5xl text-krapinjon-orange"
            variants={animations.titleAnimation}
            initial="initial"
            animate="animate"
          >
            Projekti
          </motion.h1>
        </div>
      </div>

      <div className="self-center mt-0 mb-20 lg:mb-28 w-10/12 max-w-7xl">
        <div className="justify-items-center">
          {projects.map((project, index) => {
            return <ProjectCard key={index} project={project} />;
          })}
        </div>
      </div>
    </motion.div>
  );
}
