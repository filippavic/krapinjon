import React from "react";
import Head from "next/head";
import Link from "next/link";
import { motion } from "framer-motion";
import { createClient } from "contentful";

import Carousel from "../components/Carousel";
import NewsCard from "../components/NewsCard";

import ArrowSmRightIcon from "@heroicons/react/20/solid/ArrowSmallRightIcon";
import constants from "../utils/constants";

export async function getStaticProps() {
  const client = createClient({
    space: process.env.CONTENTFUL_SPACE_ID,
    accessToken: process.env.CONTENTFUL_ACCESS_TOKEN,
    environment: process.env.NEXT_PUBLIC_ENV,
  });

  const events = await client.getEntries({
    content_type: "event",
    "fields.finished": "false",
    select:
      "fields.name,fields.slug,fields.type,fields.location,fields.startDateTime,fields.endDateTime,fields.allDay,fields.infoLink,fields.thumbnail,fields.accentColor,fields.finished,sys.contentType",
    order: "fields.startDateTime",
  });

  const latestNews = await client.getEntries({
    content_type: "news",
    select:
      "fields.slug,fields.title,fields.tags,fields.thumbnail,sys.contentType,sys.createdAt",
    limit: 4,
    order: "-sys.createdAt",
  });

  return {
    props: {
      events: events.items,
      latestNews: latestNews.items,
    },
  };
}

export default function Home({ events, latestNews }) {
  return (
    <motion.div
      className="min-h-screen flex flex-col justify-center items-center"
      exit={{
        opacity: 0,
        transition: { duration: 0.35, ease: [0.6, 0.01, -0.05, 0.9] },
      }}
    >
      <Head>
        <title>{constants.krapinjonTitle}</title>

        <meta name="description" content={constants.krapinjonDescriptionText} />
        <meta name="copyright" content="Filip Pavić/Udruga Krapinjon" />

        {/* Theme color */}
        <meta name="theme-color" content="#151515" />
        <meta name="msapplication-navbutton-color" content="#151515" />
        <meta name="apple-mobile-web-app-status-bar-style" content="#151515" />

        {/* Open Graph / Facebook */}
        <meta property="og:type" content="website" />
        <meta property="og:url" content={"https://www.krapinjon.hr"} />
        <meta property="og:title" content={constants.krapinjonTitle} />
        <meta
          property="og:description"
          content={constants.krapinjonDescriptionText}
        />
        <meta property="og:image" content={constants.webPreviewImageUrl} />

        {/* Twitter */}
        <meta name="twitter:card" content="summary_large_image" />
        <meta name="twitter:url" content={"https://www.krapinjon.hr"} />
        <meta name="twitter:title" content={constants.krapinjonTitle} />
        <meta
          name="twitter:description"
          content={constants.krapinjonDescriptionText}
        />
        <meta name="twitter:image" content={constants.webPreviewImageUrl} />

        <link rel="icon" href="/favicon.ico" />
      </Head>

      <div className="relative w-full h-screen flex justify-center items-center">
        <Carousel events={events} />
      </div>

      <motion.div className="w-10/12 max-w-7xl flex flex-col md:flex-row justify-between items-center mt-20 md:mt-24">
        <motion.h1
          className="font-display text-6xl text-krapinjon-orange w-fit md:w-min text-left"
          initial={{ opacity: 0, y: 10 }}
          whileInView={{
            opacity: 1,
            y: 0,
            transition: {
              duration: 1,
              ease: [0.6, 0.01, -0.05, 0.9],
            },
          }}
          viewport={{ margin: "-70px 0px -70px 0px", once: true }}
        >
          Udruga Krapinjon
        </motion.h1>

        <motion.div className="flex w-full md:w-1/2 mt-10 md:mt-0 flex-col items-start md:items-end">
          <motion.p
            className="font-normal text-base text-beige text-left md:text-right"
            initial={{
              opacity: 0,
              x: 20,
              scale: 1.02,
            }}
            whileInView={{
              opacity: 1,
              x: 0,
              scale: 1,
              transition: {
                duration: 1,
                ease: [0.6, 0.01, -0.05, 0.9],
                delay: 0.2,
              },
            }}
            viewport={{ margin: "-70px 0px -70px 0px", once: true }}
          >
            {constants.krapinjonDescriptionText}
          </motion.p>
          <Link href="/informacije" prefetch={false} passHref legacyBehavior>
            <motion.a
              className="group flex flex-row items-center cursor-pointer mt-5"
              initial={{ opacity: 0, x: 10 }}
              whileInView={{
                opacity: 1,
                x: 0,
                transition: {
                  duration: 1,
                  ease: [0.6, 0.01, -0.05, 0.9],
                  delay: 0.3,
                },
              }}
              viewport={{ margin: "-70px 0px -70px 0px", once: true }}
            >
              <span className="text-sm font-semibold transition-all duration-500 ease-in-out text-beige">
                Više o udruzi
              </span>
              <div className="ml-1.5 flex rounded-full h-5 w-5 items-center justify-center transition-all duration-500 ease-in-out group-hover:scale-110  group-hover:ml-3 group-hover:mr-3 bg-medium-gray group-hover:bg-beige">
                <ArrowSmRightIcon className="h-3 w-3 text-beige group-hover:text-medium-gray transition duration-500 ease-in-out" />
              </div>
            </motion.a>
          </Link>
        </motion.div>
      </motion.div>

      <div className="self-center mt-44 w-10/12 max-w-7xl">
        <motion.h1
          className="flex font-display text-5xl text-krapinjon-orange"
          initial={{ opacity: 0, y: 10 }}
          whileInView={{
            opacity: 1,
            y: 0,
            transition: {
              duration: 1,
              ease: [0.6, 0.01, -0.05, 0.9],
            },
          }}
          viewport={{ margin: "-70px 0px -70px 0px", once: true }}
        >
          Novosti
        </motion.h1>

        <div className="justify-items-center mt-10">
          {/* Highlighted news card */}
          {latestNews && latestNews.length > 0 && (
            <NewsCard
              key={0}
              article={latestNews[0]}
              index={0}
              highlightImage={true}
            />
          )}

          {/* Other news cards */}
          <div className="w-full grid grid-cols-1 md:grid-cols-3 gap-10 mt-10">
            {latestNews &&
              latestNews.length > 1 &&
              latestNews.slice(1).map((article, index) => {
                return (
                  <NewsCard
                    key={index + 1}
                    article={article}
                    index={index + 1}
                    highlightImage={false}
                  />
                );
              })}
          </div>

          {latestNews.length == 0 && (
            <motion.p
              className="flex text-base font-semibold text-beige-dark"
              initial={{ opacity: 0, y: 10 }}
              whileInView={{
                opacity: 1,
                y: 0,
                transition: {
                  duration: 1,
                  ease: [0.6, 0.01, -0.05, 0.9],
                },
              }}
              viewport={{ margin: "-70px 0px -70px 0px", once: true }}
            >
              Trenutno nema novosti.
            </motion.p>
          )}
        </div>

        {latestNews.length != 0 && (
          <Link href="/novosti" prefetch={false} passHref legacyBehavior>
            <motion.a
              className="group flex flex-row items-center cursor-pointer mt-10 max-w-fit"
              initial={{ opacity: 0, x: -10 }}
              whileInView={{
                opacity: 1,
                x: 0,
                transition: {
                  duration: 1,
                  ease: [0.6, 0.01, -0.05, 0.9],
                },
              }}
              viewport={{ margin: "-70px 0px -70px 0px", once: true }}
            >
              <span className="text-sm font-semibold transition-all duration-500 ease-in-out text-beige pl-0 group-hover:pl-2">
                Više novosti
              </span>
              <div className="ml-1.5 flex rounded-full h-5 w-5 items-center justify-center transition-all group-hover:ml-3 duration-500 ease-in-out group-hover:scale-110 bg-medium-gray group-hover:bg-beige">
                <ArrowSmRightIcon className="h-3 w-3 text-beige group-hover:text-medium-gray transition duration-500 ease-in-out" />
              </div>
            </motion.a>
          </Link>
        )}
      </div>
    </motion.div>
  );
}
