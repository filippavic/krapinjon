import React from "react";
import { motion } from "framer-motion";
import { createClient } from "contentful";

// Components
import ArticleScreen from "../../components/screens/news/ArticleScreen";

// Functions and helpers
import constants from "../../utils/constants";
import CustomHead from "../../components/head/CustomHead";

// Contentful client
const client = createClient({
  space: process.env.CONTENTFUL_SPACE_ID,
  accessToken: process.env.CONTENTFUL_ACCESS_TOKEN,
  environment: process.env.NEXT_PUBLIC_ENV,
});

export async function getStaticPaths() {
  const res = await client.getEntries({ content_type: "news" });

  const paths = res.items.map((item) => {
    return {
      params: { slug: item.fields.slug },
    };
  });

  return {
    paths,
    fallback: "blocking",
  };
}

export async function getStaticProps({ params }) {
  const { items } = await client.getEntries({
    content_type: "news",
    "fields.slug": params.slug,
  });

  if (!items[0]) {
    return {
      notFound: true,
    };
  }

  return {
    props: { article: items[0] },
  };
}

export default function NewsArticle({ article }) {
  // Article data
  const { title, slug, thumbnail } = article.fields;

  const articleData = article.fields;
  const publishDateTime = article.sys.createdAt;
  let url = "https://www.krapinjon.hr/novosti/" + slug;

  // Meta tags
  let metaTags = {
    title: `${title} | ${constants.krapinjonTitle}`,
    url: url,
    previewImageUrl: thumbnail[0].original_secure_url,
    accentColor: "#151515",
  };

  return (
    <motion.div
      className="min-h-screen flex flex-col"
      exit={{
        opacity: 0,
        transition: { duration: 0.35, ease: [0.6, 0.01, -0.05, 0.9] },
      }}
    >
      <CustomHead metaTags={metaTags} />

      <ArticleScreen
        article={articleData}
        publishDateTime={publishDateTime}
        url={url}
      />
    </motion.div>
  );
}
