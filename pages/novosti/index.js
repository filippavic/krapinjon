import React from "react";
import { motion } from "framer-motion";

// Components
import NewsScreen from "../../components/screens/news/NewsScreen";

// Functions and helpers
import ContentfulHelper from "../../utils/contentfulHelper";
import { Config } from "../../utils/config";
import constants from "../../utils/constants";
import CustomHead from "../../components/head/CustomHead";

export async function getStaticProps() {
  const res = await ContentfulHelper.getArticlePage(1);

  const totalPages = Math.ceil(res.totalCount / Config.pagination.pageSize);

  return {
    props: {
      news: res.articles,
      totalPages,
      currentPage: "1",
    },
  };
}

export default function Novosti({ news, totalPages, currentPage }) {
  // Meta tags
  let metaTags = {
    title: `Novosti (stranica ${currentPage}) | ${constants.krapinjonTitle}`,
    description: constants.newsDescriptionText,
    url: "https://www.krapinjon.hr/novosti",
    previewImageUrl: constants.newsPreviewImageUrl,
    accentColor: "#151515",
  };

  return (
    <motion.div
      className="min-h-screen flex flex-col content-center"
      exit={{
        opacity: 0,
        transition: { duration: 0.35, ease: [0.6, 0.01, -0.05, 0.9] },
      }}
    >
      <CustomHead metaTags={metaTags} />

      <NewsScreen
        news={news}
        totalPages={totalPages}
        currentPage={currentPage}
      />
    </motion.div>
  );
}
