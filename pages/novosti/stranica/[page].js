import React from "react";
import { motion } from "framer-motion";

// Components
import NewsScreen from "../../../components/screens/news/NewsScreen";

// Functions and helpers
import ContentfulHelper from "../../../utils/contentfulHelper";
import { Config } from "../../../utils/config";
import constants from "../../../utils/constants";
import CustomHead from "../../../components/head/CustomHead";

export async function getStaticPaths() {
  const totalArticles = await ContentfulHelper.getTotalArticlesNumber();
  const totalPages = Math.ceil(totalArticles / Config.pagination.pageSize);

  const paths = [];

  for (let page = 2; page <= totalPages; page++) {
    paths.push({ params: { page: page.toString() } });
  }

  return {
    paths,
    fallback: "blocking",
  };
}

export async function getStaticProps({ params }) {
  const res = await ContentfulHelper.getArticlePage(params.page);

  const totalPages = Math.ceil(res.totalCount / Config.pagination.pageSize);

  if (params.page <= 0 || params.page > totalPages) {
    return {
      notFound: true,
    };
  }

  return {
    props: {
      news: res.articles,
      totalPages,
      currentPage: params.page,
    },
  };
}

export default function Novosti({ news, totalPages, currentPage }) {
  // Meta tags
  let metaTags = {
    title: `Novosti (stranica ${currentPage}) | ${constants.krapinjonTitle}`,
    description: constants.newsDescriptionText,
    url: `https://www.krapinjon.hr/novosti/stranica/${currentPage}`,
    previewImageUrl: constants.newsPreviewImageUrl,
    accentColor: "#151515",
  };

  return (
    <motion.div
      className="min-h-screen flex flex-col"
      exit={{
        opacity: 0,
        transition: { duration: 0.35, ease: [0.6, 0.01, -0.05, 0.9] },
      }}
    >
      <CustomHead metaTags={metaTags} />

      <NewsScreen
        news={news}
        totalPages={totalPages}
        currentPage={currentPage}
      />
    </motion.div>
  );
}
