import React from "react";
import Head from "next/head";
import Link from "next/link";
import { motion } from "framer-motion";

import animations from "../utils/otherAnimations";
import constants from "../utils/constants";

export default function NotFound() {
  return (
    <motion.div
      className="min-h-screen flex flex-col"
      exit={{
        opacity: 0,
        transition: { duration: 0.35, ease: [0.6, 0.01, -0.05, 0.9] },
      }}
    >
      <Head>
        <title>{`Stranica ne postoji | ${constants.krapinjonTitle}`}</title>

        <meta name="description" content={constants.krapinjonDescriptionText} />
        <meta name="copyright" content="Filip Pavić/Udruga Krapinjon" />

        {/* Theme color */}
        <meta name="theme-color" content="#151515" />
        <meta name="msapplication-navbutton-color" content="#151515" />
        <meta name="apple-mobile-web-app-status-bar-style" content="#151515" />

        {/* Open Graph / Facebook */}
        <meta property="og:type" content="website" />
        <meta property="og:url" content={"https://www.krapinjon.hr/404"} />
        <meta
          property="og:title"
          content={`Stranica ne postoji | ${constants.krapinjonTitle}`}
        />
        <meta
          property="og:description"
          content={constants.krapinjonDescriptionText}
        />
        <meta property="og:image" content={constants.webPreviewImageUrl} />

        {/* Twitter */}
        <meta name="twitter:card" content="summary_large_image" />
        <meta name="twitter:url" content={"https://www.krapinjon.hr/404"} />
        <meta
          name="twitter:title"
          content={`Stranica ne postoji | ${constants.krapinjonTitle}`}
        />
        <meta
          name="twitter:description"
          content={constants.krapinjonDescriptionText}
        />
        <meta name="twitter:image" content={constants.webPreviewImageUrl} />

        <link rel="icon" href="/favicon.ico" />
      </Head>

      <div className="w-full h-screen flex flex-col justify-evenly content-center">
        <div className="w-full flex flex-col items-center">
          <motion.h1
            className="font-display text-5xl sm:text-6xl text-beige-dark"
            variants={animations.titleAnimation}
            initial="initial"
            animate="animate"
          >
            404
          </motion.h1>

          <motion.h2
            className="font-semibold text-base sm:text-lg md:text-2xl mt-7 text-krapinjon-orange text-center"
            variants={animations.titleAnimation}
            initial="initial"
            animate="animate"
          >
            Izgleda da ova stranica ne postoji.
          </motion.h2>
        </div>

        <div className="w-full flex flex-col items-center">
          <motion.h2
            className="font-medium text-base sm:text-lg text-beige"
            variants={animations.titleAnimation}
            initial="initial"
            animate="animate"
          >
            Ali zato postoje ove:
          </motion.h2>

          <motion.div className="flex flex-col md:flex-row max-w-xs w-1/2 justify-evenly items-center mt-7">
            <Link href="/" prefetch={false} passHref legacyBehavior>
              <motion.a
                className="cursor-pointer font-display text-base text-beige hover:text-krapinjon-orange transition ease-easeAlt2 duration-300"
                initial="initial"
                animate="animate"
                variants={animations.titleAnimation}
              >
                Naslovna
              </motion.a>
            </Link>
            <Link href="/projekti" prefetch={false} passHref legacyBehavior>
              <motion.a
                className="mt-2 md:mt-0 cursor-pointer font-display text-base text-beige hover:text-krapinjon-orange transition ease-easeAlt2 duration-300"
                initial="initial"
                animate="animate"
                variants={animations.titleAnimation}
              >
                Projekti
              </motion.a>
            </Link>
            <Link href="/novosti" prefetch={false} passHref legacyBehavior>
              <motion.a
                className="mt-2 md:mt-0 cursor-pointer font-display text-base text-beige hover:text-krapinjon-orange transition ease-easeAlt2 duration-300"
                initial="initial"
                animate="animate"
                variants={animations.titleAnimation}
              >
                Novosti
              </motion.a>
            </Link>
            <Link href="/informacije" prefetch={false} passHref legacyBehavior>
              <motion.a
                initial="initial"
                animate="animate"
                className="mt-2 md:mt-0 cursor-pointer font-display text-base text-beige hover:text-krapinjon-orange transition ease-easeAlt2 duration-300"
                variants={animations.titleAnimation}
              >
                Informacije
              </motion.a>
            </Link>
          </motion.div>
        </div>
      </div>
    </motion.div>
  );
}
