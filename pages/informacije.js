import React from "react";
import Head from "next/head";
import Image from "next/image";
import { motion, useReducedMotion } from "framer-motion";
import { createClient } from "contentful";
import { documentToReactComponents } from "@contentful/rich-text-react-renderer";
import { BLOCKS, MARKS, INLINES } from "@contentful/rich-text-types";

import { getCloudinaryThumbLink } from "../utils/helperFunctions";

import AnimatedWords from "../components/AnimatedWords";
import constants from "../utils/constants";

// Rich text component customization
const Heading1 = ({ children }) => (
  <motion.h1
    className="font-bold text-krapinjon-orange text-lg"
    initial={{ opacity: 0, x: 20 }}
    whileInView={{
      opacity: 1,
      x: 0,
      transition: {
        duration: 1,
        ease: [0.6, 0.01, -0.05, 0.9],
      },
    }}
    viewport={{ margin: "-70px 0px -70px 0px", once: true }}
  >
    {children}
  </motion.h1>
);

const Heading2 = ({ children }) => (
  <motion.h2
    className="font-bold text-beige-dark text-base"
    initial={{ opacity: 0, x: 15 }}
    whileInView={{
      opacity: 1,
      x: 0,
      transition: {
        duration: 1,
        ease: [0.6, 0.01, -0.05, 0.9],
      },
    }}
    viewport={{ margin: "-70px 0px -70px 0px", once: true }}
  >
    {children}
  </motion.h2>
);

const Heading3 = ({ children }) => (
  <motion.h3
    className="font-semibold text-beige text-sm"
    initial={{ opacity: 0, x: 10 }}
    whileInView={{
      opacity: 1,
      x: 0,
      transition: {
        duration: 1,
        ease: [0.6, 0.01, -0.05, 0.9],
      },
    }}
    viewport={{ margin: "-70px 0px -70px 0px", once: true }}
  >
    {children}
  </motion.h3>
);

const Bold = ({ children }) => (
  <span className="font-semibold text-krapinjon-orange">{children}</span>
);

const Text = ({ children }) => (
  <motion.p
    className="align-center text-beige py-3 font-medium tracking-krapinjon-wide"
    initial={{ opacity: 0, x: 10 }}
    whileInView={{
      opacity: 1,
      x: 0,
      transition: {
        duration: 1,
        ease: [0.6, 0.01, -0.05, 0.9],
      },
    }}
    viewport={{ margin: "-70px 0px -70px 0px", once: true }}
  >
    {children}
  </motion.p>
);

const Quote = ({ children }) => (
  <motion.blockquote
    className="py-5 px-5 align-center"
    initial={{ opacity: 0, x: 10 }}
    whileInView={{
      opacity: 1,
      x: 0,
      transition: {
        duration: 1,
        ease: [0.6, 0.01, -0.05, 0.9],
      },
    }}
    viewport={{ margin: "-70px 0px -70px 0px", once: true }}
  >
    {children}
  </motion.blockquote>
);

const textRenderOptions = {
  renderMark: {
    [MARKS.BOLD]: function BoldText(text) {
      return <Bold>{text}</Bold>;
    },
  },
  renderNode: {
    [BLOCKS.PARAGRAPH]: function Paragraph(node, children) {
      return <Text>{children}</Text>;
    },
    [BLOCKS.HEADING_1]: function H1(node, children) {
      return <Heading1>{children}</Heading1>;
    },
    [BLOCKS.HEADING_2]: function H2(node, children) {
      return <Heading2>{children}</Heading2>;
    },
    [BLOCKS.HEADING_3]: function H3(node, children) {
      return <Heading3>{children}</Heading3>;
    },
    [BLOCKS.QUOTE]: function Q(node, children) {
      return <Quote>{children}</Quote>;
    },
    [INLINES.HYPERLINK]: function Hyperlink({ data }, children) {
      return (
        <a
          className="text-krapinjon-orange underline decoration-wavy hover:text-krapinjon-orange-dark transition duration-300 ease-in-out"
          href={data.uri}
          target="_blank"
          rel="noopener noreferrer"
        >
          {children}
        </a>
      );
    },
    [BLOCKS.UL_LIST]: function UlList(node, children) {
      return (
        <motion.ul
          className="list-disc list-outside pl-12"
          initial={{ opacity: 0, x: 10 }}
          whileInView={{
            opacity: 1,
            x: 0,
            transition: {
              duration: 1,
              ease: [0.6, 0.01, -0.05, 0.9],
            },
          }}
          viewport={{ margin: "-70px 0px -70px 0px", once: true }}
        >
          {children}
        </motion.ul>
      );
    },
    [BLOCKS.OL_LIST]: function OlList(node, children) {
      return (
        <motion.ul
          className="list-decimal list-outside pl-12"
          initial={{ opacity: 0, x: 10 }}
          whileInView={{
            opacity: 1,
            x: 0,
            transition: {
              duration: 1,
              ease: [0.6, 0.01, -0.05, 0.9],
            },
          }}
          viewport={{ margin: "-70px 0px -70px 0px", once: true }}
        >
          {children}
        </motion.ul>
      );
    },
    [BLOCKS.LIST_ITEM]: function ListItem(node, children) {
      return (
        <li className="list-item text-krapinjon-orange leading-none">
          {children}
        </li>
      );
    },
    [BLOCKS.HR]: function Hr(node, children) {
      return (
        <motion.div
          className="border-t border-krapinjon-orange my-9"
          initial={{ opacity: 0, scaleX: 0.7 }}
          whileInView={{
            opacity: 0.4,
            scaleX: 1,
            transition: {
              duration: 1,
              ease: [0.6, 0.01, -0.05, 0.9],
            },
          }}
          viewport={{ margin: "-70px 0px -70px 0px" }}
        ></motion.div>
      );
    },
  },
};

export async function getStaticProps() {
  const client = createClient({
    space: process.env.CONTENTFUL_SPACE_ID,
    accessToken: process.env.CONTENTFUL_ACCESS_TOKEN,
    environment: process.env.NEXT_PUBLIC_ENV,
  });

  const res = await client.getEntries({
    content_type: "about",
    select:
      "fields.information,fields.documents,fields.financialReports,fields.reports,fields.sponsors,fields.thumbnail,sys.contentType",
    limit: 1,
  });

  return {
    props: {
      info: res.items[0],
    },
  };
}

export default function Informacije({ info }) {
  // Info page data
  const {
    information,
    documents,
    financialReports,
    reports,
    sponsors,
    thumbnail,
  } = info.fields;

  let thumbLink = getCloudinaryThumbLink(thumbnail[0].original_secure_url);

  // Resize image based on scroll postion
  // Removed for performance reasons
  // const { scrollYProgress } = useScroll();
  // const imageScroll = useTransform(scrollYProgress, [0, 1], [1, 1.2]);

  // Accessibility
  const shouldReduceMotion = useReducedMotion();

  return (
    <motion.div
      className="min-h-screen flex flex-col"
      exit={{
        opacity: 0,
        transition: { duration: 0.35, ease: [0.6, 0.01, -0.05, 0.9] },
      }}
    >
      <Head>
        <title>{`Informacije | ${constants.krapinjonTitle}`}</title>

        <meta name="description" content={constants.krapinjonDescriptionText} />
        <meta name="copyright" content="Filip Pavić/Udruga Krapinjon" />

        {/* Theme color */}
        <meta name="theme-color" content="#151515" />
        <meta name="msapplication-navbutton-color" content="#151515" />
        <meta name="apple-mobile-web-app-status-bar-style" content="#151515" />

        {/* Open Graph / Facebook */}
        <meta property="og:type" content="website" />
        <meta
          property="og:url"
          content={"https://www.krapinjon.hr/informacije"}
        />
        <meta
          property="og:title"
          content={`Informacije | ${constants.krapinjonTitle}`}
        />
        <meta
          property="og:description"
          content={constants.krapinjonDescriptionText}
        />
        <meta property="og:image" content={constants.webPreviewImageUrl} />

        {/* Twitter */}
        <meta name="twitter:card" content="summary_large_image" />
        <meta
          name="twitter:url"
          content={"https://www.krapinjon.hr/informacije"}
        />
        <meta
          name="twitter:title"
          content={`Informacije | ${constants.krapinjonTitle}`}
        />
        <meta
          name="twitter:description"
          content={constants.krapinjonDescriptionText}
        />
        <meta name="twitter:image" content={constants.webPreviewImageUrl} />

        <link rel="icon" href="/favicon.ico" />
      </Head>

      <motion.div className="absolute w-full h-4/5 overflow-hidden">
        <div className="absolute z-20 w-full h-full bg-gradient-to-b from-transparent to-background-black"></div>
        <div className="absolute z-10 w-full h-full bg-krapinjon-orange mix-blend-multiply"></div>

        <div className="absolute z-10 w-full h-full bg-texture-image mix-blend-multiply"></div>

        <motion.div
          className="w-full h-full relative"
          initial={{
            opacity: 0,
            y: 40,
          }}
          animate={{
            opacity: 1,
            y: 0,
            transition: {
              duration: 1,
              ease: [0.6, 0.01, -0.05, 0.9],
            },
          }}
          // Removed for performance reasons
          // style={{
          //   scale: shouldReduceMotion ? 1 : imageScroll,
          // }}
        >
          <Image
            src={thumbnail[0].original_secure_url}
            blurDataURL={thumbLink}
            placeholder="blur"
            fill
            style={{ objectFit: "cover" }}
            alt=""
            quality={68}
            className="grayscale"
          />
        </motion.div>
      </motion.div>

      <motion.div className="self-center mt-h-1/3 w-10/12 rounded-xl max-w-screen-2xl">
        <motion.div className="flex flex-col relative w-full h-80 justify-end overflow-hidden">
          <AnimatedWords title="Informacije o udruzi" />
        </motion.div>
        <div className="flex flex-row w-full h-auto relative z-50 mt-24">
          <div className="flex flex-col w-full">
            <motion.div
              initial={{ opacity: 0, y: 15 }}
              animate={{
                opacity: 1,
                y: 0,
                transition: {
                  duration: 1,
                  ease: [0.6, 0.01, -0.05, 0.9],
                },
              }}
            >
              {documentToReactComponents(information, textRenderOptions)}
            </motion.div>
          </div>
        </div>

        {documents !== undefined ? (
          <div className="mt-20">
            <motion.span
              className="text-sm text-beige font-semibold inline-block"
              initial={{ opacity: 0, x: -10 }}
              whileInView={{
                opacity: 1,
                x: 0,
                transition: {
                  duration: 1,
                  ease: [0.6, 0.01, -0.05, 0.9],
                },
              }}
              viewport={{ margin: "-70px 0px -70px 0px", once: true }}
            >
              Dokumenti
            </motion.span>
            <div className="mt-1">
              {documents.map((document, index) => {
                return (
                  <motion.a
                    className="text-beige-dark font-semibold hover:text-krapinjon-orange transition duration-300 ease-in-out block w-fit"
                    href={"https:" + document.fields.file.url}
                    key={index}
                    target="_blank"
                    rel="noreferrer"
                    initial={{ opacity: 0, x: -10 }}
                    whileInView={{
                      opacity: 1,
                      x: 0,
                      transition: {
                        duration: 0.5,
                        ease: [0.6, 0.01, -0.05, 0.9],
                      },
                    }}
                    viewport={{ margin: "-70px 0px -70px 0px", once: true }}
                  >
                    {`${document.fields.title} (.${document.fields.file.fileName
                      .split(".")
                      .pop()})`}
                  </motion.a>
                );
              })}
            </div>
          </div>
        ) : (
          <></>
        )}

        {financialReports !== undefined ? (
          <div className="mt-20">
            <motion.span
              className="text-sm text-beige font-semibold inline-block"
              initial={{ opacity: 0, x: -10 }}
              whileInView={{
                opacity: 1,
                x: 0,
                transition: {
                  duration: 1,
                  ease: [0.6, 0.01, -0.05, 0.9],
                },
              }}
              viewport={{ margin: "-70px 0px -70px 0px", once: true }}
            >
              Financijska izvješća
            </motion.span>
            <div className="mt-1">
              {financialReports.map((document, index) => {
                return (
                  <motion.a
                    className="text-beige-dark font-semibold hover:text-krapinjon-orange transition duration-300 ease-in-out block w-fit"
                    href={"https:" + document.fields.file.url}
                    key={index}
                    target="_blank"
                    rel="noreferrer"
                    initial={{ opacity: 0, x: -10 }}
                    whileInView={{
                      opacity: 1,
                      x: 0,
                      transition: {
                        duration: 0.5,
                        ease: [0.6, 0.01, -0.05, 0.9],
                      },
                    }}
                    viewport={{ margin: "-70px 0px -70px 0px", once: true }}
                  >
                    {`${document.fields.title} (.${document.fields.file.fileName
                      .split(".")
                      .pop()})`}
                  </motion.a>
                );
              })}
            </div>
          </div>
        ) : (
          <></>
        )}

        {reports !== undefined ? (
          <div className="mt-20">
            <motion.span
              className="text-sm text-beige font-semibold inline-block"
              initial={{ opacity: 0, x: -10 }}
              whileInView={{
                opacity: 1,
                x: 0,
                transition: {
                  duration: 1,
                  ease: [0.6, 0.01, -0.05, 0.9],
                },
              }}
              viewport={{ margin: "-70px 0px -70px 0px", once: true }}
            >
              Ostala izvješća
            </motion.span>
            <div className="mt-1">
              {reports.map((document, index) => {
                return (
                  <motion.a
                    className="text-beige-dark font-semibold hover:text-krapinjon-orange transition duration-300 ease-in-out block w-fit"
                    href={"https:" + document.fields.file.url}
                    key={index}
                    target="_blank"
                    rel="noreferrer"
                    initial={{ opacity: 0, x: -10 }}
                    whileInView={{
                      opacity: 1,
                      x: 0,
                      transition: {
                        duration: 0.5,
                        ease: [0.6, 0.01, -0.05, 0.9],
                      },
                    }}
                    viewport={{ margin: "-70px 0px -70px 0px", once: true }}
                  >
                    {`${document.fields.title} (.${document.fields.file.fileName
                      .split(".")
                      .pop()})`}
                  </motion.a>
                );
              })}
            </div>
          </div>
        ) : (
          <></>
        )}

        {sponsors !== undefined ? (
          <div className="mt-20">
            <motion.span
              className="text-sm text-beige font-semibold inline-block"
              initial={{ opacity: 0, x: -10 }}
              whileInView={{
                opacity: 1,
                x: 0,
                transition: {
                  duration: 1,
                  ease: [0.6, 0.01, -0.05, 0.9],
                },
              }}
              viewport={{ margin: "-70px 0px -70px 0px", once: true }}
            >
              Podržavatelji
            </motion.span>

            <div className="grid grid-cols-repeat gap-4 mt-5">
              {sponsors.map((image, index) => {
                return (
                  <motion.div
                    className="relative max-w-sm h-32 w-full rounded-lg"
                    key={index}
                    initial={{ opacity: 0, y: 10 }}
                    whileInView={{
                      opacity: 1,
                      y: 0,
                      transition: {
                        duration: 1.5,
                        ease: [0.6, 0.01, -0.05, 0.9],
                        delay: 0.05 * index,
                      },
                    }}
                    viewport={{
                      margin: "-100px 0px -100px 0px",
                      once: true,
                    }}
                  >
                    <Image
                      src={image.original_secure_url}
                      width={image.width}
                      height={image.height}
                      sizes="384px"
                      style={{
                        objectFit: "contain",
                        width: "100%",
                        height: "100%",
                      }}
                      alt=""
                      className="rounded-lg"
                    />
                  </motion.div>
                );
              })}
            </div>
          </div>
        ) : (
          <></>
        )}
      </motion.div>
    </motion.div>
  );
}
