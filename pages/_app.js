import { AnimatePresence } from "framer-motion";
import localFont from "next/font/local";

import useScrollRestoration from "../utils/hooks/useScrollRestoration";

import MainLayout from "../components/layouts/MainLayout";

import "../styles/globals.css";

// Load local fonts
const tankerFont = localFont({
  src: "../public/fonts/Tanker/Tanker-Regular.woff2",
  weight: "400",
  variable: "--font-tanker",
  display: "swap",
});
const clashGroteskFont = localFont({
  src: "../public/fonts/ClashGrotesk/ClashGrotesk-Variable.woff2",
  variable: "--font-clashGrotesk",
  display: "swap",
});

function MyApp({ Component, pageProps, router }) {
  useScrollRestoration(router);

  // Render a custom layout if it exists
  const renderWithLayout = function (page) {
    // Font classnames
    const classNames = `${clashGroteskFont.className} ${tankerFont.variable}`;

    if (Component.getLayout != null) {
      return Component.getLayout(page, classNames);
    } else {
      return (
        <main className={classNames}>
          <MainLayout>{page}</MainLayout>
        </main>
      );
    }
  };

  return renderWithLayout(
    <AnimatePresence exitBeforeEnter>
      <Component {...pageProps} key={router.route} />
    </AnimatePresence>
  );
}

export default MyApp;
