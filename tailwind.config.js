const defaultTheme = require("tailwindcss/defaultTheme");

const krakonPurple = "#861FC5";

module.exports = {
  content: [
    "./pages/**/*.{js,ts,jsx,tsx}",
    "./components/**/*.{js,ts,jsx,tsx}",
  ],
  theme: {
    fontFamily: {
      sans: ["var(--font-clashGrotesk)", ...defaultTheme.fontFamily.sans],
      display: ["var(--font-tanker)", ...defaultTheme.fontFamily.sans],
    },
    extend: {
      screens: {
        xs: "390px",
        md2: "824px",
      },
      fontSize: {
        xxs: ".6rem",
        huge: "10rem",
        large: "5rem",
        massive: "20rem",
      },
      colors: {
        "krapinjon-orange": "#FC8A17",
        "krapinjon-orange-dark": "#C56C12",
        "dark-gray": "#1C1C1C",
        "medium-gray": "#454545",
        "light-gray": "#858585",
        beige: "#EBD6C2",
        "beige-dark": "#AB967E",
        "background-gray": "#151515",
        "background-black": "#0A0A0A",
        "krakon-purple": "#5A4BC3",
        "krakon-purple-mid": "#4436A3",
        "krakon-purple-dark": "#34297E",
        "huskaj-red": "#DC143C",
        "huskaj-red-dark": "#8a1129",
      },
      gridTemplateColumns: {
        repeat: "repeat(auto-fill, minmax(12rem, 1fr))",
      },
      zIndex: {
        50: 50,
        75: 75,
        100: 100,
      },
      spacing: {
        "s-9/20": "45vw",
        "s-1/2": "50vw",
        "h-1/2": "50vh",
        "h-1/3": "33vh",
        "h-2/5": "40vh",
        "h-1/4": "25vh",
        100: "28rem",
        110: "34rem",
        120: "40rem",
      },
      transitionTimingFunction: {
        easeAlt: "cubic-bezier(0.54, 1.6, 0.5, 1)",
        easeAlt2: "cubic-bezier(0.145, 0.045, 0.155, 1)",
      },
      backgroundImage: {
        "texture-light": "url('/images/textures/texture_light.png')",
        "texture-image": "url('/images/textures/texture_image.jpg')",
      },
      letterSpacing: {
        "krapinjon-wide": "0.015em",
      },
    },
  },
  plugins: [require("tailwindcss-text-border")],
};
