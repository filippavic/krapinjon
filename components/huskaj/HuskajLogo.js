import Link from "next/link";
import Image from "next/image";

import huskajLogo from "../../public/images/huskaj/huskaj_logo.png";

export default function HuskajLogo({ closeMenu }) {
  return (
    <Link
      href="/huskaj"
      prefetch={false}
      passHref={true}
      aria-label="hušKAJ!"
      onClick={closeMenu}
    >
      <Image
        src={huskajLogo}
        sizes="64px"
        alt=""
        priority
        quality={68}
        style={{ objectFit: "cover" }}
      />
    </Link>
  );
}
