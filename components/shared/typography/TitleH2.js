import { motion } from "framer-motion";

export default function TitleH2({ text, icon }) {
  return (
    <motion.h2
      className="z-50 flex text-base sm:text-xl font-bold text-beige items-center"
      initial={{ opacity: 0, y: 15 }}
      animate={{
        opacity: 1,
        y: 0,
        transition: {
          duration: 0.7,
          ease: [0.6, 0.01, -0.05, 0.9],
          delay: 0.1,
        },
      }}
    >
      {icon}
      {text}
    </motion.h2>
  );
}
