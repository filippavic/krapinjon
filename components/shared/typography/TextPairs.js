import { motion } from "framer-motion";
import { accentColorVariants } from "../../../styles/colorVariants";

export default function TextPairs({ id, title, text, variant }) {
  return (
    <motion.div
      key={id}
      className="z-50 flex flex-col md:flex-row items-center mt-8 md:mt-2 justify-center"
      initial={{ opacity: 0, y: -10 }}
      whileInView={{
        opacity: 1,
        y: 0,
        transition: {
          duration: 0.5,
          ease: [0.6, 0.01, -0.05, 0.9],
        },
      }}
      viewport={{ margin: "-70px 0px -70px 0px", once: true }}
    >
      <motion.span className="text-base text-beige text-center font-semibold pr-1">
        {title}
      </motion.span>
      <motion.span
        className={`text-base text-center ${
          accentColorVariants[variant || "default"]
        } font-semibold`}
      >
        {text}
      </motion.span>
    </motion.div>
  );
}
