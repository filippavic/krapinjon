import { motion } from "framer-motion";

export default function ScrollSpanTiny({ text }) {
  return (
    <motion.span
      className="mt-5 text-xxs text-beige-dark"
      initial={{ opacity: 0, x: 10 }}
      whileInView={{
        opacity: 1,
        x: 0,
        transition: {
          duration: 1,
          ease: [0.6, 0.01, -0.05, 0.9],
        },
      }}
      viewport={{ margin: "-70px 0px -70px 0px", once: true }}
    >
      {text}
    </motion.span>
  );
}
