import { motion } from "framer-motion";

export default function ScrollParagraph({ text }) {
  return (
    <motion.p
      className={`flex text-base font-semibold text-beige-dark`}
      initial={{ opacity: 0, y: 10 }}
      whileInView={{
        opacity: 1,
        y: 0,
        transition: {
          duration: 1,
          ease: [0.6, 0.01, -0.05, 0.9],
        },
      }}
      viewport={{ margin: "-70px 0px -70px 0px", once: true }}
    >
      {text}
    </motion.p>
  );
}
