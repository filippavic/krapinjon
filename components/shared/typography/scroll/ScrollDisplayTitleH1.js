import { motion } from "framer-motion";
import { accentColorVariants } from "../../../../styles/colorVariants";

export default function ScrollDisplayTitleH1({ title, variant }) {
  return (
    <motion.h1
      className={`flex font-display text-5xl ${
        accentColorVariants[variant || "default"]
      }`}
      initial={{ opacity: 0, y: 10 }}
      whileInView={{
        opacity: 1,
        y: 0,
        transition: {
          duration: 1,
          ease: [0.6, 0.01, -0.05, 0.9],
        },
      }}
      viewport={{ margin: "-70px 0px -70px 0px", once: true }}
    >
      {title}
    </motion.h1>
  );
}
