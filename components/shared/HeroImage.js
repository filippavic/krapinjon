import Image from "next/image";
import {
  motion,
  //   useReducedMotion,
  //   useScroll,
  //   useTransform,
} from "framer-motion";
import { bgColorVariants } from "../../styles/colorVariants";

function ImageComp({ imageSrc, imageBlurUrl }) {
  return (
    <Image
      src={imageSrc}
      blurDataURL={imageBlurUrl}
      placeholder="blur"
      fill
      style={{ objectFit: "cover" }}
      alt=""
      quality={68}
      className="grayscale"
    />
  );
}

export default function HeroImage({ imageSrc, imageBlurUrl, variant }) {
  // Resize image based on scroll postion
  // Removed for performance reasons
  //   const { scrollYProgress } = useScroll();
  //   const imageScroll = useTransform(scrollYProgress, [0, 1], [1, 1.2]);

  //   const shouldReduceMotion = useReducedMotion();

  return (
    <>
      <div className="absolute z-20 w-full h-full bg-gradient-to-b from-transparent to-background-black"></div>
      <div
        className={`absolute z-10 w-full h-full ${
          bgColorVariants[variant || "default"]
        } mix-blend-multiply`}
      ></div>

      <div className="absolute z-10 w-full h-full bg-texture-image mix-blend-multiply"></div>

      <motion.div
        className="w-full h-full relative"
        initial={{
          opacity: 0,
          y: 40,
        }}
        animate={{
          opacity: 1,
          y: 0,
          transition: {
            duration: 1,
            ease: [0.6, 0.01, -0.05, 0.9],
          },
        }}
        // Removed for performance reasons
        // style={{
        //   scale: shouldReduceMotion ? 1 : imageScroll,
        // }}
      >
        <ImageComp imageSrc={imageSrc} imageBlurUrl={imageBlurUrl} />
      </motion.div>
    </>
  );
}
