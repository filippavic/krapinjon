import { motion } from "framer-motion";
import Image from "next/image";

export default function ImageGrid({ id, images, onImageClick }) {
  return (
    <div key={id} className="grid grid-cols-repeat gap-4 mt-4">
      {images.map((image, index) => {
        return (
          <a key={index}>
            <motion.div
              className="relative max-w-sm h-32 w-full"
              initial={{ opacity: 0, y: 10 }}
              whileInView={{
                opacity: 1,
                y: 0,
                transition: {
                  duration: 1.5,
                  ease: [0.6, 0.01, -0.05, 0.9],
                  delay: 0.05 * index,
                },
              }}
              viewport={{
                margin: "-100px 0px -100px 0px",
                once: true,
              }}
            >
              <Image
                src={image.original_secure_url}
                width={image.width}
                height={image.height}
                sizes="384px"
                style={{
                  width: "100%",
                  height: "100%",
                  objectFit: "cover",
                }}
                alt=""
                className="cursor-pointer motion-safe:hover:scale-105 transition duration-500 ease-in-out"
                quality={68}
                onClick={() => onImageClick(index)}
              />
            </motion.div>
          </a>
        );
      })}
    </div>
  );
}
