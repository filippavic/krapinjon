import Image from "next/image";
import Lightbox from "yet-another-react-lightbox";
import "yet-another-react-lightbox/styles.css";

import ArrowSmUpIcon from "@heroicons/react/20/solid/ArrowSmallUpIcon";
import XIcon from "@heroicons/react/20/solid/XMarkIcon";

// Gallery taskbar icons
const GalleryIcon = ({ type }) => (
  <div className="group flex rounded-full h-10 w-10 items-center justify-center bg-medium-gray cursor-pointer transition duration-500 ease-in-out hover:scale-105 hover:bg-beige">
    {type == "prev" && (
      <ArrowSmUpIcon className="h-5 w-5 text-beige -rotate-90 group-hover:text-medium-gray duration-500" />
    )}
    {type == "next" && (
      <ArrowSmUpIcon className="h-5 w-5 text-beige rotate-90 group-hover:text-medium-gray duration-500" />
    )}
    {type == "close" && (
      <XIcon className="h-5 w-5 text-beige rotate-90 group-hover:text-medium-gray duration-500" />
    )}
  </div>
);

export default function LightboxGallery({
  images,
  galleryIndex,
  setGalleryIndex,
}) {
  return (
    <Lightbox
      styles={{ container: { backgroundColor: "rgba(0, 0, 0, .85)" } }}
      open={galleryIndex != null ? true : false}
      close={() => setGalleryIndex(null)}
      index={galleryIndex != null ? galleryIndex : 0}
      slides={images ?? []}
      render={{
        slide: function Slide(image, offset, rect) {
          const width = Math.round(
            Math.min(rect.width, (rect.height / image.height) * image.width)
          );
          const height = Math.round(
            Math.min(rect.height, (rect.width / image.width) * image.height)
          );

          return (
            <div style={{ position: "relative", width, height }}>
              <Image
                src={image.original_secure_url}
                fill
                style={{ objectFit: "cover" }}
                alt=""
                sizes={
                  typeof window !== "undefined"
                    ? `${Math.ceil((width / window.innerWidth) * 100)}vw`
                    : `${width}px`
                }
                priority
              />
            </div>
          );
        },
        iconClose: function IconClose() {
          return <GalleryIcon type="close" />;
        },
        iconPrev: function IconPrev() {
          return <GalleryIcon type="prev" />;
        },
        iconNext: function IconNext() {
          return <GalleryIcon type="next" />;
        },
      }}
    />
  );
}
