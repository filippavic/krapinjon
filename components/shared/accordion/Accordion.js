import AccordionItem from "./AccordionItem";

export default function Accordion({ id, items, variant }) {
  return (
    <div key={id}>
      {items.map((item, index) => {
        return (
          <AccordionItem
            key={item.id}
            index={index}
            title={item.key}
            text={item.value}
            variant={variant}
          />
        );
      })}
    </div>
  );
}
