import React, { useState } from "react";
import { motion } from "framer-motion";
import { accentColorVariants } from "../../../styles/colorVariants";

import QuestionMarkIcon from "@heroicons/react/20/solid/QuestionMarkCircleIcon";
import PlusIcon from "@heroicons/react/24/solid/PlusCircleIcon";
import MinusIcon from "@heroicons/react/24/outline/MinusCircleIcon";

export default function AccordionItem({ id, index, title, text, variant }) {
  const [openItem, setOpenItem] = useState(false);
  const handleOpenItem = () => {
    setOpenItem(!openItem);
  };

  return (
    <div
      key={id}
      className="border border-l-0 border-r-0 border-b-1 border-t-0 border-beige-dark"
    >
      <motion.h3
        className="mb-0"
        initial={{ opacity: 0, x: 10 }}
        whileInView={{
          opacity: 1,
          x: 0,
          transition: {
            duration: 1,
            ease: [0.6, 0.01, -0.05, 0.9],
            delay: index * 0.2,
          },
        }}
        viewport={{ margin: "-70px 0px -70px 0px", once: true }}
      >
        <button
          className="group relative flex w-full items-center text-beige font-semibold py-4 justify-between"
          type="button"
          onClick={() => handleOpenItem()}
        >
          <div className="inline-flex items-center text-left">
            <QuestionMarkIcon
              className={`h-5 w-5 ${
                accentColorVariants[variant || "default"]
              } mr-2 md:mr-1`}
            />
            {title}
          </div>

          {openItem == true ? (
            <MinusIcon className="h-5 w-5 text-beige-dark" />
          ) : (
            <PlusIcon className="h-5 w-5 text-beige-dark" />
          )}
        </button>
      </motion.h3>
      <div
        className={`${
          openItem == false && "hidden"
        } transition-all duration-500 ease-in-out pt-2 pb-6`}
      >
        <span className="text-beige">{text}</span>
      </div>
    </div>
  );
}
