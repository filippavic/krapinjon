import { motion } from "framer-motion";

export default function Divider({ id, variant }) {
  const borderVariants = {
    krakon: "border-krakon-purple",
    default: "border-beige",
  };

  return (
    <motion.div
      key={id}
      className={`border-t ${borderVariants[variant || "default"]} my-9`}
      initial={{ opacity: 0, scaleX: 0.7 }}
      whileInView={{
        opacity: 0.4,
        scaleX: 1,
        transition: {
          duration: 1,
          ease: [0.6, 0.01, -0.05, 0.9],
        },
      }}
      viewport={{ margin: "-70px 0px -70px 0px", once: true }}
    ></motion.div>
  );
}
