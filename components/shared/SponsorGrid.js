import { motion } from "framer-motion";
import Image from "next/image";

const ConditionalWrapper = ({ condition, wrapper, children }) =>
  condition ? wrapper(children) : children;

export default function SponsorGrid({ id, sponsors }) {
  return (
    <div key={id} className="grid grid-cols-repeat gap-4 mt-5">
      {sponsors.map((image, index) => {
        var sponsorUrl = image.context?.custom?.URL ?? null;

        return (
          <motion.div
            className="relative max-w-sm h-20 w-full rounded-lg"
            key={index}
            initial={{ opacity: 0, y: 10 }}
            whileInView={{
              opacity: 1,
              y: 0,
              transition: {
                duration: 1.5,
                ease: [0.6, 0.01, -0.05, 0.9],
                delay: 0.1 * index,
              },
            }}
            viewport={{
              margin: "-100px 0px -100px 0px",
              once: true,
            }}
          >
            <ConditionalWrapper
              condition={sponsorUrl != null}
              wrapper={(children) => (
                <a
                  target="_blank"
                  href={sponsorUrl ?? "/"}
                  rel="noopener noreferrer"
                >
                  {children}
                </a>
              )}
            >
              <Image
                src={image.original_secure_url}
                width={image.width}
                height={image.height}
                sizes="384px"
                style={{
                  objectFit: "contain",
                  width: "100%",
                  height: "100%",
                }}
                alt=""
                className="rounded-lg"
              />
            </ConditionalWrapper>
          </motion.div>
        );
      })}
    </div>
  );
}
