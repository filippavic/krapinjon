import { motion } from "framer-motion";
import {
  accentColorVariants,
  accentColorHoverVariants,
} from "../../../styles/colorVariants";

export default function LinkButton({ id, title, link, icon, variant }) {
  return (
    <motion.div
      key={id}
      className="z-50 inline-flex items-center"
      initial={{ opacity: 0, x: 10 }}
      whileInView={{
        opacity: 1,
        x: 0,
        transition: {
          duration: 1,
          ease: [0.6, 0.01, -0.05, 0.9],
        },
      }}
      viewport={{ margin: "-70px 0px -70px 0px", once: true }}
    >
      {icon}
      {link ? (
        <a
          className={`${
            accentColorHoverVariants[variant || "default"]
          } text-base font-semibold transition duration-300 ease-in-out`}
          href={link}
        >
          {title}
        </a>
      ) : (
        <span
          className={`${
            accentColorVariants[variant || "default"]
          } text-base font-semibold`}
        >
          {title}
        </span>
      )}
    </motion.div>
  );
}
