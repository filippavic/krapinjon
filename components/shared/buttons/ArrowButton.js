import { motion } from "framer-motion";
import Link from "next/link";
import ArrowSmRightIcon from "@heroicons/react/20/solid/ArrowSmallRightIcon";
import {
  buttonIconVariants,
  buttonBgVariants,
} from "../../../styles/colorVariants";

export default function ArrowButton({ id, text, link, position, variant }) {
  const paddingVariants = {
    right: "pr-0 group-hover:pl-3",
    left: "pl-0 group-hover:pl-2",
  };

  return (
    <Link key={id} href={link} prefetch={false} passHref legacyBehavior>
      <motion.a
        className="group flex flex-row items-center cursor-pointer mt-10 max-w-fit"
        initial={{ opacity: 0, x: -10 }}
        whileInView={{
          opacity: 1,
          x: 0,
          transition: {
            duration: 1,
            ease: [0.6, 0.01, -0.05, 0.9],
          },
        }}
        viewport={{ margin: "-70px 0px -70px 0px", once: true }}
      >
        <span
          className={`text-sm font-semibold transition-all duration-500 ease-in-out text-beige ${
            paddingVariants[position || "left"]
          }`}
        >
          {text}
        </span>
        <div
          className={`ml-1.5 flex rounded-full h-5 w-5 items-center justify-center transition-all group-hover:ml-3 duration-500 ease-in-out group-hover:scale-110 ${
            buttonBgVariants[variant || "default"]
          }`}
        >
          <ArrowSmRightIcon
            className={`${
              buttonIconVariants[variant || "default"]
            } h-3 w-3 transition duration-500 ease-in-out`}
          />
        </div>
      </motion.a>
    </Link>
  );
}
