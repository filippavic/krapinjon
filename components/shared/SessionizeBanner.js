import { motion } from "framer-motion";

export default function SessionizeBanner() {
  return (
    <motion.a
      href="https://sessionize.com/"
      style={{
        color: "#fff",
        textDecoration: "none",
        zIndex: 50,
        paddingTop: "20px",
      }}
      initial={{ opacity: 0, y: 15 }}
      animate={{
        opacity: 1,
        y: 0,
        transition: {
          duration: 0.7,
          ease: [0.6, 0.01, -0.05, 0.9],
          delay: 0.1,
        },
      }}
    >
      <strong>Call for Sessions</strong> powered by{" "}
      <strong style={{ color: "#1AB394" }}>Sessionize.com</strong>
    </motion.a>
  );
}
