import { motion } from "framer-motion";
import { accentColorHoverVariants } from "../../styles/colorVariants";

export default function DocumentList({ id, title, documents, variant }) {
  return (
    <div key={id} className="mt-1">
      {documents.map((document, index) => {
        return (
          <motion.a
            className={`font-semibold ${
              accentColorHoverVariants[variant || "default"]
            } transition duration-300 ease-in-out block w-fit`}
            href={"https:" + document.fields.file.url}
            key={`${title}_${index}`}
            target="_blank"
            rel="noreferrer"
            initial={{ opacity: 0, x: 10 }}
            whileInView={{
              opacity: 1,
              x: 0,
              transition: {
                duration: 0.5,
                ease: [0.6, 0.01, -0.05, 0.9],
              },
            }}
            viewport={{ margin: "-70px 0px -70px 0px", once: true }}
          >
            {`${document.fields.title} (.${document.fields.file.fileName
              .split(".")
              .pop()})`}
          </motion.a>
        );
      })}
    </div>
  );
}
