import { motion } from "framer-motion";
import { accentColorHoverVariants } from "../../../styles/colorVariants";

/// Tagline element, which displays a given tagline.
export default function TaglineElement({ text, variant }) {
  return (
    <motion.div className={`flex items-center text-center justify-center`}>
      <motion.h1
        className={`font-display text-4xl ${
          accentColorHoverVariants[variant || "default"]
        } text-center`}
        initial={{ opacity: 0, y: 10 }}
        whileInView={{
          opacity: 1,
          y: 0,
          transition: {
            duration: 1,
            ease: [0.6, 0.01, -0.05, 0.9],
          },
        }}
        viewport={{ margin: "-70px 0px -70px 0px", once: true }}
      >
        {text}
      </motion.h1>
    </motion.div>
  );
}
