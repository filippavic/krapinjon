import { motion } from "framer-motion";
import constants from "../../../utils/constants";

// Iframe element for video players (YouTube, Vimeo)
export default function IframeElement({ url, variant }) {
  return (
    <motion.span className="w-full" key={url}>
      <iframe
        className="w-full h-full aspect-video"
        title={variant}
        src={url}
        allow={
          variant == constants.videoVariant.YouTube
            ? "accelerometer; encrypted-media; gyroscope; picture-in-picture"
            : undefined
        }
        allowFullScreen
      />
    </motion.span>
  );
}
