import { motion } from "framer-motion";
import { mainColorVariants } from "../../../styles/colorVariants";

export default function InfoParagraph({ id, title, icon, content, variant }) {
  return (
    <div className="mt-14 w-full" key={id}>
      <motion.h3
        className={`text-base ${
          mainColorVariants[variant || "default"]
        } font-semibold inline-flex items-center`}
        initial={{ opacity: 0, x: 10 }}
        whileInView={{
          opacity: 1,
          x: 0,
          transition: {
            duration: 1,
            ease: [0.6, 0.01, -0.05, 0.9],
          },
        }}
        viewport={{ margin: "-70px 0px -70px 0px", once: true }}
      >
        {icon}
        {title}
      </motion.h3>

      <div className="flex flex-col w-full">
        <motion.div
          initial={{ opacity: 0, x: 15 }}
          animate={{
            opacity: 1,
            x: 0,
            transition: {
              duration: 1,
              ease: [0.6, 0.01, -0.05, 0.9],
            },
          }}
        >
          {content}
        </motion.div>
      </div>
    </div>
  );
}
