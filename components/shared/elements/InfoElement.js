import { motion } from "framer-motion";
import Image from "next/image";
import { documentToReactComponents } from "@contentful/rich-text-react-renderer";
import { accentColorHoverVariants } from "../../../styles/colorVariants";
import { huskajTextRenderOptions } from "../../../styles/textStyles";

/// Info component, which displays a title and info text
/// or an image and info text.
export default function InfoElement({
  title,
  text,
  image,
  alignment,
  variant,
}) {
  const rowAlign = {
    left: "md:flex-row",
    right: "md:flex-row-reverse",
  };

  const textAlign = {
    right: "text-right",
    left: "text-left",
  };

  return (
    <motion.div
      className={`flex flex-col ${
        rowAlign[alignment || "left"]
      } justify-between items-center`}
    >
      <motion.div className="flex w-full md:w-1/2">
        {title != null && image == null && (
          <motion.h1
            className={`font-display text-6xl ${
              accentColorHoverVariants[variant || "default"]
            } w-fit md:w-min ${textAlign[alignment || "left"]}`}
            initial={{ opacity: 0, y: 10 }}
            whileInView={{
              opacity: 1,
              y: 0,
              transition: {
                duration: 1,
                ease: [0.6, 0.01, -0.05, 0.9],
              },
            }}
            viewport={{ margin: "-70px 0px -70px 0px", once: true }}
          >
            {title}
          </motion.h1>
        )}

        {image != null && (
          <motion.div
            className={`w-full  h-32 lg:h-52 ${
              alignment == "left" ? "md:mr-16" : "md:ml-16"
            }`}
            initial={{
              opacity: 0,
            }}
            animate={{
              opacity: 1,
              transition: {
                ease: [0.6, 0.01, -0.05, 0.95],
                duration: 0.6,
              },
            }}
            viewport={{ margin: "-70px 0px -70px 0px", once: false }}
          >
            <Image
              src={image[0].original_secure_url}
              width={image[0].width}
              height={image[0].height}
              sizes="(max-width: 768px) 85vw, 50vw"
              style={{
                objectFit: "cover",
                width: "100%",
                height: "100%",
              }}
              alt=""
              className="rounded-xl transition duration-500 ease-in-out grayscale"
            />
          </motion.div>
        )}
      </motion.div>

      <motion.div className="flex w-full md:w-1/2 mt-4 md:mt-0 flex-col items-start md:items-end">
        <motion.div
          className={`font-normal text-base text-beige text-left ${
            alignment == "left" ? "md:ml-16" : "md:mr-16"
          } md:${textAlign[alignment == "left" ? "right" : "left"]}`}
          initial={{
            opacity: 0,
            x: alignment == "left" ? 20 : -20,
            scale: 1.02,
          }}
          whileInView={{
            opacity: 1,
            x: 0,
            scale: 1,
            transition: {
              duration: 1,
              ease: [0.6, 0.01, -0.05, 0.9],
              delay: 0.2,
            },
          }}
          viewport={{ margin: "-70px 0px -70px 0px", once: true }}
        >
          {documentToReactComponents(text, huskajTextRenderOptions)}
        </motion.div>
      </motion.div>
    </motion.div>
  );
}
