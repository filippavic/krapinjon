import React, { useState, useEffect } from "react";
import Image from "next/image";
import { motion } from "framer-motion";

import backgroundImage from "../../public/images/krakon/krakon_landing_background.jpg";
import {
  dateTimeToString,
  daysRemaining,
  hasEventEnded,
} from "../../utils/helperFunctions";

const TextContent = ({
  locationName,
  startDateTime,
  endDateTime,
  allDayEvent,
}) => {
  // Time of event as a string
  const [eventDateTime, setEventDateTime] = useState("");
  const [daysToEvent, setDaysToEvent] = useState("");
  const [eventEnded, setEventEnded] = useState(false);

  useEffect(() => {
    // Set date and time client-side
    let eventDateTime = dateTimeToString(
      startDateTime,
      endDateTime,
      allDayEvent
    );

    // Set remaining days client-side
    let daysToEvent = daysRemaining(startDateTime);

    setEventDateTime(eventDateTime);
    setDaysToEvent(daysToEvent);
    setEventEnded(hasEventEnded(endDateTime));

    // eslint-disable-next-line react-hooks/exhaustive-deps
  }, []);

  if (eventEnded) {
    return (
      <div className="w-full flex flex-col items-center px-10">
        <motion.h1
          className="flex font-display text-6xl text-center text-krakon-purple text-border-black text-border-2"
          initial={{ opacity: 0, x: -10 }}
          animate={{
            opacity: 1,
            x: 0,
            transition: {
              duration: 1,
              ease: [0.6, 0.01, -0.05, 0.9],
            },
          }}
        >
          Hvala na dolasku!
        </motion.h1>
        <motion.h2
          className="flex font-display text-2xl text-beige text-border-black text-border-2 mt-4"
          initial={{ opacity: 0, x: 10 }}
          animate={{
            opacity: 1,
            x: 0,
            transition: {
              duration: 1,
              ease: [0.6, 0.01, -0.05, 0.9],
              delay: 0.2,
            },
          }}
        >
          Vidimo se sljedeće godine
        </motion.h2>
      </div>
    );
  }

  return (
    <div className="w-full flex flex-col md:flex-row justify-between align-middle px-10">
      <div className="flex flex-col justify-center items-center md:items-start">
        <motion.h1
          className="flex font-display text-2xl xs:text-3xl sm:text-4xl md:!text-5xl text-center md:text-left text-beige text-border-black text-border-2"
          initial={{ opacity: 0, x: -10 }}
          animate={{
            opacity: 1,
            x: 0,
            transition: {
              duration: 1,
              ease: [0.6, 0.01, -0.05, 0.9],
            },
          }}
        >
          {eventDateTime}
        </motion.h1>
        <motion.h1
          className="flex font-display text-2xl xs:text-3xl sm:text-4xl md:!text-5xl text-center md:text-left text-beige text-border-black text-border-2 mt-4"
          initial={{ opacity: 0, x: -10 }}
          animate={{
            opacity: 1,
            x: 0,
            transition: {
              duration: 1,
              ease: [0.6, 0.01, -0.05, 0.9],
              delay: 0.1,
            },
          }}
        >
          {locationName}
        </motion.h1>
      </div>
      <div className="flex flex-col justify-center items-center md:items-end mt-16 md:mt-0">
        {daysToEvent > 0 && (
          <motion.h2
            className="flex font-display text-6xl xs:text-huge sm:text-large md:!text-massive text-krakon-purple text-border-black text-border-2"
            initial={{ opacity: 0, x: 10 }}
            animate={{
              opacity: 1,
              x: 0,
              transition: {
                duration: 1,
                ease: [0.6, 0.01, -0.05, 0.9],
                delay: 0.2,
              },
            }}
          >
            {daysToEvent}
          </motion.h2>
        )}
        <motion.h2
          className="flex font-display text-sm xs:text-lg md:!text-xl text-center md:text-left text-beige text-border-black text-border-2"
          initial={{ opacity: 0, x: 10 }}
          animate={{
            opacity: 1,
            x: 0,
            transition: {
              duration: 1,
              ease: [0.6, 0.01, -0.05, 0.9],
              delay: 0.3,
            },
          }}
        >
          {daysToEvent > 0 ? "dana do KraKona" : "Upravo traje - pridruži se!"}
        </motion.h2>
      </div>
    </div>
  );
};

const KrakonCountdown = ({ eventData }) => {
  const { locationName, startDateTime, endDateTime, allDayEvent } = eventData;

  // Resize image and text based on scroll postion
  // Removed for performance reasons
  // const { scrollY } = useScroll();
  // const imageScroll = useTransform(scrollY, [0, 500], [1, 1.15]);
  // const textScroll = useTransform(scrollY, [0, 500], [1, 1.08]);

  // Accessibility
  // const shouldReduceMotion = useReducedMotion();

  return (
    <motion.div className="relative w-full h-full flex justify-center items-center overflow-hidden">
      <div className="absolute z-20 w-full h-full bg-gradient-to-b from-black/20 to-background-black flex flex-col justify-center items-center">
        <TextContent
          locationName={locationName}
          startDateTime={startDateTime}
          endDateTime={endDateTime}
          allDayEvent={allDayEvent}
        />
      </div>

      <div
        className="absolute z-10 w-full h-full mix-blend-multiply"
        style={{
          backgroundColor: "#4436A3",
        }}
      ></div>

      <div className="absolute z-10 w-full h-full bg-texture-image mix-blend-multiply"></div>

      <motion.div
        className="w-full h-full relative"
        initial={{
          opacity: 0,
        }}
        animate={{
          opacity: 1,
          transition: {
            duration: 1.5,
            ease: [0.6, 0.01, -0.05, 0.9],
            delay: 0.7,
          },
        }}
        exit={{
          opacity: 0,
          transition: {
            duration: 1,
            ease: [0.6, 0.01, -0.05, 0.9],
          },
        }}
        // Removed for performance reasons
        // style={{ scale: shouldReduceMotion ? 1 : imageScroll }}
      >
        <Image
          src={backgroundImage}
          sizes="100vw"
          fill
          alt=""
          className="grayscale"
          priority
          quality={68}
          style={{ objectFit: "cover" }}
        />
      </motion.div>
    </motion.div>
  );
};

export default KrakonCountdown;
