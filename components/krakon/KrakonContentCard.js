import Link from "next/link";
import Image from "next/image";
import { motion } from "framer-motion";

export default function KrakonContentCard(props) {
  const { title, thumbnail } = props.content;

  return (
    // <Link href={""} passHref>
    <motion.div
      className="flex flex-col bg-none w-full rounded-xl transition duration-500 ease-in-out group"
      initial={{ opacity: 0, y: 20 }}
      whileInView={{ opacity: 1, y: 0 }}
      viewport={{ margin: "-70px 0px -70px 0px", once: true }}
    >
      <div className="w-full flex flex-row justify-between">
        <motion.div className="relative w-full h-full rounded-xl overflow-hidden">
          <div className="absolute z-20 w-full h-full bg-black/40 flex flex-col p-2 justify-between">
            <div className="flex flex-col h-full items-center  justify-center">
              <h3 className="flex font-display text-2xl md:text-4xl text-center text-beige text-border-black text-border-2 duration-500 ease-in-out">
                {title}
              </h3>
            </div>
          </div>

          <motion.div
            className={"w-full h-32 lg:h-52"}
            initial={{
              opacity: 0,
              scale: 1.3,
            }}
            animate={{
              opacity: 1,
              scale: 1,
              transition: {
                ease: [0.6, 0.01, -0.05, 0.95],
                duration: 0.6,
              },
            }}
          >
            {/* <div
              className="absolute z-10 w-full h-full mix-blend-multiply opacity-0 group-hover:opacity-100 transition duration-500 ease-in-out"
              style={{
                backgroundColor: "#861FC5",
              }}
            ></div> */}

            <Image
              src={thumbnail}
              sizes="384px"
              style={{
                objectFit: "cover",
                width: "100%",
                height: "100%",
              }}
              alt=""
              className="rounded-xl transition duration-500 ease-in-out"
            />
          </motion.div>
        </motion.div>
      </div>
    </motion.div>
    // </Link>
  );
}
