import React from "react";
import Link from "next/link";
import { motion } from "framer-motion";
import Marquee from "react-fast-marquee";

import ArrowSmRightIcon from "@heroicons/react/20/solid/ArrowSmallRightIcon";
import XIcon from "@heroicons/react/20/solid/XCircleIcon";

const HighlightTicker = ({ text, slug, onDismiss, widthPercent, baseUrl }) => {
  return (
    <motion.div
      className="flex items-center justify-between max-w-sm h-8 bg-dark-gray/70 rounded-full px-0 xs:px-2 shadow-lg shadow-medium-gray/10 cursor-pointer"
      initial={{ opacity: 0, width: "0%" }}
      animate={{
        opacity: 1,
        width: ["0%", `${widthPercent}%`, "100%"],
        transition: {
          duration: 3,
          times: [0, 0.2, 1],
          ease: [0.6, 0.01, -0.05, 0.9],
        },
      }}
      exit={{
        opacity: 0,
        scaleX: 0,
        transition: {
          duration: 0.7,
          delay: 0.5,
          ease: [0.6, 0.01, -0.05, 0.9],
        },
      }}
      whileHover={{
        scale: 1.05,
      }}
      drag="x"
      dragElastic
      dragTransition={{ bounceStiffness: 600, bounceDamping: 10 }}
      onDragEnd={onDismiss}
    >
      <motion.div
        className="flex rounded-full h-5 w-5 cursor-pointer items-center justify-center bg-medium-gray/70 hover:bg-medium-gray transition-all duration-500 ease-in-out"
        initial={{ opacity: 0, scale: 0 }}
        animate={{
          opacity: 1,
          scale: 1,
          transition: {
            duration: 0.7,
            ease: [0.6, 0.01, -0.05, 0.9],
            delay: 0.9,
          },
        }}
        exit={{
          opacity: 0,
          transition: {
            duration: 0.3,
            ease: [0.6, 0.01, -0.05, 0.9],
          },
        }}
        onClick={() => onDismiss()}
      >
        <XIcon className="h-3 w-3 text-beige" />
      </motion.div>

      <motion.div
        className="flex-grow w-5"
        initial={{ opacity: 0 }}
        animate={{
          opacity: 1,

          transition: {
            duration: 0.7,
            ease: [0.6, 0.01, -0.05, 0.9],
            delay: 0.9,
          },
        }}
        exit={{
          opacity: 0,
          transition: {
            duration: 0.4,
            ease: [0.6, 0.01, -0.05, 0.9],
          },
        }}
      >
        <Link
          href={`/${baseUrl != null ? baseUrl + "/" : ""}novosti/` + slug}
          passHref
          onClick={onDismiss}
        >
          <Marquee gradient={false}>
            <span className="text-xs font-semibold text-beige select-none cursor-pointer pr-3">
              {text}
            </span>
          </Marquee>
        </Link>
      </motion.div>

      <Link
        href={`/${baseUrl != null ? baseUrl + "/" : ""}novosti/` + slug}
        passHref
        onClick={onDismiss}
      >
        <motion.div
          className="flex rounded-full h-5 w-5 items-center justify-center bg-beige group-hover:bg-medium-gray/70 transition-all duration-500 ease-in-out cursor-pointer"
          initial={{ opacity: 0, scale: 0 }}
          animate={{
            opacity: 1,
            scale: 1,
            transition: {
              duration: 0.7,
              ease: [0.6, 0.01, -0.05, 0.9],
              delay: 0.9,
            },
          }}
          exit={{
            opacity: 0,
            transition: {
              duration: 0.3,
              ease: [0.6, 0.01, -0.05, 0.9],
            },
          }}
        >
          <ArrowSmRightIcon className="h-3 w-3 text-medium-gray" />
        </motion.div>
      </Link>
    </motion.div>
  );
};

export default HighlightTicker;
