import React from "react";
import Link from "next/link";
import { motion, AnimatePresence } from "framer-motion";

import animations from "../../../utils/layoutAnimations";
import { hoverVariants, variants } from "../../../styles/colorVariants";

export default function MobileMenu({
  isVisible,
  openCloseMenu,
  menuItems,
  variant,
}) {
  let hoverColor = hoverVariants[variant];

  return (
    <AnimatePresence>
      {isVisible && (
        <motion.div
          className="flex w-screen h-screen fixed bg-black z-75 justify-center items-center"
          variants={animations.headerAnimation}
          initial="initial"
          animate="animate"
          exit={{ opacity: 0 }}
        >
          <div className="flex flex-col w-4/5 h-3/5 text-center justify-evenly">
            {menuItems &&
              menuItems.map((menuItem) => (
                <Link
                  href={menuItem.link}
                  prefetch={false}
                  passHref
                  legacyBehavior
                  key={menuItem.link}
                >
                  <motion.a
                    className={`font-display text-lg text-beige ${hoverColor} transition ease-easeAlt2 duration-300`}
                    variants={animations.headerChildAnimation}
                    onClick={openCloseMenu}
                    target={menuItem.isExternal ? "_blank" : undefined}
                    rel={
                      menuItem.isExternal ? "noopener noreferrer" : undefined
                    }
                  >
                    {menuItem.title}
                  </motion.a>
                </Link>
              ))}
          </div>
        </motion.div>
      )}
    </AnimatePresence>
  );
}

MobileMenu.defaultProps = {
  variant: variants.krapinjon,
};
