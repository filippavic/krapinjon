import React, { useState, useEffect, useRef, useMemo } from "react";
import Link from "next/link";
import Image from "next/image";
import { AnimatePresence, motion, MotionConfig } from "framer-motion";

import animations from "../../utils/layoutAnimations";
import {
  getCurrentYear,
  readFromLocalStorage,
  saveToLocalStorage,
} from "../../utils/helperFunctions";
import ContentfulHelper from "../../utils/contentfulHelper";

import MobileMenu from "./menus/MobileMenu";
import HighlightTicker from "../alerts/HighlightTicker";

import logo from "../../public/images/krapinjon_logo.png";
import ArrowSmUpIcon from "@heroicons/react/20/solid/ArrowSmallUpIcon";
import KrakonLogo from "../krakon/KrakonLogo";
import { variants } from "../../styles/colorVariants";

const menuItems = [
  {
    link: "https://krakon-2024.sessionize.com/",
    title: "Program",
    isExternal: true,
  },
  {
    link: "/krakon/novosti",
    title: "Novosti",
    isExternal: false,
  },
  {
    link: "/krakon/informacije",
    title: "Informacije",
    isExternal: false,
  },
  {
    link: "/",
    title: "Krapinjon",
    isExternal: false,
  },
];

export default function KrakonLayout({ children }) {
  // Header menu
  const [state, setState] = useState({
    initial: false,
    open: false,
    menuName: "IZBORNIK",
  });

  const openCloseMenu = () => {
    if (state.open === true) {
      closeMenu();
    } else if (state.open === false) {
      setState({
        open: true,
        menuName: "ZATVORI",
      });
    }
  };

  const closeMenu = () => {
    setState({
      open: false,
      menuName: "IZBORNIK",
    });
  };

  const scrollToTop = () => {
    window.scrollTo({ top: 0, behavior: "smooth" });
  };

  let now = getCurrentYear();

  // Highlighted news article
  const [highlight, setHighlight] = useState(null);
  const fetchTriggered = useRef(false);

  // Dismiss the highlighted news ticker
  const onDismiss = () => {
    setHighlight(null);
  };

  // Width percentage of screen
  // Used to determine the necessary width for the highlight ticker
  // The width should be about the same as its height (32px)
  const [percent, setPercent] = useState(0);

  function calculateWidthPercent() {
    var width = window.innerWidth;
    var percent = Math.round((32 / width) * 100);
    setPercent(percent);
  }

  useEffect(() => {
    if (window != undefined) {
      calculateWidthPercent();

      setTimeout(async function () {
        if (fetchTriggered.current == false) {
          // Fetch news only once while visiting the page
          fetchTriggered.current = true;

          const newHighlight = await ContentfulHelper.getHighlightedNews(
            "KraKon"
          );

          if (newHighlight != null) {
            const newHighlightSlug = newHighlight.fields.slug;
            const lastHighlightSlug = readFromLocalStorage("lastHighlight");

            // Don't show the highlight twice
            if (lastHighlightSlug != newHighlightSlug) {
              saveToLocalStorage("lastHighlight", newHighlightSlug);

              setHighlight({
                text: newHighlight.fields.title,
                slug: newHighlight.fields.slug,
              });
            }
          }
        }
      }, 1500);
    }
  }, []);

  const logoSection = useMemo(() => {
    return (
      <motion.div
        className="md:flex-1"
        variants={animations.headerChildAnimation}
      >
        <motion.div className="group w-16 cursor-pointer">
          <KrakonLogo closeMenu={closeMenu} />
        </motion.div>
      </motion.div>
    );
  }, []);

  const tickerSection = useMemo(() => {
    return (
      <div className="flex flex-1 justify-center px-3">
        <AnimatePresence>
          {highlight != null && (
            <HighlightTicker
              text={highlight.text}
              slug={highlight.slug}
              onDismiss={onDismiss}
              widthPercent={percent}
              baseUrl={"krakon"}
            />
          )}
        </AnimatePresence>
      </div>
    );
  }, [highlight, percent]);

  const menuSection = useMemo(() => {
    return (
      <div className="flex w-16 md2:w-auto md:flex-1 justify-end">
        <motion.div
          className="w-full flex justify-end md2:justify-between max-w-xs visible pl-0 "
          variants={animations.headerAnimation}
        >
          {menuItems &&
            menuItems.map((menuItem) => (
              <Link
                href={menuItem.link}
                prefetch={false}
                passHref={menuItem.isExternal}
                legacyBehavior
                key={"header-" + menuItem.link}
              >
                <motion.a
                  className="hidden md2:block cursor-pointer font-display text-base text-beige hover:text-krakon-purple transition ease-easeAlt2 duration-300"
                  variants={animations.headerChildAnimation}
                  target={menuItem.isExternal ? "_blank" : undefined}
                  rel={menuItem.isExternal ? "noopener noreferrer" : undefined}
                >
                  {menuItem.title}
                </motion.a>
              </Link>
            ))}

          <motion.span
            onClick={openCloseMenu}
            className="block md2:hidden cursor-pointer font-display text-base text-beige hover:text-krakon-purple transition ease-easeAlt2 duration-300"
            variants={animations.headerChildAnimation}
          >
            {state.menuName}
          </motion.span>
        </motion.div>
      </div>
    );
  }, [state]);

  const footerSection = useMemo(() => {
    return (
      <footer>
        <div
          variants={animations.footerAnimation}
          className="mx-auto w-10/12 max-w-screen-xl mt-20 md:mt-36"
        >
          <div className="mt-8 sm:mt-0 sm:w-full sm:px-8 flex flex-col md:flex-row md:justify-between justify-start pt-10 pb-10">
            <div className="flex-none w-24 self-center md:self-start">
              <Link href="/" prefetch={false}>
                <Image
                  src={logo}
                  style={{
                    width: "100%",
                    height: "100%",
                    objectFit: "cover",
                  }}
                  alt="Krapinjon"
                  sizes="384px"
                />
              </Link>
            </div>

            <div className="flex-none w-full md:w-2/6 lg:w-3/6 flex flex-col justify-between mt-5 md:mt-0 items-center">
              <h2 className="w-2/3 text-center text-base mt-3 md:mt-0 md:text-left font-extralight text-beige">
                Organizator: Udruga Krapinjon
              </h2>

              <div className="w-2/3 mt-3 md:mt-0 flex flex-col items-center md:items-start md:justify-items-start">
                <a
                  href="mailto:krakon@krapinjon.hr"
                  className="w-auto text-beige font-extralight text-sm hover:text-krakon-purple transition duration-200 ease-in-out"
                >
                  krakon@krapinjon.hr
                </a>
              </div>
            </div>

            <div className="flex-none w-full md:w-3/6 lg:w-2/6 mt-10 md:mt-0 flex flex-col justify-between self-center">
              <div className="w-full flex flex-row align-middle md:ali justify-evenly md:justify-between mb-10">
                <div className="flex flex-col">
                  <Link
                    href="/krakon"
                    prefetch={false}
                    className="text-beige font-extralight text-xs hover:text-krakon-purple mb-3 transition duration-200 ease-in-out"
                  >
                    Početna
                  </Link>
                  {menuItems.slice(0, menuItems.length - 1).map((menuItem) => (
                    <Link
                      key={"footer-" + menuItem.link}
                      href={menuItem.link}
                      prefetch={false}
                      className="text-beige font-extralight text-xs hover:text-krakon-purple mb-3 transition duration-200 ease-in-out"
                    >
                      {menuItem.title}
                    </Link>
                  ))}
                </div>

                <div className="flex flex-col">
                  <a
                    href="https://www.facebook.com/galaktickigujdek"
                    target="_blank"
                    rel="noopener noreferrer"
                    className="text-beige font-extralight text-xs text-right md:text-left hover:text-krakon-purple mb-3 transition duration-200 ease-in-out"
                  >
                    Facebook
                  </a>
                  <a
                    href="https://www.instagram.com/galakticki_gujdek"
                    target="_blank"
                    rel="noopener noreferrer"
                    className="text-beige font-extralight text-xs text-right md:text-left hover:text-krakon-purple mb-3 transition duration-200 ease-in-out"
                  >
                    Instagram
                  </a>
                  <a
                    href="https://www.youtube.com/@krapinjon"
                    target="_blank"
                    rel="noopener noreferrer"
                    className="text-beige font-extralight text-xs text-right md:text-left hover:text-krakon-purple mb-3 transition duration-200 ease-in-out"
                  >
                    YouTube
                  </a>
                </div>

                <div
                  className="hidden md:flex rounded-full h-10 w-10 items-center justify-center bg-krakon-purple cursor-pointer transition duration-200 ease-in-out hover:scale-105 hover:bg-krakon-purple-dark"
                  onClick={() => scrollToTop()}
                >
                  <ArrowSmUpIcon className="h-5 w-5 text-beige" />
                </div>
              </div>

              <span className="text-beige font-extralight text-xs self-center md:self-start">
                &copy; {now}. Udruga Krapinjon
              </span>
            </div>
          </div>
        </div>
      </footer>
    );
  }, []);

  return (
    <MotionConfig reducedMotion="user">
      <div style={{ WebkitTapHighlightColor: "rgba(68, 54, 163, 0.2)" }}>
        <header className="w-full h-full fixed z-100 pointer-events-none">
          <div className="w-full h-full fixed bg-texture-light bg-[length:350px] pointer-events-none opacity-50"></div>
          <motion.div className="h-36 md:h-28 w-full top-0 flex justify-center bg-gradient-to-b from-black to-transparent">
            <motion.div
              className="w-full flex items-center justify-between h-20 px-4 sm:px-8 pt-2 pointer-events-auto"
              variants={animations.headerMainAnimation}
              initial="initial"
              animate="animate"
            >
              {logoSection}
              {tickerSection}
              {menuSection}
            </motion.div>
          </motion.div>
        </header>

        <MobileMenu
          isVisible={state.open}
          openCloseMenu={openCloseMenu}
          menuItems={menuItems}
          variant={variants.krakon}
        />

        {children}

        {footerSection}
      </div>
    </MotionConfig>
  );
}
