import React, { useState, useRef } from "react";
import { motion, AnimatePresence } from "framer-motion";
import { Swiper, SwiperSlide } from "swiper/react";
import { Keyboard, Autoplay } from "swiper";

import CarouselMain from "./CarouselMain";
import CarouselEvent from "./CarouselEvent";

import ArrowSmRightIcon from "@heroicons/react/20/solid/ArrowSmallRightIcon";

import "swiper/css";
import "swiper/css/keyboard";

const CarouselComp = ({ events }) => {
  // Swiper
  const swiperRef = useRef();

  // Current slide index
  const [currentSlide, setCurrentSlide] = useState(0);

  // Tracks the completion of first (loading) animation
  const [firstAnimation, setFirstAnimation] = useState(true);

  /** Change slide
   * @param newSlide - new slide index
   */
  function setSlide(newSlide) {
    if (firstAnimation) setFirstAnimation(false);

    setCurrentSlide(newSlide);
  }

  /** Trigger next slide */
  function goToNextSlide() {
    if (firstAnimation) setFirstAnimation(false);
    swiperRef.current.slideNext();
  }

  /** Trigger previous slide */
  function goToPrevSlide() {
    if (firstAnimation) setFirstAnimation(false);
    swiperRef.current.slidePrev();
  }

  /** Stop Swiper autoplay after chaning to first slide */
  function stopOnAutoplay(swiper) {
    swiper.autoplay.stop();
  }

  return (
    <div className="w-full h-full overflow-hidden flex justify-end">
      <div className="absolute z-30 flex flex-col self-end pr-4 mb-4 md:pr-8 md:mb-8 w-full">
        <AnimatePresence exitBeforeEnter>
          {events && events.length != 0 && currentSlide != events.length && (
            <div
              className="flex flex-col items-end"
              key={events[currentSlide].fields.slug}
            >
              <motion.span
                className="text-xs font-bold uppercase text-beige-dark"
                initial={{ opacity: 0, x: 10 }}
                animate={{
                  opacity: 1,
                  x: 0,
                  transition: {
                    duration: 0.7,
                    ease: [0.6, 0.01, -0.05, 0.9],
                    delay: firstAnimation ? 0.9 : 0.2,
                  },
                }}
                exit={{
                  opacity: 0,
                  transition: {
                    duration: 0.7,
                    ease: [0.6, 0.01, -0.05, 0.9],
                  },
                }}
              >
                Uskoro
              </motion.span>
              <motion.div
                className="group flex flex-row items-center cursor-pointer"
                initial={{ opacity: 0, x: 10 }}
                animate={{
                  opacity: 1,
                  x: 0,
                  transition: {
                    duration: 0.7,
                    ease: [0.6, 0.01, -0.05, 0.9],
                    delay: firstAnimation ? 1 : 0.3,
                  },
                }}
                exit={{
                  opacity: 0,
                  transition: {
                    duration: 0.7,
                    ease: [0.6, 0.01, -0.05, 0.9],
                    delay: 0.1,
                  },
                }}
                onClick={() => goToNextSlide()}
              >
                <motion.h3 className="text-sm sm:text-base font-semibold text-beige group-hover:text-white transition duration-500 ease-in-out">
                  {events[currentSlide].fields.name}
                </motion.h3>
                <div className="ml-1.5 group-hover:ml-3 flex rounded-full h-5 w-5 items-center justify-center bg-medium-gray/40 group-hover:bg-medium-gray/70 transition-all duration-500 ease-in-out">
                  <ArrowSmRightIcon className="h-3 w-3 text-beige " />
                </div>
              </motion.div>
            </div>
          )}
          {events && events.length != 0 && currentSlide == events.length && (
            <div className="flex flex-col items-end" key="main-next">
              <motion.div
                className="flex flex-row items-center group cursor-pointer"
                initial={{ opacity: 0, x: 10 }}
                animate={{
                  opacity: 1,
                  x: 0,
                  transition: {
                    duration: 1,
                    ease: [0.6, 0.01, -0.05, 0.9],
                  },
                }}
                exit={{
                  opacity: 0,
                  transition: {
                    duration: 1,
                    ease: [0.6, 0.01, -0.05, 0.9],
                  },
                }}
                onClick={() => goToNextSlide()}
              >
                <motion.span className="text-xs font-bold uppercase text-beige-dark group-hover:text-white transition duration-500 ease-in-out">
                  Početna
                </motion.span>
                <div className="ml-1.5 group-hover:ml-3 flex rounded-full h-5 w-5 items-center justify-center bg-medium-gray/40 group-hover:bg-medium-gray/70 transition-all duration-500 ease-in-out">
                  <ArrowSmRightIcon className="h-3 w-3 text-beige " />
                </div>
              </motion.div>
            </div>
          )}
        </AnimatePresence>

        {events && events.length != 0 && (
          <motion.div
            className="flex flex-row justify-end mt-3 sm:mt-5"
            key="navigation"
            initial={{ opacity: 0, x: 10 }}
            animate={{
              opacity: 1,
              x: 0,
              transition: {
                duration: 1,
                ease: [0.6, 0.01, -0.05, 0.9],
                delay: 0.7,
              },
            }}
          >
            <div
              className="mr-4 group flex rounded-full h-8 w-8 sm:h-9 sm:w-9 hover:scale-105 items-center justify-center bg-medium-gray/40 hover:bg-beige cursor-pointer border border-beige/60 transition duration-500 ease-in-out"
              onClick={() => goToPrevSlide()}
            >
              <ArrowSmRightIcon className="h-4 w-4 sm:h-5 sm:w-5 text-beige group-hover:text-medium-gray transition duration-500 ease-in-out rotate-180" />
            </div>
            <div
              className="group flex rounded-full h-8 w-8 sm:h-9 sm:w-9 hover:scale-105 items-center justify-center bg-medium-gray/40 hover:bg-beige cursor-pointer border border-beige/60 transition duration-500 ease-in-out"
              onClick={() => goToNextSlide()}
            >
              <ArrowSmRightIcon className="h-4 w-4 sm:h-5 sm:w-5 text-beige group-hover:text-medium-gray transition duration-500 ease-in-out" />
            </div>
          </motion.div>
        )}
      </div>

      <Swiper
        spaceBetween={0}
        slidesPerView={1}
        onSlideChange={(slider) => setSlide(slider.realIndex)}
        className="w-full h-full"
        onSwiper={(swiper) => {
          swiperRef.current = swiper;
        }}
        speed={700}
        rewind
        modules={[Keyboard, Autoplay]}
        keyboard={{ enabled: true, onlyInViewport: true }}
        autoplay={true}
        onAutoplay={(swiper) => {
          // stopOnAutoplay(swiper);
        }}
      >
        <SwiperSlide className="bg-background-black">
          <CarouselMain key="main" firstAnimation={firstAnimation} />
        </SwiperSlide>
        {events.map((eventData) => {
          return (
            <SwiperSlide
              key={eventData.fields.slug}
              className="bg-background-black"
            >
              <CarouselEvent
                eventData={eventData.fields}
                key={eventData.fields.slug}
              />
            </SwiperSlide>
          );
        })}
      </Swiper>
    </div>
  );
};

export default CarouselComp;
