import Link from "next/link";
import Image from "next/image";
import { motion } from "framer-motion";

import ArrowSmRightIcon from "@heroicons/react/20/solid/ArrowSmallRightIcon";

import {
  getCloudinaryThumbLink,
  getFormattedDate,
  getFormattedYear,
} from "../utils/helperFunctions";

export default function NewsCard(props) {
  // All possible color variants
  const mainColorVariants = {
    krakon: "text-beige",
    huskaj: "text-beige",
    default: "text-beige",
  };

  const accentColorVariants = {
    krakon: "text-krakon-purple",
    huskaj: "text-huskaj-red",
    default: "text-krapinjon-orange",
  };

  const iconColorVariants = {
    krakon: "text-krakon-purple group-hover:text-beige",
    huskaj: "text-huskaj-red group-hover:text-beige",
    default: "text-beige group-hover:text-medium-gray",
  };

  const bgColorVariants = {
    krakon: "bg-medium-gray group-hover:bg-krakon-purple",
    huskaj: "bg-medium-gray group-hover:bg-huskaj-red",
    default: "bg-medium-gray group-hover:bg-beige",
  };

  const titleColorVariants = {
    krakon: "text-beige group-hover:text-krakon-purple-dark",
    huskaj: "text-beige group-hover:text-huskaj-red-dark",
    default: "text-beige group-hover:text-beige-dark",
  };

  const highlightImage = props.highlightImage;

  const { title, slug, thumbnail, tags } = props.article.fields;
  const date = getFormattedDate(props.article.sys.createdAt);
  const year = getFormattedYear(props.article.sys.createdAt);

  let thumbLink = getCloudinaryThumbLink(thumbnail[0].original_secure_url);

  // Custom colors
  const isGrayscale = props.isGrayscale || false;
  const variant = props.variant || "default";

  const subpage = props.subpage;

  return (
    <Link
      href={`/${subpage != null ? subpage + "/" : ""}novosti/` + slug}
      passHref
    >
      <motion.div
        className="flex flex-col bg-none w-full rounded-xl transition duration-500 ease-in-out cursor-pointer group"
        initial={{ opacity: 0, y: 20 }}
        whileInView={{ opacity: 1, y: 0 }}
        viewport={{ margin: "-70px 0px -70px 0px", once: true }}
      >
        <div className="w-full flex flex-row justify-between">
          <motion.div className="relative w-full h-full overflow-hidden rounded-xl">
            <div className="absolute z-20 w-full h-full rounded-xl bg-gradient-to-r from-black/80 via-black/30 to-black/80 flex flex-col p-3 justify-between">
              <div className="flex flex-col">
                <motion.span
                  className={`text-2xl font-semibold ${mainColorVariants[variant]} leading-none overflow-hidden`}
                  initial={{
                    opacity: 0,
                    y: 20,
                  }}
                  animate={{
                    opacity: 1,
                    y: 0,
                    transition: {
                      ease: [0.6, 0.01, -0.05, 0.95],
                      duration: 0.6,
                      delay: 0.1,
                    },
                  }}
                >
                  {date}
                </motion.span>
                <motion.span
                  className={`text-sm font-semibold ${mainColorVariants[variant]} leading-none`}
                  initial={{
                    opacity: 0,
                    y: 20,
                  }}
                  animate={{
                    opacity: 1,
                    y: 0,
                    transition: {
                      ease: [0.6, 0.01, -0.05, 0.95],
                      duration: 0.6,
                      delay: 0.2,
                    },
                  }}
                >
                  {year}
                </motion.span>
              </div>
              <div className="w-full flex flex-row justify-between">
                <div>
                  {tags.map((tag, index) => {
                    return (
                      <motion.span
                        key={index}
                        className={`text-xxs font-semibold ${mainColorVariants[variant]} mr-2`}
                        initial={{
                          opacity: 0,
                          x: -20,
                        }}
                        animate={{
                          opacity: 1,
                          x: 0,
                          transition: {
                            ease: [0.6, 0.01, -0.05, 0.95],
                            duration: 0.5,
                            delay: 0.3 * index,
                          },
                        }}
                      >
                        {tag}
                      </motion.span>
                    );
                  })}
                </div>
                <div
                  className={`flex rounded-full h-5 w-5 items-center justify-center transition duration-500 ease-in-out scale-0 group-hover:scale-100 ${bgColorVariants[variant]}`}
                >
                  <ArrowSmRightIcon
                    className={`h-3 w-3 ${iconColorVariants[variant]} transition duration-500 ease-in-out`}
                  />
                </div>
              </div>
            </div>

            <motion.div
              className={
                highlightImage == true
                  ? "w-full rounded-xl h-80 overflow-hidden will-change-transform"
                  : "w-full rounded-xl h-52 overflow-hidden will-change-transform"
              }
              initial={{
                opacity: 0,
                scale: 1.3,
              }}
              animate={{
                opacity: 1,
                scale: 1,
                transition: {
                  ease: [0.6, 0.01, -0.05, 0.95],
                  duration: 0.6,
                },
              }}
            >
              {isGrayscale && (
                <div
                  className="absolute z-10 w-full h-full rounded-xl mix-blend-multiply opacity-0 group-hover:opacity-100 transition duration-500 ease-in-out"
                  style={{
                    backgroundColor:
                      variant == "krakon" ? "#861FC5" : "#DC143C",
                  }}
                ></div>
              )}

              <Image
                src={thumbnail[0].original_secure_url}
                blurDataURL={thumbLink}
                width={thumbnail[0].width}
                height={thumbnail[0].height}
                sizes="384px"
                style={{
                  objectFit: "cover",
                  width: "100%",
                  height: "100%",
                  filter: isGrayscale ? "grayscale(100%)" : null,
                }}
                alt=""
                priority={highlightImage}
                className="motion-safe:group-hover:scale-105 rounded-xl transition duration-500 ease-in-out overflow-hidden will-change-transform"
              />
            </motion.div>
          </motion.div>
        </div>
        <div className="w-full pt-3">
          <motion.h2
            className={`text-left text-sm md:text-xl font-semibold ${titleColorVariants[variant]} transition duration-500 ease-in-out`}
            initial={{
              opacity: 0,
              x: -30,
            }}
            animate={{
              opacity: 1,
              x: 0,
              transition: {
                ease: "easeInOut",
                duration: 0.3,
              },
            }}
          >
            {title}
          </motion.h2>
        </div>
      </motion.div>
    </Link>
  );
}
