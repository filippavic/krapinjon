import React from "react";

import { motion } from "framer-motion";

export default function AnimatedWords({ title, accentColor }) {
  return (
    <motion.h1 className="z-50 mb-10">
      {title.split(/(\s+)/).map((word, index) => (
        <motion.span
          initial={{
            opacity: 0,
            y: 20,
          }}
          animate={{
            opacity: 1,
            y: 0,
            transition: {
              ease: [0.6, 0.01, -0.05, 0.95],
              duration: 0.7,
              delay: 0.03 * index,
            },
          }}
          className="text-3xl sm:text-5xl md:text-5xl font-semibold inline-block whitespace-pre"
          key={index}
          style={{
            color: accentColor ? accentColor : "#EBD6C2",
          }}
        >
          {word}
        </motion.span>
      ))}
    </motion.h1>
  );
}
