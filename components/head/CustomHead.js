import Head from "next/head";

export default function CustomHead({ metaTags }) {
  const { title, description, url, previewImageUrl, accentColor } = metaTags;

  return (
    <Head>
      <title>{title}</title>

      <meta name="description" content={description} />
      <meta name="copyright" content="Filip Pavić/Udruga Krapinjon" />

      {/* Theme color */}
      <meta name="theme-color" content={accentColor} />
      <meta name="msapplication-navbutton-color" content={accentColor} />
      <meta
        name="apple-mobile-web-app-status-bar-style"
        content={accentColor}
      />

      {/* Open Graph / Facebook */}
      <meta property="og:type" content="website" />
      <meta property="og:url" content={url} />
      <meta property="og:title" content={title} />
      <meta property="og:description" content={description} />
      <meta property="og:image" content={previewImageUrl} />

      {/* Twitter */}
      <meta name="twitter:card" content="summary_large_image" />
      <meta name="twitter:url" content={url} />
      <meta name="twitter:title" content={title} />
      <meta name="twitter:description" content={description} />
      <meta name="twitter:image" content={previewImageUrl} />

      <link rel="icon" href="/favicon.ico" />
    </Head>
  );
}
