import React, { useEffect, useState } from "react";
import Link from "next/link";
import Image from "next/image";
import { motion } from "framer-motion";

import ArrowSmRightIcon from "@heroicons/react/20/solid/ArrowSmallRightIcon";

import {
  getCloudinaryThumbLink,
  dateTimeToString,
  getEventLink,
} from "../utils/helperFunctions";

// Title animation
const AnimatedWords = ({ title }) => (
  <motion.h1
    className="text-center md:text-left pl-4 pr-4 md:pl-0 md:pr-0"
    initial={{
      opacity: 0,
      y: 20,
    }}
    whileInView={{
      opacity: 1,
      y: 0,
      transition: {
        ease: [0.6, 0.01, -0.05, 0.95],
        duration: 0.8,
        delay: 0.3,
      },
    }}
    exit={{
      opacity: 0,
      y: 10,
      transition: {
        ease: [0.6, 0.01, -0.05, 0.95],
        duration: 0.7,
      },
    }}
  >
    {title.split(/(\s+)/).map((word, index) => (
      <div
        className="text-4xl sm:text-6xl font-display text-beige inline-block whitespace-pre"
        key={index}
      >
        {word}
      </div>
    ))}
  </motion.h1>
);

const CarouselEvent = ({ eventData }) => {
  const {
    name,
    slug,
    thumbnail,
    type,
    location,
    startDateTime,
    endDateTime,
    allDay,
    accentColor,
    infoLink,
  } = eventData;

  // Resize image based on scroll postion
  // Removed for performance reasons
  // const { scrollY } = useScroll();
  // const imageScroll = useTransform(scrollY, [0, 500], [1, 1.15]);

  // Accessibility
  // const shouldReduceMotion = useReducedMotion();

  // Time of event as a string
  const [eventDateTime, setEventDateTime] = useState("");

  // Blurred thumbnail link
  let thumbLink = getCloudinaryThumbLink(thumbnail[0].original_secure_url);

  // More information link
  let link = getEventLink(infoLink, slug);

  useEffect(() => {
    // Set date and time client-side
    let eventDateTime = dateTimeToString(startDateTime, endDateTime, allDay);
    setEventDateTime(eventDateTime);

    // eslint-disable-next-line react-hooks/exhaustive-deps
  }, []);

  return (
    <motion.div
      className={`relative w-full h-full flex justify-center items-center overflow-hidden`}
    >
      <div className="absolute z-20 w-full h-full pl-0 md:pl-16 bg-gradient-to-b from-black/20 to-background-black flex flex-col justify-center items-center md:items-start">
        <motion.span
          className="text-xs uppercase font-extrabold mb-1"
          style={{
            color: accentColor ? accentColor : "#FC8A17",
          }}
          initial={{ opacity: 0, y: 15 }}
          whileInView={{
            opacity: 1,
            y: 0,
            transition: {
              duration: 0.7,
              ease: [0.6, 0.01, -0.05, 0.9],
            },
          }}
          exit={{
            opacity: 0,
            y: 15,
            transition: {
              duration: 0.7,
              ease: [0.6, 0.01, -0.05, 0.9],
            },
          }}
        >
          {type}
        </motion.span>
        <AnimatedWords title={name} key={name} />
        <motion.h2
          className="mt-8 sm:mt-10 text-base sm:text-xl font-semibold text-beige leading-none overflow-hidden"
          initial={{ opacity: 0, y: 15 }}
          whileInView={{
            opacity: 1,
            y: 0,
            transition: {
              duration: 0.7,
              ease: [0.6, 0.01, -0.05, 0.9],
              delay: 0.1,
            },
          }}
          exit={{
            opacity: 0,
            y: 0,
            transition: {
              duration: 0.7,
              ease: [0.6, 0.01, -0.05, 0.9],
              delay: 0.1,
            },
          }}
        >
          {eventDateTime}
        </motion.h2>
        <motion.h2
          className="mt-2 sm:mt-3 text-base sm:text-xl font-semibold text-beige leading-none overflow-hidden"
          initial={{ opacity: 0, y: 15 }}
          whileInView={{
            opacity: 1,
            y: 0,
            transition: {
              duration: 0.7,
              ease: [0.6, 0.01, -0.05, 0.9],
              delay: 0.2,
            },
          }}
          exit={{
            opacity: 0,
            y: 0,
            transition: {
              duration: 0.7,
              ease: [0.6, 0.01, -0.05, 0.9],
              delay: 0.2,
            },
          }}
        >
          {location}
        </motion.h2>
        <Link href={link.url} prefetch={false} passHref legacyBehavior>
          <motion.a
            className="group flex flex-row items-center cursor-pointer mt-10"
            initial={{ opacity: 0, y: 15 }}
            whileInView={{
              opacity: 1,
              y: 0,
              transition: {
                duration: 0.7,
                ease: [0.6, 0.01, -0.05, 0.9],
                delay: 0.3,
              },
            }}
            exit={{
              opacity: 0,
              y: 0,
              transition: {
                duration: 0.7,
                ease: [0.6, 0.01, -0.05, 0.9],
                delay: 0.3,
              },
            }}
            target={link.external ? "_blank" : undefined}
            rel={link.external ? "noopener noreferrer" : undefined}
          >
            <span className="text-sm text-beige font-semibold opacity-80 group-hover:opacity-100 group-hover:pl-2 transition-all duration-500 ease-in-out">
              Više
            </span>
            <div
              className="ml-1.5 group-hover:ml-3 flex rounded-full h-5 w-5 items-center justify-center transition-all duration-500 ease-in-out group-hover:scale-110"
              style={{
                backgroundColor: accentColor ? accentColor : "#FC8A17",
              }}
            >
              <ArrowSmRightIcon className="h-3 w-3 text-white" />
            </div>
          </motion.a>
        </Link>
      </div>

      <div
        className="absolute z-10 w-full h-full mix-blend-multiply"
        style={{
          backgroundColor: accentColor ? accentColor : "#FC8A17",
        }}
      ></div>

      <div className="absolute z-10 w-full h-full bg-texture-image mix-blend-multiply"></div>

      <motion.div
        className="w-full h-full relative"
        initial={{
          opacity: 0,
        }}
        whileInView={{
          opacity: 1,
          transition: {
            duration: 1,
            ease: [0.6, 0.01, -0.05, 0.9],
          },
        }}
        exit={{
          opacity: 0,
          transition: {
            duration: 1,
            ease: [0.6, 0.01, -0.05, 0.9],
          },
        }}
        // Removed for performance reasons
        // style={{ scale: shouldReduceMotion ? 1 : imageScroll }}
      >
        <Image
          src={thumbnail[0].original_secure_url}
          sizes="100vw"
          blurDataURL={thumbLink}
          placeholder="blur"
          fill
          alt=""
          className="grayscale"
          quality={68}
          style={{ objectFit: "cover" }}
          fetchpriority="low"
        />
      </motion.div>
    </motion.div>
  );
};

export default CarouselEvent;
