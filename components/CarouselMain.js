import React from "react";
import Image from "next/image";
import { motion } from "framer-motion";

import backgroundImage from "../public/images/landing_background.jpg";

// Title animation
// const AnimatedWords = ({ title, shouldReduceMotion, scroll }) => (
const AnimatedWords = ({ title }) => (
  <motion.h1
    className="text-center pl-4 pr-4 md:pl-0 md:pr-0"
    // style={{ scale: shouldReduceMotion ? 1 : scroll }}
  >
    {title.split(/(\s+)/).map((word, index) => (
      <motion.span
        initial={{
          opacity: 0,
          y: 20,
        }}
        animate={{
          opacity: 1,
          y: 0,
          transition: {
            ease: [0.6, 0.01, -0.05, 0.95],
            duration: 0.7,
            delay: 0.03 * index,
          },
        }}
        exit={{
          opacity: 0,
          y: 20,
          transition: {
            ease: [0.6, 0.01, -0.05, 0.95],
            duration: 0.8,
            delay: 0.025 * index,
          },
        }}
        className="text-large md:text-huge font-display text-beige block leading-none"
        key={index}
      >
        {word}
      </motion.span>
    ))}
  </motion.h1>
);

const CarouselMain = ({ firstAnimation }) => {
  // Resize image and text based on scroll postion
  // const { scrollY } = useScroll();
  // const imageScroll = useTransform(scrollY, [0, 500], [1, 1.15]);
  // const textScroll = useTransform(scrollY, [0, 500], [1, 1.08]);

  // Accessibility
  // const shouldReduceMotion = useReducedMotion();

  return (
    <motion.div className="relative w-full h-full flex justify-center items-center overflow-hidden">
      <div className="absolute z-20 w-full h-full bg-gradient-to-b from-black/20 to-background-black flex flex-col justify-center items-center">
        <AnimatedWords
          title={"Zapali svoju vatru."}
          // shouldReduceMotion={shouldReduceMotion}
          // scroll={textScroll}
        />
      </div>

      <div
        className="absolute z-10 w-full h-full mix-blend-multiply"
        style={{
          backgroundColor: "#FC8A17",
        }}
      ></div>

      <div className="absolute z-10 w-full h-full bg-texture-image mix-blend-multiply"></div>

      <motion.div
        className="w-full h-full relative"
        initial={{
          opacity: 0,
        }}
        animate={{
          opacity: 1,
          transition: {
            duration: firstAnimation ? 1.5 : 1,
            ease: [0.6, 0.01, -0.05, 0.9],
            delay: firstAnimation ? 0.7 : 0,
          },
        }}
        exit={{
          opacity: 0,
          transition: {
            duration: 1,
            ease: [0.6, 0.01, -0.05, 0.9],
          },
        }}
        // Removed for performance reasons
        // style={{ scale: shouldReduceMotion ? 1 : imageScroll }}
      >
        <Image
          src={backgroundImage}
          sizes="100vw"
          fill
          alt=""
          className="grayscale"
          priority
          quality={68}
          style={{ objectFit: "cover" }}
        />
      </motion.div>
    </motion.div>
  );
};

export default CarouselMain;
