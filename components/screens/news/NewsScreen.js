import { motion } from "framer-motion";

// Components
import NewsCard from "../../NewsCard";
import Pagination from "../../Pagination";
import ScrollParagraph from "../../shared/typography/scroll/ScrollParagraph";

// Functions and helpers
import { accentColorVariants, variants } from "../../../styles/colorVariants";
import otherAnimations from "../../../utils/otherAnimations";

export default function NewsScreen({ news, totalPages, currentPage, variant }) {
  const nextDisabled = parseInt(currentPage, 10) === parseInt(totalPages, 10);
  const prevDisabled = parseInt(currentPage, 10) === 1;

  let accentColor = accentColorVariants[variant];
  let subpage = variant == variants.krapinjon ? null : variant;
  let isGrayscale = variant != variants.krapinjon;

  return (
    <div className="flex flex-col content-center">
      <div className="w-full h-72 items-center justify-center flex">
        <div className="w-10/12 max-w-7xl flex overflow-hidden">
          <motion.h1
            className={`flex self-center justify-center font-display text-5xl ${accentColor}`}
            variants={otherAnimations.titleAnimation}
            initial="initial"
            animate="animate"
          >
            Novosti
          </motion.h1>
        </div>
      </div>

      <div className="self-center mt-0 mb-20 lg:mb-28 w-10/12 max-w-7xl">
        <div className="justify-items-center">
          {news && news.length == 0 && (
            <ScrollParagraph text={"Trenutno nema novosti."} />
          )}
        </div>

        {/* Highlighted news card */}
        {news && news.length > 0 && (
          <NewsCard
            key={0}
            article={news[0]}
            index={0}
            highlightImage={true}
            isGrayscale={isGrayscale}
            subpage={subpage}
            variant={variant}
          />
        )}

        {/* Other news cards */}
        <div className="w-full grid grid-cols-1 md:grid-cols-3 gap-10 mt-10">
          {news &&
            news.length > 1 &&
            news.slice(1).map((article, index) => {
              return (
                <NewsCard
                  key={index + 1}
                  article={article}
                  index={index + 1}
                  highlightImage={false}
                  isGrayscale={isGrayscale}
                  subpage={subpage}
                  variant={variant}
                />
              );
            })}
        </div>
      </div>

      {news && news.length != 0 && (
        <div className="self-center mt-0 w-60 sm:w-80">
          <Pagination
            totalPages={totalPages}
            currentPage={currentPage}
            nextDisabled={nextDisabled}
            prevDisabled={prevDisabled}
            subpage={subpage}
          />
        </div>
      )}
    </div>
  );
}

NewsScreen.defaultProps = {
  variant: variants.krapinjon,
};
