import { useEffect, useMemo, useState } from "react";
import { motion } from "framer-motion";

// Components
import HeroImage from "../../shared/HeroImage";
import AnimatedWords from "../../AnimatedWords";
import TitleH2 from "../../shared/typography/TitleH2";
import ImageGrid from "../../shared/ImageGrid";
import LightboxGallery from "../../shared/LightboxGallery";
import ShareComp from "../../ShareComp";
import SessionizeBanner from "../../shared/SessionizeBanner";

// Functions and helpers
import { hoverVariants, variants } from "../../../styles/colorVariants";
import {
  getCloudinaryThumbLink,
  getFormattedDateYear,
  getFormattedDateYearTime,
} from "../../../utils/helperFunctions";
import { documentToReactComponents } from "@contentful/rich-text-react-renderer";
import { getVariantTextRenderOptions } from "../../../styles/textStyles";

export default function ArticleScreen({
  article,
  publishDateTime,
  url,
  variant,
}) {
  const { title, thumbnail, tags, text, images, documents } = article;

  const [dateTimes, setDateTimes] = useState({
    publishDate: "",
    publishTime: "",
  });

  let thumbLink = getCloudinaryThumbLink(thumbnail[0].original_secure_url);
  let textRenderOptions = getVariantTextRenderOptions(variant);
  let hoverColor = hoverVariants[variant];

  // Tracks the image to be displayed in gallery
  const [galleryIndex, setGalleryIndex] = useState(null);

  useEffect(() => {
    // Set date and time client-side
    let publishDate = getFormattedDateYear(publishDateTime);
    let publishTime = getFormattedDateYearTime(publishDateTime);

    setDateTimes({
      publishDate: publishDate,
      publishTime: publishTime,
    });
    // eslint-disable-next-line react-hooks/exhaustive-deps
  }, []);

  const heroSection = useMemo(() => {
    return (
      <motion.div className="absolute w-full h-4/5 overflow-hidden">
        <HeroImage
          imageSrc={thumbnail[0].original_secure_url}
          imageBlurUrl={thumbLink}
          variant={variant}
        />
      </motion.div>
    );
  }, []);

  const titleSection = useMemo(() => {
    return (
      <motion.div className="flex flex-col relative w-full h-80 justify-end overflow-hidden">
        <AnimatedWords title={title} />
        <TitleH2 text={dateTimes.publishDate} />
        {title.includes("program") && <SessionizeBanner />}
      </motion.div>
    );
  }, [dateTimes]);

  const articleTextSection = useMemo(() => {
    return (
      <div className="flex flex-col w-full md:w-3/5">
        <motion.div
          initial={{ opacity: 0, y: 15 }}
          animate={{
            opacity: 1,
            y: 0,
            transition: {
              duration: 1,
              ease: [0.6, 0.01, -0.05, 0.9],
            },
          }}
        >
          {documentToReactComponents(text, textRenderOptions)}
        </motion.div>
      </div>
    );
  }, []);

  const leftSection = useMemo(() => {
    return (
      <div className="flex flex-col w-full md:w-2/5 justify-between md:justify-start mt-16 md:mt-0">
        <motion.div className="w-min flex flex-row md:flex-col h-min">
          {tags.map((tag, index) => {
            return (
              <motion.span
                key={index}
                className="text-sm font-semibold text-beige mb-0 md:mb-3 mr-3 md:mr-0"
                initial={{ opacity: 0, x: -10 }}
                animate={{
                  opacity: 1,
                  x: 0,
                  transition: {
                    duration: 1,
                    ease: [0.6, 0.01, -0.05, 0.9],
                    delay: 0.1 * index,
                  },
                }}
              >
                {tag}
              </motion.span>
            );
          })}
        </motion.div>
        {documents !== undefined ? (
          <motion.div className="mt-8 md:mt-16">
            <motion.span
              className="text-sm text-beige font-semibold block"
              initial={{ opacity: 0, x: -10 }}
              animate={{
                opacity: 1,
                x: 0,
                transition: {
                  duration: 1,
                  ease: [0.6, 0.01, -0.05, 0.9],
                },
              }}
            >
              Prilozi
            </motion.span>
            <div className="mt-1">
              {documents.map((document, index) => {
                return (
                  <motion.a
                    className={`text-beige-dark font-semibold ${hoverColor} transition duration-300 ease-in-out block`}
                    href={"https:" + document.fields.file.url}
                    key={index}
                    target="_blank"
                    rel="noreferrer"
                    initial={{ opacity: 0, x: -10 }}
                    animate={{
                      opacity: 1,
                      x: 0,
                      transition: {
                        duration: 0.5,
                        ease: [0.6, 0.01, -0.05, 0.9],
                        delay: 0.05 * index,
                      },
                    }}
                  >
                    {`${document.fields.title} (.${document.fields.file.fileName
                      .split(".")
                      .pop()})`}
                  </motion.a>
                );
              })}
            </div>
          </motion.div>
        ) : (
          <></>
        )}
      </div>
    );
  }, []);

  const imageSection = useMemo(() => {
    if (images === undefined) {
      return <></>;
    } else {
      return (
        <motion.div className="mt-20">
          <motion.span
            className="text-beige font-semibold text-sm"
            initial={{ opacity: 0, y: 10 }}
            whileInView={{
              opacity: 1,
              y: 0,
              transition: {
                duration: 1,
                ease: [0.6, 0.01, -0.05, 0.9],
              },
            }}
            viewport={{
              margin: "-100px 0px -100px 0px",
              once: true,
            }}
          >
            Galerija slika
          </motion.span>

          <ImageGrid
            key="image-grid"
            images={images}
            onImageClick={(index) => setGalleryIndex(index)}
          />
        </motion.div>
      );
    }
  }, []);

  const shareSection = useMemo(() => {
    return (
      <motion.div
        className="flex flex-col mt-16"
        initial={{ opacity: 0, y: 15 }}
        whileInView={{
          opacity: 1,
          y: 0,
          transition: {
            duration: 0.8,
            ease: [0.6, 0.01, -0.05, 0.9],
          },
        }}
        viewport={{
          margin: "-50px 0px -50px 0px",
          once: true,
        }}
      >
        <span className="text-beige font-semibold text-sm">Podijeli</span>
        <ShareComp url={url} />
      </motion.div>
    );
  }, []);

  const publishedSection = useMemo(() => {
    return (
      <motion.div
        className="flex flex-col mt-16"
        initial={{ opacity: 0, y: 15 }}
        whileInView={{
          opacity: 1,
          y: 0,
          transition: {
            duration: 0.8,
            ease: [0.6, 0.01, -0.05, 0.9],
          },
        }}
        viewport={{
          margin: "-50px 0px -50px 0px",
          once: true,
        }}
      >
        <span className="text-beige-dark text-sm">
          Objavljeno: {dateTimes.publishTime}
        </span>
      </motion.div>
    );
  }, [dateTimes]);

  return (
    <div className="flex flex-col">
      <LightboxGallery
        images={images}
        galleryIndex={galleryIndex}
        setGalleryIndex={setGalleryIndex}
      />

      {heroSection}

      <motion.div className="self-center mt-h-1/3 md:mt-h-1/4 w-10/12 rounded-xl max-w-screen-2xl">
        {titleSection}

        <div className="flex flex-col-reverse md:flex-row w-full h-auto relative z-50 mt-24">
          {leftSection}
          {articleTextSection}
        </div>

        {imageSection}
        {shareSection}
        {publishedSection}
      </motion.div>
    </div>
  );
}

ArticleScreen.defaultProps = {
  variant: variants.krapinjon,
};
