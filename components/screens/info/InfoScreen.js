import { useEffect, useState } from "react";
import { motion } from "framer-motion";

// Icons
import HomeIcon from "@heroicons/react/20/solid/HomeIcon";
import LocationMarkerIcon from "@heroicons/react/20/solid/MapPinIcon";
import RocketLaunchIcon from "@heroicons/react/20/solid/RocketLaunchIcon";
import HeartIcon from "@heroicons/react/20/solid/HeartIcon";
import MapIcon from "@heroicons/react/20/solid/MapIcon";
import CalendarIcon from "@heroicons/react/20/solid/CalendarDaysIcon";

// Components
import HeroImage from "../../shared/HeroImage";
import AnimatedWords from "../../AnimatedWords";
import TitleH2 from "../../shared/typography/TitleH2";
import ScrollH3 from "../../shared/typography/scroll/ScrollH3";
import ScrollH4 from "../../shared/typography/scroll/ScrollH4";
import ScrollParagraph from "../../shared/typography/scroll/ScrollParagraph";
import Divider from "../../shared/Divider";
import TextFeature from "../../shared/typography/TextFeature";
import TextPairs from "../../shared/typography/TextPairs";
import LinkButton from "../../shared/buttons/LinkButton";
import Accordion from "../../shared/accordion/Accordion";
import DocumentList from "../../shared/DocumentList";
import SponsorGrid from "../../shared/SponsorGrid";
import InfoParagraph from "../../shared/elements/InfoParagraph";

// Functions and helpers
import { documentToReactComponents } from "@contentful/rich-text-react-renderer";
import { accentColorVariants, variants } from "../../../styles/colorVariants";
import { getVariantTextRenderOptions } from "../../../styles/textStyles";
import {
  dateTimeToString,
  getCloudinaryThumbLink,
} from "../../../utils/helperFunctions";

export default function InfoScreen({ eventData, variant }) {
  const {
    eventTitle,
    startDateTime,
    allDayEvent,
    endDateTime,
    locationName,
    locationForMaps,
    prices,
    discounts,
    information,
    frequentlyAsked,
    hospitality,
    transit,
    parking,
    accessibility,
    guidelines,
    documents,
    sponsors,
    thumbnail,
  } = eventData;

  let accentColor = accentColorVariants[variant];

  let textRenderOptions = getVariantTextRenderOptions(variant);

  let thumbLink = getCloudinaryThumbLink(thumbnail[0].original_secure_url);

  const [eventDateTime, setEventDateTime] = useState("");

  useEffect(() => {
    // Set date and time client-side
    let eventDateTime = dateTimeToString(
      startDateTime,
      endDateTime,
      allDayEvent
    );
    setEventDateTime(eventDateTime);

    // eslint-disable-next-line react-hooks/exhaustive-deps
  }, []);

  return (
    <div className="flex flex-col">
      <motion.div className="absolute w-full h-4/5 overflow-hidden">
        <HeroImage
          imageSrc={thumbnail[0].original_secure_url}
          imageBlurUrl={thumbLink}
          variant={variant}
        />
      </motion.div>

      <motion.div className="self-center mt-h-1/3 w-10/12 rounded-xl max-w-screen-2xl">
        <motion.div className="flex flex-col relative w-full h-80 justify-end overflow-hidden">
          <AnimatedWords title={`${eventTitle} - informacije`} />
          <TitleH2
            text={eventDateTime}
            icon={<CalendarIcon className="h-4 w-4 text-beige mr-2" />}
          />
        </motion.div>

        {/* Main information */}
        <div className="flex flex-row w-full h-auto relative z-50 mt-24">
          <div className="flex flex-col w-full">
            <motion.div
              initial={{ opacity: 0, y: 15 }}
              animate={{
                opacity: 1,
                y: 0,
                transition: {
                  duration: 1,
                  ease: [0.6, 0.01, -0.05, 0.9],
                },
              }}
            >
              {documentToReactComponents(information, textRenderOptions)}
            </motion.div>
          </div>
        </div>

        {/* Divider for prices */}
        {prices !== undefined && <Divider id="divider-1" variant={variant} />}

        {/* Prices */}
        {prices !== undefined ? (
          <div className="w-full flex flex-col relative z-50 justify-center items-center">
            <ScrollH3 text={"Kotizacije"} />

            <div className="mt-0 md:mt-8 flex flex-col md:flex-row w-full max-w-4xl items-center">
              {prices.length == 0 && (
                <ScrollParagraph
                  text={"Kotizacije će biti objavljene uskoro."}
                ></ScrollParagraph>
              )}
              {prices.map((price) => {
                return (
                  <TextFeature
                    key={price.id}
                    id={price.id}
                    title={price.key}
                    text={price.value}
                    variant={variant}
                  />
                );
              })}
            </div>
          </div>
        ) : (
          <></>
        )}

        {/* Divider for discounts */}
        {discounts !== undefined && discounts.length > 0 && (
          <Divider id="divider-discounts" variant={variant} />
        )}

        {/* Discounts */}
        {discounts !== undefined && discounts.length > 0 ? (
          <div className="w-full flex flex-col justify-center items-center">
            <ScrollH4 text={"Popusti"} />

            <div className="mt-0 md:mt-8 flex flex-col w-full justify-between items-center">
              {discounts.length == 0 && (
                <ScrollParagraph
                  text={"Za ovaj događaj nema popusta."}
                ></ScrollParagraph>
              )}
              {discounts.map((discount) => {
                return (
                  <TextPairs
                    key={discount.id}
                    id={discount.id}
                    title={discount.key}
                    text={discount.value}
                    variant={variant}
                  />
                );
              })}
            </div>
          </div>
        ) : (
          <></>
        )}

        {/* Divider for location */}
        {locationName !== undefined && (
          <Divider id="divider-2" variant={variant} />
        )}

        {/* Location */}
        {locationName !== undefined ? (
          <div className="w-full">
            <ScrollH3 text={"Lokacija"} />

            <div className="mt-4 flex w-full">
              <LinkButton
                id={"location-link"}
                title={locationName}
                link={locationForMaps}
                icon={
                  <LocationMarkerIcon
                    className={`h-5 w-5 ${accentColor} mr-1`}
                  />
                }
                variant={variant}
              />
            </div>
          </div>
        ) : (
          <></>
        )}

        {/* Hospitality */}
        {hospitality !== undefined ? (
          <InfoParagraph
            id="hospitality"
            title="Smještaj"
            icon={<HomeIcon className={`h-5 w-5 ${accentColor} mr-1`} />}
            content={documentToReactComponents(hospitality, textRenderOptions)}
            variant={variant}
          />
        ) : (
          <></>
        )}

        {/* Transit */}
        {transit !== undefined ? (
          <InfoParagraph
            id="transit"
            title="Prijevoz"
            icon={
              <RocketLaunchIcon className={`h-5 w-5 ${accentColor} mr-1`} />
            }
            content={documentToReactComponents(transit, textRenderOptions)}
            variant={variant}
          />
        ) : (
          <></>
        )}

        {/* Parking */}
        {parking !== undefined ? (
          <InfoParagraph
            id="parking"
            title="Parking"
            icon={<MapIcon className={`h-5 w-5 ${accentColor} mr-1`} />}
            content={documentToReactComponents(parking, textRenderOptions)}
            variant={variant}
          />
        ) : (
          <></>
        )}

        {/* Accessibility */}
        {accessibility !== undefined ? (
          <InfoParagraph
            id="accessibility"
            title="Pristupačnost"
            icon={<HeartIcon className={`h-5 w-5 ${accentColor} mr-1`} />}
            content={documentToReactComponents(
              accessibility,
              textRenderOptions
            )}
            variant={variant}
          />
        ) : (
          <></>
        )}

        {/* Divider for FAQ */}
        {frequentlyAsked !== undefined && (
          <Divider id="divider-3" variant={variant} />
        )}

        {/* Frequently asked questions */}
        {frequentlyAsked !== undefined ? (
          <div className="">
            <ScrollH3 text={"Često postavljena pitanja"} />
            <div className="mt-8">
              <Accordion
                id={"faq-accordion"}
                items={frequentlyAsked}
                variant={variant}
              />
            </div>
          </div>
        ) : (
          <></>
        )}

        {/* Divider */}
        <Divider id="divider-4" variant={variant} />

        {/* Guidelines */}
        {guidelines !== undefined ? (
          <div>
            <ScrollH3 text={"Pravilnici"} />
            <DocumentList
              id={"guidelines"}
              title="Pravilnici"
              documents={guidelines}
              variant={variant}
            />
          </div>
        ) : (
          <></>
        )}

        {/* Documents */}
        {documents !== undefined ? (
          <div className="mt-20">
            <ScrollH3 text={"Dokumenti"} />
            <DocumentList
              id={"documents"}
              title="Dokumenti"
              documents={documents}
              variant={variant}
            />
          </div>
        ) : (
          <></>
        )}

        {/* Sponsors */}
        {sponsors !== undefined ? (
          <div className="mt-20">
            <ScrollH3 text={"Podržavatelji"} />
            <SponsorGrid id={"sponsors"} sponsors={sponsors} />
          </div>
        ) : (
          <></>
        )}
      </motion.div>
    </div>
  );
}

InfoScreen.defaultProps = {
  variant: variants.krapinjon,
};
