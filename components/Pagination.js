import Link from "next/link";

import ArrowSmRightIcon from "@heroicons/react/20/solid/ArrowSmallRightIcon";

export default function Pagination(props) {
  const { totalPages, currentPage, prevDisabled, nextDisabled, subpage } =
    props;

  const prevPageUrl =
    currentPage === "2"
      ? `/${subpage != null ? subpage + "/" : ""}novosti`
      : `/${subpage != null ? subpage + "/" : ""}novosti/stranica/${
          parseInt(currentPage, 10) - 1
        }`;

  const nextPageUrl = `/${
    subpage != null ? subpage + "/" : ""
  }novosti/stranica/${parseInt(currentPage, 10) + 1}`;

  return (
    <div className="w-full flex flex-row justify-between items-center">
      <div>
        {prevDisabled && (
          <div className="flex rounded-full h-9 w-9 items-center justify-center bg-dark-gray">
            <ArrowSmRightIcon className="h-5 w-5 text-medium-gray rotate-180" />
          </div>
        )}
        {!prevDisabled && (
          <Link href={prevPageUrl} passHref legacyBehavior>
            <div className="group flex rounded-full h-9 w-9 hover:scale-105 items-center justify-center bg-medium-gray hover:bg-beige cursor-pointer transition duration-500 ease-in-out">
              <ArrowSmRightIcon className="h-5 w-5 text-beige group-hover:text-medium-gray transition duration-500 ease-in-out rotate-180" />
            </div>
          </Link>
        )}
      </div>
      <div>
        <span className="text-beige text-xs font-semibold">
          Stranica {currentPage} od {totalPages}
        </span>
      </div>
      <div>
        {nextDisabled && (
          <div className="flex rounded-full h-9 w-9 items-center justify-center bg-dark-gray">
            <ArrowSmRightIcon className="h-5 w-5 text-medium-gray" />
          </div>
        )}
        {!nextDisabled && (
          <Link href={nextPageUrl} passHref legacyBehavior>
            <div className="group flex rounded-full h-9 w-9 hover:scale-105 items-center justify-center bg-medium-gray hover:bg-beige cursor-pointer transition duration-500 ease-in-out">
              <ArrowSmRightIcon className="h-5 w-5 text-beige group-hover:text-medium-gray transition duration-500 ease-in-out" />
            </div>
          </Link>
        )}
      </div>
    </div>
  );
}
