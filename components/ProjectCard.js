import React from "react";
import Link from "next/link";
import Image from "next/image";
import { motion } from "framer-motion";

import {
  getCloudinaryThumbLink,
  getProjectLink,
} from "../utils/helperFunctions";

import ArrowSmRightIcon from "@heroicons/react/20/solid/ArrowSmallRightIcon";

export default function ProjectCard(props) {
  const { name, slug, thumbnail, description, link, partners, accentColor } =
    props.project.fields;

  let eventLink = getProjectLink(link, slug);
  let thumbLink = getCloudinaryThumbLink(thumbnail[0].original_secure_url);

  if (eventLink) {
    return (
      <Link href={eventLink.url} passHref={eventLink.external} legacyBehavior>
        <motion.a
          className="flex flex-col md:flex-row bg-none w-full h-64 md:h-56 mb-14 rounded-xl transition duration-500 ease-in-out cursor-pointer group"
          initial={{ opacity: 0 }}
          whileInView={{ opacity: 1 }}
          viewport={{ margin: "-70px 0px -70px 0px", once: true }}
          target={eventLink.external ? "_blank" : undefined}
          rel={eventLink.external ? "noopener noreferrer" : undefined}
        >
          <div className="h-1/2 md:h-full w-full md:w-2/5 lg:w-1/3 flex flex-row justify-between">
            <motion.div className="relative w-full h-full overflow-hidden">
              <div className="absolute z-10 w-full h-full bg-black opacity-20"></div>
              <div
                className="absolute z-10 w-full h-full mix-blend-multiply opacity-80"
                style={{
                  backgroundColor: accentColor ? accentColor : "#FC8A17",
                }}
              ></div>

              <motion.div
                className="w-full h-full relative"
                initial={{
                  opacity: 0,
                  scale: 1.3,
                }}
                animate={{
                  opacity: 1,
                  scale: 1,
                  transition: {
                    ease: [0.6, 0.01, -0.05, 0.95],
                    duration: 1,
                  },
                }}
              >
                <Image
                  src={thumbnail[0].original_secure_url}
                  blurDataURL={thumbLink}
                  placeholder="blur"
                  fill
                  sizes="384px"
                  style={{ objectFit: "cover" }}
                  alt=""
                  className="group-hover:scale-105 transition duration-500 ease-in-out grayscale"
                  quality={65}
                />
              </motion.div>
            </motion.div>
          </div>
          <div className="h-1/2 md:h-full w-full md:w-3/5 lg:w-2/3 flex flex-col justify-between pt-3 md:pt-0 pl-0 md:pl-5">
            <div className="h-max">
              <motion.h2
                className="text-left text-sm md:text-xl font-semibold text-beige group-hover:text-beige-dark transition duration-500 ease-in-out"
                initial={{
                  opacity: 0,
                  x: -40,
                }}
                animate={{
                  opacity: 1,
                  x: 0,
                  transition: {
                    ease: "easeInOut",
                    duration: 0.3,
                  },
                }}
              >
                {name}
              </motion.h2>
              {partners && (
                <motion.h3
                  className="text-left text-sm md:text-base font-normal text-beige-dark group-hover:text-beige-dark transition duration-500 ease-in-out"
                  initial={{
                    opacity: 0,
                    x: -20,
                  }}
                  animate={{
                    opacity: 1,
                    x: 0,
                    transition: {
                      ease: "easeInOut",
                      duration: 0.3,
                    },
                  }}
                >
                  {"Partneri: " + partners.join(", ")}
                </motion.h3>
              )}
            </div>
            <div className="h-max flex flex-row justify-between">
              <div className="flex flex-row self-end">
                <motion.h3
                  className="text-left text-sm md:text-base font-medium text-beige group-hover:text-beige-dark transition duration-500 ease-in-out"
                  initial={{
                    opacity: 0,
                    x: -20,
                  }}
                  animate={{
                    opacity: 1,
                    x: 0,
                    transition: {
                      ease: "easeInOut",
                      duration: 0.3,
                    },
                  }}
                >
                  {description}
                </motion.h3>
              </div>
              <div className="w-min pl-5">
                <motion.div
                  className="flex rounded-full h-10 w-10 group-hover:scale-110 items-center justify-center cursor-pointer transition duration-500 ease-in-out"
                  initial={{
                    opacity: 0,
                    scale: 0,
                  }}
                  animate={{
                    opacity: 1,
                    scale: 1,
                    transition: {
                      ease: [0.6, 0.01, -0.05, 0.95],
                      duration: 0.6,
                    },
                  }}
                  style={{
                    backgroundColor: accentColor ? accentColor : "#FC8A17",
                  }}
                >
                  <ArrowSmRightIcon className="h-5 w-5 text-white" />
                </motion.div>
              </div>
            </div>
          </div>
        </motion.a>
      </Link>
    );
  } else {
    return (
      <motion.div
        className="flex flex-col md:flex-row bg-none w-full h-64 md:h-56 mb-14 rounded-xl"
        initial={{ opacity: 0 }}
        whileInView={{ opacity: 1 }}
        viewport={{ margin: "-70px 0px -70px 0px", once: true }}
      >
        <div className=" h-3/5 md:h-full w-full md:w-2/5 lg:w-1/3 flex flex-row justify-between">
          <motion.div className="relative w-full h-full overflow-hidden">
            <div className="absolute z-10 w-full h-full bg-black opacity-20"></div>
            <div
              className="absolute z-10 w-full h-full mix-blend-multiply opacity-80"
              style={{
                backgroundColor: accentColor ? accentColor : "#FC8A17",
              }}
            ></div>

            <motion.div
              className="w-full h-full relative"
              initial={{
                opacity: 0,
                scale: 1.3,
              }}
              animate={{
                opacity: 1,
                scale: 1,
                transition: {
                  ease: [0.6, 0.01, -0.05, 0.95],
                  duration: 0.6,
                },
              }}
            >
              <Image
                src={thumbnail[0].original_secure_url}
                blurDataURL={thumbLink}
                placeholder="blur"
                fill
                sizes="384px"
                style={{ objectFit: "cover" }}
                alt=""
                className="grayscale"
                quality={65}
              />
            </motion.div>
          </motion.div>
        </div>
        <div className="h-2/5 md:h-full w-full md:w-3/5 lg:w-2/3 flex flex-col justify-between pt-3 md:pt-0 pl-0 md:pl-5">
          <div className="h-max">
            <motion.h2
              className="text-left text-sm md:text-xl font-semibold text-beige"
              initial={{
                opacity: 0,
                x: -30,
              }}
              animate={{
                opacity: 1,
                x: 0,
                transition: {
                  ease: "easeInOut",
                  duration: 0.3,
                },
              }}
            >
              {name}
            </motion.h2>
            {partners && (
              <motion.h3
                className="text-left text-sm md:text-base font-normal text-beige-dark group-hover:text-beige-dark transition duration-500 ease-in-out"
                initial={{
                  opacity: 0,
                  x: -30,
                }}
                animate={{
                  opacity: 1,
                  x: 0,
                  transition: {
                    ease: "easeInOut",
                    duration: 0.3,
                  },
                }}
              >
                {"Partneri: " + partners.join(", ")}
              </motion.h3>
            )}
          </div>
          <div className="h-max flex flex-row justify-between">
            <div className="flex flex-row self-end">
              <motion.h3
                className="text-left text-sm md:text-base font-medium text-beige"
                initial={{
                  opacity: 0,
                  x: -30,
                }}
                animate={{
                  opacity: 1,
                  x: 0,
                  transition: {
                    ease: "easeInOut",
                    duration: 0.3,
                  },
                }}
              >
                {description}
              </motion.h3>
            </div>
          </div>
        </div>
      </motion.div>
    );
  }
}
